<?php

namespace Drupal\languagewire_translation_provider\Test\Content\Translation\Import;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface;
use Drupal\languagewire_translation_provider\Content\Translation\Import\CompositeFallback;
use Drupal\languagewire_translation_provider\Content\Translation\Import\TranslationImportFallbackInterface;
use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\DocumentStatus;
use Drupal\languagewire_translation_provider\Domain\Project;
use Drupal\languagewire_translation_provider\Test\Builder\JobItemBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Tests Composite Fallback .
 */
class CompositeFallbackTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Applies Fallback Strategies In Order They Were Provided .
   *
   * @test
   */
  public function appliesFallbackStrategiesInOrderTheyWereProvided(): void {
    $fallbackStrategy = new CompositeFallback(
          $this->getFooFallbackStrategy(),
          $this->getBarFallbackStrategy()
      );
    $translatedData = ['text' => 'Hello'];

    $project = $this->prophesize(Project::class)->reveal();
    $document = $this->prophesize(Document::class)->reveal();
    $result = $fallbackStrategy->applyTranslationFallback($translatedData, $document, $project);

    $this->assertSame('Hello foo bar', $result['text']);
  }

  /**
   * Get Foo Fallback Strategy .
   */
  private function getFooFallbackStrategy(): TranslationImportFallbackInterface {
    return new class implements TranslationImportFallbackInterface {

      /**
       * Apply Translation Fallback .
       */
      public function applyTranslationFallback(array $translatedData, TmgmtJobItemInterface|\Drupal\languagewire_translation_provider\Domain\Document $document_, TmgmtJobInterface|\Drupal\languagewire_translation_provider\Domain\Project $project): array {
        $translatedData['text'] .= ' foo';
        return $translatedData;
      }

    };
  }

  /**
   * Get Bar Fallback Strategy .
   */
  private function getBarFallbackStrategy(): TranslationImportFallbackInterface {
    return new class implements TranslationImportFallbackInterface {

      /**
       * Apply Translation Fallback .
       */
      public function applyTranslationFallback(array $translatedData, TmgmtJobItemInterface|\Drupal\languagewire_translation_provider\Domain\Document $document, TmgmtJobInterface|\Drupal\languagewire_translation_provider\Domain\Project $project): array {
        $translatedData['text'] .= ' bar';
        return $translatedData;
      }

    };
  }

  /**
   * Get Job Item .
   */
  private function getJobItem(): TmgmtJobItemInterface {
    return (new JobItemBuilder)->build();
  }

  /**
   * Get Job .
   */
  private function getJob(): TmgmtJobInterface {
    return $this->prophesize(TmgmtJobInterface::class)->reveal();
  }

}
