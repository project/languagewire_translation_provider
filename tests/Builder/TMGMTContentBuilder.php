<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Content\TMGMTContent;

/**
 * TMGMT Content Builder .
 */
class TMGMTContentBuilder {

  /**
   * Content Id .
   *
   * @var string
   */
  private $contentId = 'ContentId';

  /**
   * Content .
   *
   * @var array
   */
  private $content;

  /**
   * Constructs a new TMGMTContentBuilder object.
   */
  public function __construct() {
    $this->content = [
      '#text' => 'Foo',
      '#label' => 'A label',
    ];
  }

  /**
   * With Text .
   */
  public function withText(string $text): self {
    $this->content['#text'] = $text;
    return $this;
  }

  /**
   * With Content Id .
   */
  public function withContentId(string $id): self {
    $this->contentId = $id;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): TMGMTContent {
    return new TMGMTContent($this->contentId, $this->content);
  }

}
