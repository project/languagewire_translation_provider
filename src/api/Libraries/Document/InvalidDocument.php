<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

use Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface;

/**
 * Invalid Document.
 */
final class InvalidDocument extends DocumentBase {
  /**
   * Error messages.
   *
   * @var string[]
   */
  private array $errorMessages;

  /**
   * Constructs a new InvalidDocument object.
   *
   * @param string[] $errorMessages
   *   Error messages.
   * @param \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface|null $content
   *   Content.
   * @param string|null $language
   *   Language.
   * @param string $title
   *   Title.
   */
  public function __construct(array $errorMessages, ?ContentInterface $content, ?string $language, string $title) {
    parent::__construct($content, $language, $title);
    $this->errorMessages = $errorMessages;
  }

  /**
   * Returns error message.
   *
   * @return string[]
   *   Error messages.
   */
  public function getErrorMessages(): array {
    return $this->errorMessages;
  }

  /**
   * Creates new instance of self from exception.
   *
   * @param \Exception $exception
   *   Exception.
   * @param DocumentInterface $document
   *   Document.
   *
   * @return self
   *   Invalid document.
   */
  public static function fromException(\Exception $exception, DocumentInterface $document): self {
    return new self(
          [$exception->getMessage()],
          $document->getContent(),
          $document->getLanguage(),
          $document->getTitle()
      );
  }

}
