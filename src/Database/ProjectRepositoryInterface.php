<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Domain\Project;
use Drupal\languagewire_translation_provider\Domain\ProjectStatus;

/**
 * Project repository.
 */
interface ProjectRepositoryInterface {

  /**
   * Get by id.
   *
   * @param int $projectId
   *   Project id.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Project|null
   *   Project.
   */
  public function getById(int $projectId): ?Project;

  /**
   * Get all by status.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\ProjectStatus $projectStatus
   *   Project status.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Project[]
   *   Project.
   */
  public function getAllByStatus(ProjectStatus $projectStatus): array;

  /**
   * Get all by bundling ID.
   *
   * @param string $bundlingId
   *   Bundling ID.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Project[]
   *   Project.
   */
  public function getAllByBundlingId(string $bundlingId): array;

  /**
   * Get all by tmgmt job id.
   *
   * @param int $tmgmtJobId
   *   Tmgmt job id.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Project[]
   *   Project.
   */
  public function getAllByTmgmtJobId(int $tmgmtJobId): array;

  /**
   * Save.
   */
  public function save(Project $project): Project;

}
