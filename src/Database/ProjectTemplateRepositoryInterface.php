<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Domain\ProjectTemplate;

/**
 * Project template repository.
 * */
interface ProjectTemplateRepositoryInterface {

  /**
   * Get project template by ID.
   */
  public function getById(int $projectTemplateId): ?ProjectTemplate;

  /**
   * Get project template by platform ID.
   */
  public function getByPlatformId(int $projectTemplatePlatformId, TmgmtTranslatorInterface $translator): ?ProjectTemplate;

  /**
   * Get all project templates.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\ProjectTemplate[]
   *   All project templates.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

  /**
   * Save project template.
   */
  public function save(ProjectTemplate $projectTemplate, TmgmtTranslatorInterface $translator): void;

  /**
   * Save all project templates.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\ProjectTemplate[] $projectTemplates
   *   Project templates to save.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator to save project templates for.
   */
  public function saveAll(array $projectTemplates, TmgmtTranslatorInterface $translator): void;

  /**
   * Remove project template.
   */
  public function removeAll(TmgmtTranslatorInterface $translator): void;

}
