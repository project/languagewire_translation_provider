<?php

namespace Drupal\languagewire_translation_provider\Settings;

use Drupal\Core\Site\Settings;

/**
 * Drupal CMS Settings.
 *
 * @codeCoverageIgnore
 */
final class CmsSettings extends SettingsBase {

  /**
   * {@inheritdoc}
   */
  protected function getDrupalSettings(): array {
    return Settings::getAll();
  }

}
