<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Helpers;

use Drupal\languagewire_translation_provider\api\Libraries\Report\Exception\NegativeValueException;

/**
 * Ensure that the value is valid.
 */
final class Ensure {

  /**
   * Ensure that the value is not empty.
   */
  public static function notEmptyString(string $value, string $name): void {
    if ($value === '') {
      throw new \InvalidArgumentException("$name cannot be an empty string.");
    }
  }

  /**
   * Ensure that the value is not numeric.
   */
  public static function notNumeric(string $value, string $name): void {
    if (is_numeric($value)) {
      throw new \InvalidArgumentException("$name cannot be numeric string");
    }
  }

  /**
   * Ensure that the value is not positive or zero.
   */
  public static function positiveOrZeroInteger(int $value, string $name): void {
    if ($value < 0) {
      throw new NegativeValueException("$name cannot be negative. Got $value");
    }
  }

  /**
   * Ensure that the value is not longer than the specified length.
   */
  public static function notLongerThan(string $value, int $maxLength, string $name): void {
    $uriLength = mb_strlen($value);

    if ($uriLength > $maxLength) {
      throw new \InvalidArgumentException("$name is too long. $uriLength characters is more than allowed $maxLength.");
    }
  }

}
