<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite;

use Drupal\languagewire_translation_provider\Adapter\Entity\EntityInterface;

/**
 * Preview site build factory.
 */
final class PreviewSiteBuildFactory {

  const BUILD_LABEL = 'LanguageWire HTML Preview';

  const STRATEGY_ID = 'lw__%s';

  const STRATEGY_LABEL = "LanguageWire HTML Preview Strategy (%s)";

  const STRATEGY_FILE_PREFIX = "lw__%s";

  /**
   * Site build repository.
   *
   * @var PreviewSiteBuildRepositoryInterface
   */
  private PreviewSiteBuildRepositoryInterface $siteBuildRepository;

  /**
   * Strategy repository.
   *
   * @var PreviewStrategyRepositoryInterface
   */
  private PreviewStrategyRepositoryInterface $strategyRepository;

  /**
   * Constructs a new PreviewSiteBuildFactory object.
   *
   * @param PreviewSiteBuildRepositoryInterface $siteBuildRepository
   *   Site build repository.
   * @param PreviewStrategyRepositoryInterface $strategyRepository
   *   Strategy repository.
   */
  public function __construct(PreviewSiteBuildRepositoryInterface $siteBuildRepository, PreviewStrategyRepositoryInterface $strategyRepository) {
    $this->siteBuildRepository = $siteBuildRepository;
    $this->strategyRepository = $strategyRepository;
  }

  /**
   * Create site build.
   */
  public function createSiteBuild(EntityInterface $entity): ?PreviewSiteBuild {
    $strategy = $this->createStrategyForEntity($entity->getEntityId());
    return $this->siteBuildRepository->create($entity, self::BUILD_LABEL, $strategy->getId());
  }

  /**
   * Create strategy for entity.
   */
  private function createStrategyForEntity(string $contentId): PreviewStrategy {
    $contentTimeSuffix = sprintf("%s_%d", $contentId, time());
    return $this->strategyRepository->create(sprintf(self::STRATEGY_ID, $contentTimeSuffix), sprintf(self::STRATEGY_LABEL, $contentTimeSuffix), sprintf(self::STRATEGY_FILE_PREFIX, $contentTimeSuffix));
  }

}
