<?php

namespace Drupal\languagewire_translation_provider\Test\Fakes;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface;

/**
 * Fake Tmgmt Job Item.
 */
class FakeTmgmtJobItem implements TmgmtJobItemInterface {

  /**
   * Label .
   *
   * @var string
   */
  private $label;

  /**
   * Item Id .
   *
   * @var int
   */
  private $id;

  /**
   * Content .
   *
   * @var \Drupal\languagewire_translation_provider\Content\TMGMTContent[]
   */
  private $content;

  /**
   * Error .
   *
   * @var string
   */
  private $error;

  /**
   * Raw Data .
   *
   * @var array
   */
  private $rawData;

  /**
   * Wrapped Object Entity Type .
   *
   * @var string
   */
  private $wrappedObjectEntityType;

  /**
   * Wrapped Object Entity Id .
   *
   * @var string
   */
  private $wrappedObjectEntityId;

  /**
   * Constructs a new FakeTmgmtJobItem object.
   *
   * @param int $id
   *   Item Id .
   * @param string $label
   *   Label .
   * @param array $content
   *   Content .
   * @param array $rawData
   *   Raw Data .
   * @param string $wrappedObjectEntityType
   *   Wrapped Object Entity Type .
   * @param string $wrappedObjectEntityId
   *   Wrapped Object Entity Id .
   */
  public function __construct(int $id, string $label, array $content, array $rawData, string $wrappedObjectEntityType, string $wrappedObjectEntityId) {
    $this->label = $label;
    $this->id = $id;
    $this->content = $content;
    $this->rawData = $rawData;
    $this->wrappedObjectEntityType = $wrappedObjectEntityType;
    $this->wrappedObjectEntityId = $wrappedObjectEntityId;
  }

  /**
   * Label .
   */
  public function label(): string {
    return $this->label;
  }

  /**
   * Item Id .
   */
  public function itemId(): int {
    return $this->id;
  }

  /**
   * Get Content .
   */
  public function getContent(): array {
    return $this->content;
  }

  /**
   * Add Error .
   */
  public function addError(string $message): void {
    $this->error = $message;
  }

  /**
   * Get Error .
   */
  public function getError(): string {
    return $this->error;
  }

  /**
   * Add Message .
   */
  public function addMessage(string $message): void {
    // @todo Implement addMessage() method.
  }

  /**
   * Get Raw Data .
   */
  public function getRawData(): array {
    return $this->rawData;
  }

  /**
   * Get Wrapped Objects Entity Type .
   */
  public function getWrappedObjectsEntityType(): string {
    return $this->wrappedObjectEntityType;
  }

  /**
   * Get Wrapped Objects Entity Id .
   */
  public function getWrappedObjectsEntityId(): string {
    return $this->wrappedObjectEntityId;
  }

  /**
   * Set As Active .
   */
  public function setAsActive(): void {
    // Do nothing in this fake.
  }

}
