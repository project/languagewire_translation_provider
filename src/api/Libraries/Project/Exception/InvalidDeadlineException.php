<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Project\Exception;

/**
 * Invalid Deadline exception.
 *
 * Invalid Deadline exception is thrown when
 * trying to set a deadline in the past
 * or in non-UTC timezone.
 * */
final class InvalidDeadlineException extends \InvalidArgumentException {

  /**
   * Creates new instance of itself.
   *
   * Creates new instance of itself with
   * an error message about deadline
   * not being in the future.
   *
   * @param \DateTimeInterface $dateTime
   *   Deadline date.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromNonFutureDate(\DateTimeInterface $dateTime): self {
    return new InvalidDeadlineException(
          sprintf(
              'Deadline provided is not pointing to a future date. Given date was %s',
              $dateTime->format('Y-m-d H:i:s')
          )
      );
  }

  /**
   * Creates new instance of itself.
   *
   * Creates new instance of itself with
   * an error message about deadline
   * not being in UTC timezone.
   *
   * @param \DateTimeInterface $dateTime
   *   Deadline date.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromNonUtcDate(\DateTimeInterface $dateTime): self {
    return new InvalidDeadlineException(
          sprintf('Deadline date must be in UTC time zone. Provided was %s', $dateTime->getTimezone()->getName())
      );
  }

}
