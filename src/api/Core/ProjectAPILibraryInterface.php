<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core;

/**
 * Project API.
 */
interface ProjectAPILibraryInterface {

  /**
   * Execute.
   */
  public function execute(): ?array;

  /**
   * Get error message.
   */
  public function getErrorMessage(): ?string;

}
