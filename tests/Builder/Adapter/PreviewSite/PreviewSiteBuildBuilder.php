<?php

namespace Drupal\languagewire_translation_provider\Test\Builder\Adapter\PreviewSite;

use Drupal\languagewire_translation_provider\Adapter\PreviewSite\PreviewSiteBuild;
use Prophecy\Prophet;

/**
 * Preview Site Build Builder .
 */
final class PreviewSiteBuildBuilder {

  /**
   * Id .
   *
   * @var int
   */
  private int $id = 1;

  /**
   * Name .
   *
   * @var string
   */
  private string $name = "build_name";

  /**
   * Folder Path .
   *
   * @var string
   */
  private string $folderPath = "/tmp";

  /**
   * Strategy Id .
   *
   * @var string
   */
  private string $strategyId = "strategy_id";

  /**
   * Temporary Artifacts Base Path .
   *
   * @var string
   */
  private string $temporaryArtifactsFolder = '/private';

  /**
   * With Id .
   */
  public function withId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * With Name .
   */
  public function withName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * With Folder Path .
   */
  public function withFolderPath(string $folderPath): self {
    $this->folderPath = $folderPath;
    return $this;
  }

  /**
   * With Strategy Id .
   */
  public function withStrategyId(string $strategyId): self {
    $this->strategyId = $strategyId;
    return $this;
  }

  /**
   * With Temporary Artifacts Base Path .
   */
  public function withTemporaryArtifactsBasePath(string $temporaryArtifactsFolder): self {
    $this->temporaryArtifactsFolder = $temporaryArtifactsFolder;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): PreviewSiteBuild {
    $prophet = new Prophet();
    $build = $prophet->prophesize(PreviewSiteBuild::class);
    $build->getId()->willReturn($this->id);
    $build->getName()->willReturn($this->name);
    $build->getFolderPath()->willReturn($this->folderPath);
    $build->getTemporaryArtifactsBasePath()->willReturn($this->temporaryArtifactsFolder);

    $strategy = (new PreviewStrategyBuilder())->withId($this->strategyId)->build();
    $build->getStrategy()->willReturn($strategy);

    return $build->reveal();
  }

}
