<?php

namespace Drupal\languagewire_translation_provider\Test\Content;

use Drupal\languagewire_translation_provider\Content\SanitizerInterface;
use Drupal\languagewire_translation_provider\Content\TMGMTContent;
use PHPUnit\Framework\TestCase;

/**
 * TMGMT Content Test .
 */
class TMGMTContentTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Sanitizer .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $sanitizer;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->sanitizer = $this->prophesize(SanitizerInterface::class);
  }

  /**
   * It Sanitizes Content .
   *
   * @test
   */
  public function itSanitizesContent(): void {
    $contentId = 'ContentId';
    $tmgmtContent = ['#text' => 'ąę', '#label' => 'Item 1'];
    $content = new TMGMTContent($contentId, $tmgmtContent);
    $this->sanitizer->sanitize('ąę')->willReturn('ae');

    $sanitized = $content->sanitize($this->sanitizer->reveal());

    $this->assertNotSame($sanitized, $content);
    $this->assertNotSame($content->getText(), $sanitized->getText());
    $this->assertSame('ae', $sanitized->getText());
  }

  /**
   * Uses Parent Label .
   *
   * @test
   */
  public function usesParentLabel(): void {
    $content = new TMGMTContent('ContentId', ['#text' => 'Foo', '#parent_label' => ['Field A']]);

    $this->assertSame('Field A', $content->getLabel());
  }

  /**
   * Uses Content Id In Label If Latter Is Missing .
   *
   * @test
   */
  public function usesContentIdInLabelIfLatterIsMissing() {
    $content = new TMGMTContent('ContentId', ['#text' => 'Foo']);

    $this->assertSame('ID: ContentId', $content->getLabel());
  }

  /**
   * Sanitizes Content Id .
   *
   * @test
   */
  public function sanitizesContentId(): void {
    $content = new TMGMTContent('7][body][0][value', ['#text' => 'Foo']);

    $this->assertSame('7__body__0__value', $content->getSanitizedContentId());
  }

}
