<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\api\Models\Project\ProjectModel as ClientLibraryProject;
use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentModel as ClientLibraryAssignment;
use Drupal\languagewire_translation_provider\api\Models\Project\ProjectStatusModel;
use Drupal\languagewire_translation_provider\Client\AssignmentStatus;
use Drupal\languagewire_translation_provider\Client\Project;
use Drupal\languagewire_translation_provider\Client\ProjectStatus;
use Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary\AssignmentBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary\ProjectBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Project Test .
 */
class ProjectTest extends TestCase {

  /**
   * It Wraps Client Platform Project .
   *
   * @test
   */
  public function itWrapsClientPlatformProject(): void {
    $platformAssignment = $this->clientPlatformAssignment();
    $platformProject = $this->clientPlatformProject($platformAssignment);
    $project = new Project(
      $platformProject->getExternalId(),
      $platformProject->getPlatformLink(),
      $platformProject->getStatus()->getStatus(),
      [$platformAssignment]
    );

    $this->assertSame($platformProject->getId(), $project->id());
    $this->assertSame($platformProject->getPlatformLink(), $project->platformUrl());
    $this->assertSame(ProjectStatus::PENDING, $project->status()->value());

    $assignment = $project->assignments()[0];
    $this->assertSame(AssignmentStatus::PENDING, $assignment->getStatus()->getStatus());
  }

  /**
   * Client Platform Project .
   */
  public function clientPlatformProject(ClientLibraryAssignment $assignment): ClientLibraryProject {
    return (new ProjectBuilder())->withAssignment($assignment)->build();
  }

  /**
   * Client Platform Assignment .
   */
  public function clientPlatformAssignment(): ClientLibraryAssignment {
    return (new AssignmentBuilder())->build();
  }

}
