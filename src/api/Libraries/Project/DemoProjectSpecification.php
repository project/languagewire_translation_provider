<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Project;

use Drupal\languagewire_translation_provider\api\Models\ConfigurationModel;

/**
 * Demo Project Specification.
 *
 * Special case of a {@link ProjectSpecificationInterface} which
 * makes it very easy to create demo projects. Number
 * of required input parameters is significantly lower
 * than what regular `ProjectSpecification` requires.
 *
 * Example usage:
 * ```
 * $settings = new Settings(
 *         'ENDPOINT_URI', 'AUTHORIZATION_KEY'
 * );
 * $configurationService = new ConfigurationService(
 *          $settings
 * );
 *
 * $demoSpecification = new DemoProjectSpecification(
 *     'unique_demo_project_correlation_id',
 *     [$documentId1, $documentId2],
 *     [Language::POLISH],
 *     'Project title',
 *     $configurationService
 * );
 *
 * $projectService->create($demoSpecification);
 * ```
 */
final class DemoProjectSpecification extends ProjectSpecification {

  /**
   * Constructs a new DemoProjectSpecification object.
   *
   * @param string $correlationId
   *   Correlation ID.
   * @param array $sourceDocuments
   *   Source documents.
   * @param array $targetLanguages
   *   Target languages.
   * @param string $title
   *   Project title.
   * @param \Drupal\languagewire_translation_provider\api\Models\ConfigurationModel $configuration
   *   Configuration model.
   * @param \DateTimeImmutable|null $deadline
   *   Project deadline.
   *
   * @throws \Exception
   *   If the date cannot be created.
   */
  public function __construct(
    string $correlationId,
    array $sourceDocuments,
    array $targetLanguages,
    string $title,
    ConfigurationModel $configuration,
    ?\DateTimeImmutable $deadline = NULL
  ) {
    parent::__construct(
          $sourceDocuments,
          $targetLanguages,
          $correlationId,
          $title,
          $configuration,
          $deadline ?: $this->getFutureDateUtc(),
          'Demo Project Briefing',
          'XXX-123',
          TRUE
      );
  }

  /**
   * Returns a DateTime object storing some future time.
   *
   * Currently, it is 2 minutes from now in UTC timezone.
   *
   * @return \DateTimeImmutable
   *   A DateTimeImmutable object storing some future time
   *
   * @throws \Exception
   *   If the date cannot be created.
   */
  private function getFutureDateUtc(): \DateTimeImmutable {
    $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    return $now->add(new \DateInterval('PT2M'));
  }

}
