<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * Service Repository interface.
 */
interface ServiceRepositoryInterface extends ConfigurationItemRepositoryInterface {

  /**
   * Save all services.
   *
   * @param \Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface[] $services
   *   Services.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function saveAll(array $services, TmgmtTranslatorInterface $translator): void;

  /**
   * Get all services.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   *
   * @return \Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface[]
   *   Services.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

}
