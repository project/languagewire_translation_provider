<?php

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Client\LanguageWireClient;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientFactory;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientSettingsFactory;
use Drupal\languagewire_translation_provider\Client\ProjectSpecificationFactory;

/**
 * Configuration Item Importer Factory.
 */
class ConfigurationItemImporterFactory {

  /**
   * System.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\SystemInterface
   */
  private SystemInterface $system;

  /**
   * Project Specification Factory.
   *
   * @var \Drupal\languagewire_translation_provider\Client\ProjectSpecificationFactory
   */
  private ProjectSpecificationFactory $projectSpecificationFactory;

  /**
   * Constructs a new ConfigurationItemImporterFactory object.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\SystemInterface $system
   *   System.
   * @param \Drupal\languagewire_translation_provider\Client\ProjectSpecificationFactory $projectSpecificationFactory
   *   Project Specification Factory.
   */
  public function __construct(SystemInterface $system, ProjectSpecificationFactory $projectSpecificationFactory) {
    $this->system = $system;
    $this->projectSpecificationFactory = $projectSpecificationFactory;
  }

  /**
   * Create for.
   *
   * @throws \Drupal\languagewire_translation_provider\api\Core\Exception\Error
   */
  public function createFor(TmgmtTranslatorInterface $translatorAdapter): ConfigurationItemImporter {
    $clientFactory = $this->getLanguageWireClientFactory();
    $client = $clientFactory->createForTmgmtTranslator($translatorAdapter);

    return $this->createConfigurationItemImporter($client);
  }

  /**
   * Create dummy.
   */
  public function createDummy(): ConfigurationItemImporter {
    $clientFactory = $this->getLanguageWireClientFactory();
    $client = $clientFactory->createDummy();

    return $this->createConfigurationItemImporter($client);
  }

  /**
   * Get LanguageWire Client Factory.
   */
  public function getLanguageWireClientFactory(): LanguageWireClientFactory {
    $clientSettingsFactory = new LanguageWireClientSettingsFactory($this->system);
    return new LanguageWireClientFactory($clientSettingsFactory, $this->projectSpecificationFactory);
  }

  /**
   * Create Configuration Item Importer.
   */
  private function createConfigurationItemImporter(LanguageWireClient $client): ConfigurationItemImporter {
    $availableServices = new AvailableServices($client);
    $availableUsers = new AvailableUsers($client);
    $availableInvoicingAccounts = new AvailableInvoicingAccounts($client);
    $availableTranslationMemories = new AvailableTranslationMemories($client);
    $availableTermBases = new AvailableTermBases($client);
    $availableWorkAreas = new AvailableWorkAreas($client);
    $availableProjectTemplates = new AvailableProjectTemplates($client);

    return new ConfigurationItemImporter(
      $this->system,
      $availableUsers,
      $availableInvoicingAccounts,
      $availableServices,
      $availableTranslationMemories,
      $availableTermBases,
      $availableWorkAreas,
      $availableProjectTemplates,
      $client);
  }

}
