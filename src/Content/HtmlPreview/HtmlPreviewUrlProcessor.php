<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Content\HtmlPreview;

/**
 * Html Preview Url Processor.
 */
final class HtmlPreviewUrlProcessor {

  /**
   * Set relative URLs.
   */
  public function setRelativeUrls(string $htmlContent): string {
    $search = [
      "/core",
      "/sites",
    ];
    $replace = [
      "./core",
      "./sites",
    ];

    return str_replace($search, $replace, $htmlContent);
  }

}
