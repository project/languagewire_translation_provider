<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Platform\AllLanguages;
use Drupal\languagewire_translation_provider\Platform\AvailableServices;
use Drupal\languagewire_translation_provider\Platform\MachineTranslationLanguages;
use Drupal\languagewire_translation_provider\Platform\Services\DemoTranslation;
use Drupal\languagewire_translation_provider\Platform\Services\HumanAssistedMachineTranslation;
use Drupal\languagewire_translation_provider\Platform\Services\HumanTranslation;
use Drupal\languagewire_translation_provider\Platform\Services\MachineTranslation;

/**
 * Service Repository.
 */
final class TypedServiceRepository extends TypedConfigurationItemRepositoryBase implements ServiceRepositoryInterface {
  private const TYPE = 'service';

  /**
   * {@inheritDoc}
   */
  protected function type(): string {
    return self::TYPE;
  }

  /**
   * {@inheritDoc}
   */
  protected function map(\stdClass $configurationItem): ConfigurationItemInterface {
    return match ($configurationItem->item_id) {
      AvailableServices::DEMO_SERVICE_ID => new DemoTranslation(new AllLanguages()),
      AvailableServices::MACHINE_TRANSLATION_SERVICE_ID => new MachineTranslation($configurationItem->item_id, $configurationItem->name, new MachineTranslationLanguages()),
      AvailableServices::POST_EDITED_MACHINE_TRANSLATION_SERVICE_ID, AvailableServices::POST_EDITED_MACHINE_TRANSLATION_WITH_VALIDATION_SERVICE_ID => new HumanAssistedMachineTranslation($configurationItem->item_id, $configurationItem->name, new MachineTranslationLanguages()),
      default => new HumanTranslation($configurationItem->item_id, $configurationItem->name, new AllLanguages()),
    };
  }

}
