<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

/**
 * JSON Serializer utility class.
 *
 * Provides basic mechanisms for serialization
 * and deserialization of JSON.
 */
final class JsonSerializer implements SerializerInterface {

  /**
   * Serialize objects.
   *
   * Serializes objects implementing
   * DehydrationInterface into JSON string format.
   *
   * @param DehydrationInterface $object
   *   Object implementing DehydrationInterface.
   *
   * @return string
   *   JSON string.
   */
  public function serialize(DehydrationInterface $object): string {
    return JsonCodec::encode($object->dehydrate());
  }

  /**
   * Deserialize into Universal Document.
   *
   * Deserialize into Universal Document
   * from a specific string format.
   *
   * @param string $data
   *   String data to deserialize.
   *
   * @return array
   *   Deserialized data.
   */
  public function deserialize(string $data): array {
    return JsonCodec::decode($data);
  }

}
