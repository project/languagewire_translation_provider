<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

/**
 * Document Title Too Long Exception.
 *
 * Document Title Too Long Exception is thrown when
 * trying to instantiate Document object with a title parameter that
 * is too long.
 */
final class DocumentTitleTooLongException extends \InvalidArgumentException {
}
