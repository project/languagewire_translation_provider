<?php

namespace Drupal\languagewire_translation_provider\Test\Fakes;

use Drupal\languagewire_translation_provider\Settings\SettingsBase;

/**
 * Fake Drupal Settings.
 * */
class FakeSettings extends SettingsBase {

  /**
   * Drupal Settings .
   *
   * @var array
   */
  private $settings = [];

  /**
   * Constructs a new FakeSettings object.
   */
  public function __construct(array $settings) {
    $this->settings = $settings;
  }

  /**
   * Get Drupal Settings .
   */
  protected function getDrupalSettings(): array {
    return $this->settings;
  }

}
