<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Managers;

use Drupal\languagewire_translation_provider\api\Core\Settings;

/**
 * Abstract manager.
 */
abstract class AbstractManager {

  /**
   * Settings.
   *
   * @var \Drupal\languagewire_translation_provider\api\Core\Settings
   */
  protected Settings $settings;

  public function __construct(Settings $settings) {
    $this->settings = $settings;
  }

}
