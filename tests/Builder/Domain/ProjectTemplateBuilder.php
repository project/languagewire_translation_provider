<?php

declare(strict_types=1);
namespace Drupal\languagewire_translation_provider\Test\Builder\Domain;

use Drupal\languagewire_translation_provider\Domain\ProjectTemplate;

/**
 * Project Template Builder .
 */
final class ProjectTemplateBuilder {
  /**
   * Name .
   *
   * @var string
   */
  private $name = 'Project Template Name';
  /**
   * Platform Id .
   *
   * @var int
   */
  private $platformId = 1;
  /**
   * Target Languages .
   *
   * @var array
   */
  private $targetLanguages = ['en-US'];
  /**
   * Work Area Id .
   *
   * @var int
   */
  private $workAreaId = 2;
  /**
   * Invoicing Account Id .
   *
   * @var int
   */
  private $invoicingAccountId = 3;
  /**
   * Translation Memory Id .
   *
   * @var int
   */
  private $translationMemoryId = 4;
  /**
   * Term Base Id .
   *
   * @var int
   */
  private $termBaseId = 5;
  /**
   * User Id .
   *
   * @var int
   */
  private $userId = 6;
  /**
   * Template Id .
   *
   * @var int
   */
  private $templateId = 7;
  /**
   * Order Type .
   *
   * @var string
   */
  private $orderType = 'order';
  /**
   * Description .
   *
   * @var string
   */
  private $description = 'description';
  /**
   * Project Name .
   *
   * @var string
   */
  private $projectName = 'Default project name';
  /**
   * Source Language .
   *
   * @var string
   */
  private $sourceLanguage = 'en-US';

  /**
   * Desired Deadline .
   *
   * @var \DateTimeImmutable
   */
  private $desiredDeadline = NULL;
  /**
   * Available Services .
   *
   * @var array
   */
  private $availableServices = NULL;

  /**
   * Constructs a new ProjectTemplateBuilder object.
   */
  public function __construct() {
    $this->desiredDeadline = new \DateTimeImmutable("now");
  }

  /**
   * With Platform Id .
   */
  public function withPlatformId(int $platformId): self {
    $this->platformId = $platformId;
    return $this;
  }

  /**
   * With Template Id .
   */
  public function withTemplateId(int $templateId): self {
    $this->templateId = $templateId;
    return $this;
  }

  /**
   * With Source Language .
   */
  public function withSourceLanguage(string $sourceLanguage): self {
    $this->sourceLanguage = $sourceLanguage;
    return $this;
  }

  /**
   * With Target Languages .
   */
  public function withTargetLanguages(array $targetLanguages): self {
    $this->targetLanguages = $targetLanguages;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): ProjectTemplate {
    return new ProjectTemplate(
      $this->name,
      $this->platformId,
      $this->targetLanguages,
      $this->userId,
      $this->workAreaId,
      $this->invoicingAccountId,
      $this->translationMemoryId,
      $this->termBaseId,
      $this->orderType,
      $this->templateId,
      $this->description,
      $this->projectName,
      $this->sourceLanguage,
      $this->desiredDeadline,
      $this->availableServices
    );
  }

}
