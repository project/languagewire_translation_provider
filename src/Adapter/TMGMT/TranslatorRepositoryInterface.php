<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

use Drupal\tmgmt\TranslatorInterface;

/**
 * Translator Repository interface.
 */
interface TranslatorRepositoryInterface {

  /**
   * Get translator by ID.
   */
  public function getById(string $translatorId): ?TranslatorInterface;

}
