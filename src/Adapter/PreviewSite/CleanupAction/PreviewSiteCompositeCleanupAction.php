<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite\CleanupAction;

use Drupal\languagewire_translation_provider\Adapter\PreviewSite\PreviewSiteCleanupActionInterface;
use Drupal\languagewire_translation_provider\Content\HtmlPreview\Result\HtmlPreviewResult;

/**
 * Preview site cleanup action that runs multiple cleanup actions.
 */
final class PreviewSiteCompositeCleanupAction implements PreviewSiteCleanupActionInterface {
  /**
   * Cleanup actions.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\PreviewSite\PreviewSiteCleanupActionInterface[]
   */
  private array $actions;

  /**
   * Constructs a new PreviewSiteCompositeCleanupAction object.
   *
   * @param PreviewSiteIndexFileMover $previewSiteIndexFileMover
   *   Index file mover.
   * @param PreviewSiteUrlFixer $previewSiteURLFixer
   *   URL fixer.
   */
  public function __construct(PreviewSiteIndexFileMover $previewSiteIndexFileMover, PreviewSiteUrlFixer $previewSiteURLFixer) {
    $this->actions = [$previewSiteIndexFileMover, $previewSiteURLFixer];
  }

  /**
   * {@inheritdoc}
   */
  public function cleanUpResult(HtmlPreviewResult $htmlPreviewResult): HtmlPreviewResult {
    foreach ($this->actions as $action) {
      $htmlPreviewResult = $action->cleanUpResult($htmlPreviewResult);
    }

    return $htmlPreviewResult;
  }

}
