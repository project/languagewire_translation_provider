<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\Date;
use Drupal\languagewire_translation_provider\LanguageWireConnector;
use Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface;
use Drupal\languagewire_translation_provider\Platform\Services\DemoTranslation;
use Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use Drupal\languagewire_translation_provider\Settings\OrderType;
use Drupal\languagewire_translation_provider\Test\Fakes\FakeService;

/**
 * Service Builder .
 */
class ServiceBuilder {

  /**
   * Service Id .
   *
   * @var int
   */
  private $id = 1;

  /**
   * Service Name .
   *
   * @var string
   */
  private $name = 'Foo';

  /**
   * Template .
   *
   * @var int
   */
  private $template = 10;

  /**
   * Translation Memory .
   *
   * @var int
   */
  private $translationMemory = 11;

  /**
   * Term Base .
   *
   * @var int
   */
  private $termBase = 12;

  /**
   * Invoicing Account .
   *
   * @var int
   */
  private $invoicingAccount = 14;

  /**
   * Work Area .
   *
   * @var int
   */
  private $workArea = 14;

  /**
   * User .
   *
   * @var int
   */
  private $user = 15;

  /**
   * Deadline .
   *
   * @var \Drupal\languagewire_translation_provider\Date
   */
  private $deadline;

  /**
   * Platform Languages .
   *
   * @var \Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface
   */
  private $platformLanguages;

  /**
   * Constructs a new ServiceBuilder object.
   */
  public function __construct() {
    $this->deadline = new Date(new \DateTimeImmutable('+1 day'));
    $this->platformLanguages = $this->getFakePlatformLanguages();
  }

  /**
   * Build .
   */
  public function build(): ServiceInterface {
    return new FakeService(
          $this->id,
          $this->name,
          $this->template,
          $this->translationMemory,
          $this->termBase,
          $this->invoicingAccount,
          $this->workArea,
          $this->user,
          $this->deadline
      );
  }

  /**
   * Build Demo Translation .
   */
  public function buildDemoTranslation(): ServiceInterface {
    $jobSettings = $this->demoJobSettings();
    $service = new DemoTranslation($this->platformLanguages);
    $service->configure($jobSettings);
    return $service;
  }

  /**
   * Demo Job Settings .
   */
  private function demoJobSettings(): JobSettings {
    return new JobSettings([
      'user' => 1,
      'translation_memory' => 1,
      'work_area' => 1,
      'term_base' => 1,
      'invoicing_account' => 1,
      'order_type' => OrderType::ORDER,
      'deadline' => $this->deadline->toUtc()->format(LanguageWireConnector::DEADLINE_DATE_FORMAT),
    ]);
  }

  /**
   * Get Fake Platform Languages .
   */
  private function getFakePlatformLanguages(): PlatformLanguagesInterface {
    return new class implements PlatformLanguagesInterface
        {

      /**
       * Is Supported .
       */
      public function isSupported(LanguageMapping $languageMapping): bool {
        return TRUE;
      }

      /**
       * Is Supported By Machine Translation .
       */
      public function isSupportedByMachineTranslation(LanguageMapping $languageMapping): bool {
        return TRUE;
      }

      /**
       * Get All Languages .
       */
      public function getAllLanguages(): array {
        return [];
      }

    };
  }

}
