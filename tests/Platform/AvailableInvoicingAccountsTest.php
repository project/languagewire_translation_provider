<?php

namespace Drupal\languagewire_translation_provider\Test\Platform;

use Drupal\languagewire_translation_provider\Client\ClientInterface;
use Drupal\languagewire_translation_provider\Client\InvoicingAccount;
use Drupal\languagewire_translation_provider\Platform\AvailableInvoicingAccounts;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * Tests Available Invoicing Accounts .
 */
class AvailableInvoicingAccountsTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Client .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $client;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->client = $this->prophesize(ClientInterface::class);
  }

  /**
   * Returns Generator Yielding Invoicing Accounts .
   *
   * @test
   */
  public function returnsGeneratorYieldingInvoicingAccounts(): void {
    $invoicingAccount = new InvoicingAccount(1, 'IA');
    $this->client->getInvoicingAccounts()->willReturn([$invoicingAccount]);
    $service = new AvailableInvoicingAccounts($this->client->reveal());
    $invoicingAccounts = $service->get();

    $this->assertInstanceOf(Generator::class, $invoicingAccounts);

    $invoicingAccounts->rewind();
    $retrievedInvoicingAccount = $invoicingAccounts->current();
    $this->assertSame($invoicingAccount->id(), $retrievedInvoicingAccount->id());
    $this->assertSame($invoicingAccount->name(), $retrievedInvoicingAccount->name());
  }

}
