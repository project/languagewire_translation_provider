<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\ContentType;

/**
 * Content JSON.
 */
class ContentJson extends ContentTypeAbstract {
  protected const CONTENT_TYPE = 'application/json';

  /**
   * Encode.
   */
  public function encode(array $data): string {
    return json_encode($data);
  }

  /**
   * Decode.
   */
  public function decode(string $data): array {
    return json_decode($data, TRUE);
  }

}
