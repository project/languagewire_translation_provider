<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Project;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Get project endpoint.
 */
class Project extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id';
  protected const METHOD = Method::GET;
  protected const REQUEST_STRUCTURE = [
    'id',
  ];
  protected const RESPONSE_STRUCTURE = [
    'id',
    'externalId',
    'title',
    'briefing',
    'briefingForExperts',
    'purchaseOrderNumber',
    'deadline',
    'creationDate',
    'finishedAt',
    'templateId',
    'serviceId',
    'translationMemoryId',
    'termBaseId',
    'invoicingAccountId',
    'userId',
    'workAreaId',
    'status',
    'platformLink',
    'referenceFiles',
    'files' => [
      [
        'sourceFileId',
        'sourceFileName',
        'sourceLanguage',
        'translations' => [
          [
            'id',
            'fileId',
            'targetLanguage',
            'status',
            'translationFileName',
            'translationFileLink',
          ],
        ],
      ],
    ],
  ];

  /**
   * Set project ID.
   */
  public function setProjectId(int $projectID): void {
    $this->setParams(
          [
            'id' => $projectID,
          ]
      );
  }

}
