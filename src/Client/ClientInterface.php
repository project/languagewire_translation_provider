<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\Client\Project as PlatformProject;
use Drupal\languagewire_translation_provider\Domain\Project;

/**
 * Client interface.
 */
interface ClientInterface {

  /**
   * Get products.
   */
  public function getProducts(): array;

  /**
   * Get users.
   */
  public function getUsers(): array;

  /**
   * Get work areas.
   */
  public function getWorkAreas(): array;

  /**
   * Get translation memories.
   */
  public function getTranslationMemories(): array;

  /**
   * Get term bases.
   */
  public function getTermBases(): array;

  /**
   * Get invoicing accounts.
   */
  public function getInvoicingAccounts(): array;

  /**
   * Get project templates.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\ProjectTemplate[]
   *   Project templates.
   */
  public function getProjectTemplates(): array;

  /**
   * Upload documents.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Project $project
   *   Project to update documents.
   * @param \Drupal\languagewire_translation_provider\Platform\Document\UploadableDocument[] $uploadableDocuments
   *   Uploadable documents.
   *
   * @return \Drupal\languagewire_translation_provider\Platform\Document\UploadedDocument[]|\Drupal\languagewire_translation_provider\Platform\Document\InvalidDocument[]
   *   Uploaded documents or invalid documents.
   */
  public function uploadDocuments(Project $project, array $uploadableDocuments): array;

  /**
   * Start a project.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Project $project
   *   Project to start.
   */
  public function startProject(Project $project): bool;

  /**
   * Start a project as a quote.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Project $project
   *   Project to start as a quote.
   */
  public function startQuoteProject(Project $project): bool;

  /**
   * Submit.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Project $project
   *   Project.
   */
  public function submit(Project $project): Project;

  /**
   * Get platform project.
   */
  public function getPlatformProject(Project $project): PlatformProject;

  /**
   * Fetch translations.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Project $project
   *   Project.
   *
   * @return \Drupal\languagewire_translation_provider\Platform\Document\TranslatedDocument[]
   *   Translated documents.
   */
  public function fetchTranslations(Project $project): array;

  /**
   * Upload HTML previews.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Project $project
   *   Project to update html previews.
   * @param \Drupal\languagewire_translation_provider\Content\HtmlPreview\Result\HtmlPreviewDocumentResult[] $previewDocumentResults
   *   Preview document results.
   *
   * @return PlatformReferenceFile[]
   *   Platform reference files.
   */
  public function uploadHtmlPreviews(Project $project, array $previewDocumentResults): array;

}
