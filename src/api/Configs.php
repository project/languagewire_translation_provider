<?php

/**
 * @file
 */

return [
  'PROJECT_API_ENDPOINT'      => 'https://api.languagewire.com/project/v1',
  'PROJECT_API_AUTH_ENDPOINT' => 'https://idp.languagewire.com/realms/languagewire/protocol/openid-connect/token',
];
