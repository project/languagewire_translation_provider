<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * Translation Memory Repository interface.
 */
interface TranslationMemoryRepositoryInterface extends ConfigurationItemRepositoryInterface {

  /**
   * Save all.
   *
   * @param \Drupal\languagewire_translation_provider\Client\TranslationMemory[] $items
   *   Translation memory items.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function saveAll(array $items, TmgmtTranslatorInterface $translator): void;

  /**
   * Get all.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   *
   * @return \Drupal\languagewire_translation_provider\Client\TranslationMemory[]
   *   Translation memory items.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

}
