<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Domain;

/**
 * Document status.
* */
final class DocumentStatus {
  public const EMPTY = 'empty';
  public const ERROR = 'error';
  public const CANCELLED = 'cancelled';
  public const TRANSLATED = 'translated';
  public const UPLOADED_CONTENT = 'uploaded_content';
  public const UPLOADING_CONTENT = 'uploading_content';

  /**
   * Status.
   *
   * @var string
   */
  private string $status;

  /**
   * Constructs a new DocumentStatus object.
   *
   * @param string $status
   *   Status.
   */
  public function __construct(string $status) {
    $this->status = $status;
  }

  /**
   * Value.
   */
  public function value(): string {
    return $this->status;
  }

  /**
   * Equals.
   */
  public function equals(DocumentStatus $other): bool {
    return $this->status == $other->status;
  }

  /**
   * Uploading content.
   */
  public static function uploadingContent(): self {
    return new self(self::UPLOADING_CONTENT);
  }

  /**
   * Error.
   */
  public static function error(): self {
    return new self(self::ERROR);
  }

  /**
   * Cancelled.
   */
  public static function cancelled(): self {
    return new self(self::CANCELLED);
  }

  /**
   * Uploaded content.
   */
  public static function uploadedContent(): self {
    return new self(self::UPLOADED_CONTENT);
  }

  /**
   * Translated.
   */
  public static function translated(): self {
    return new self(self::TRANSLATED);
  }

  /**
   * Empty.
   */
  public static function empty(): self {
    return new self(self::EMPTY);
  }

}
