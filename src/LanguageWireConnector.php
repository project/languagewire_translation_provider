<?php

namespace Drupal\languagewire_translation_provider;

/**
 * LanguageWire Connector.
* */
final class LanguageWireConnector {
  public const MODULE_NAME = 'languagewire_translation_provider';
  public const DEADLINE_DATE_FORMAT = 'Y-m-d';

}
