<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentStatusModel as PlatformAssignmentStatus;
use Drupal\languagewire_translation_provider\Client\AssignmentStatus;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Assignment Status Test .
 */
class AssignmentStatusTest extends TestCase {

  /**
   * It Is Pending .
   *
   * @test
   */
  public function itIsPending(): void {
    $status = new AssignmentStatus(AssignmentStatus::PENDING);
    $this->assertTrue($status->isPending());
  }

  /**
   * It Is Finished .
   *
   * @test
   */
  public function itIsFinished(): void {
    $status = new AssignmentStatus(AssignmentStatus::FINISHED);
    $this->assertTrue($status->isFinished());
  }

  /**
   * It Is Cancelled .
   *
   * @test
   */
  public function itIsCancelled(): void {
    $status = new AssignmentStatus(AssignmentStatus::CANCELLED);
    $this->assertTrue($status->isCancelled());
  }

  /**
   * It Creates New Status From Platform Status Value .
   *
   * @test
   * @dataProvider statusDataProvider
   */
  public function itCreatesNewStatusFromPlatformStatusValue(int $platformStatusValue, int $expectedStatusValue): void {
    $platformAssignmentStatus = new PlatformAssignmentStatus($platformStatusValue);
    $assignmentStatus = AssignmentStatus::fromPlatformStatus($platformAssignmentStatus);

    $this->assertSame($expectedStatusValue, $assignmentStatus->value());
  }

  /**
   * Throws For Unrecognized Status .
   *
   * @test
   */
  public function throwsForUnrecognizedStatus(): void {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Assignment status 99 is not recognized.");

    $platformAssignmentStatus = new PlatformAssignmentStatus(99);
    AssignmentStatus::fromPlatformStatus($platformAssignmentStatus);
  }

  /**
   * Status Data Provider .
   */
  public function statusDataProvider(): array {
    return [
          [PlatformAssignmentStatus::IN_PROGRESS, AssignmentStatus::PENDING],
          [PlatformAssignmentStatus::FINISHED, AssignmentStatus::FINISHED],
          [PlatformAssignmentStatus::CANCELLED, AssignmentStatus::CANCELLED],
    ];
  }

}
