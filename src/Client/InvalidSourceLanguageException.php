<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Recoverable Exception.
* */
final class InvalidSourceLanguageException extends \Exception {
}
