<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Html Entities Encoder.
* */
final class HtmlEntitiesEncoder implements SanitizerInterface {

  /**
   * Sanitize.
   */
  public function sanitize(string $content): string {
    return $this->isPlainText($content)
            ? htmlentities($content, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8', FALSE)
            : $content;
  }

  /**
   * Desanitize.
   */
  public function desanitize(string $content): string {
    return $this->isPlainText($content)
            ? html_entity_decode($content, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8')
            : $content;
  }

  /**
   * Is plain text.
   */
  public function isPlainText(string $content): bool {
    return strlen($content) == strlen(strip_tags($content));
  }

}
