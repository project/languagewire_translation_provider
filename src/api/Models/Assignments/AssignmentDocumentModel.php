<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Assignments;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * Assignment document model.
 */
class AssignmentDocumentModel extends AbstractModel {
  /**
   * Source document unique ID.
   */
  protected string $sourceDocumentId;

  /**
   * Target document unique ID.
   */
  protected ?string $targetDocumentId;

  /**
   * Creates new instance of a AssignmentDocument.
   */
  public function __construct(string $sourceDocumentID, string $targetDocumentID = NULL) {
    $this->sourceDocumentId = $sourceDocumentID;
    $this->targetDocumentId = $targetDocumentID;
  }

  /**
   * Returns source document unique ID.
   *
   * @return string
   *   The source document unique ID.
   */
  public function getSourceDocumentId(): string {
    return $this->sourceDocumentId;
  }

  /**
   * Returns target document unique ID.
   *
   * @return string|null
   *   The target document unique ID.
   */
  public function getTargetDocumentId(): ?string {
    return $this->targetDocumentId;
  }

}
