<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * User Repository interface.
 */
interface UserRepositoryInterface extends ConfigurationItemRepositoryInterface {

  /**
   * Save all.
   *
   * @param \Drupal\languagewire_translation_provider\Client\User[] $items
   *   Work area items.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function saveAll(array $items, TmgmtTranslatorInterface $translator): void;

  /**
   * Get all.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   *
   * @return \Drupal\languagewire_translation_provider\Client\User[]
   *   Work area items.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

}
