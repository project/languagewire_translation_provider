<?php

namespace Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary;


use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentDocumentModel;
use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentModel;
use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentDocument;
use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentStatusModel;

/**
 * Assignment Builder .
 */
class AssignmentBuilder {

  /**
   * Assignment Id .
   *
   * @var int
   */
  private $assignmentId = 1;

  /**
   * Source Language .
   *
   * @var string
   */
  private $sourceLanguage = 'pl';

  /**
   * Target Language .
   *
   * @var string
   */
  private $targetLanguage = 'dk';

  /**
   * Status .
   *
   * @var \Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentStatusModel
   */
  private $status;

  /**
   * Assignment Documents .
   *
   * @var array
   */
  private $assignmentDocuments = [];

  /**
   * Constructs a new AssignmentBuilder object.
   */
  public function __construct() {
    $this->status = new AssignmentStatusModel(AssignmentStatusModel::IN_PROGRESS);
  }

  /**
   * With Assignment Document .
   */
  public function withAssignmentDocument(AssignmentDocumentModel $assignmentDocument): self {
    $this->assignmentDocuments[] = $assignmentDocument;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): AssignmentModel {
    return new AssignmentModel(
          $this->assignmentId,
          $this->sourceLanguage,
          $this->targetLanguage,
          $this->status,
          $this->assignmentDocuments
      );
  }

}
