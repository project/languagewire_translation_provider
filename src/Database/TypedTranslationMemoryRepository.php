<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Client\TranslationMemory;

/**
 * Translation Memory Repository.
 */
final class TypedTranslationMemoryRepository extends TypedConfigurationItemRepositoryBase implements TranslationMemoryRepositoryInterface {
  private const TYPE = 'translation_memory';

  /**
   * {@inheritDoc}
   */
  protected function type(): string {
    return self::TYPE;
  }

  /**
   * {@inheritDoc}
   */
  protected function map(\stdClass $translationMemory): ConfigurationItemInterface {
    return new TranslationMemory($translationMemory->item_id, $translationMemory->name);
  }

}
