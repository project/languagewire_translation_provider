<?php

namespace Drupal\languagewire_translation_provider\Test\Settings;

use Drupal\languagewire_translation_provider\Settings\Endpoint;
use Drupal\languagewire_translation_provider\Test\Builder\SettingsBuilder;
use Laminas\Diactoros\Uri;
use PHPUnit\Framework\TestCase;

/**
 * Endpoint Test.
 */
class EndpointTest extends TestCase {

  /**
   * It Returns Endpoint Configured In Settings .
   *
   * @test
   */
  public function itReturnsEndpointConfiguredInSettings() {
    $drupalSettings = (new SettingsBuilder())->withEndpoint('configured endpoint')->build();

    $endpoint = new Endpoint($drupalSettings);

    $this->assertEquals(new Uri('configured endpoint'), $endpoint->get());
  }

  /**
   * It Returns Default Endpoint If Not Configured .
   *
   * @test
   */
  public function itReturnsDefaultEndpointIfNotConfigured() {
    $drupalSettings = (new SettingsBuilder())->build();

    $endpoint = new Endpoint($drupalSettings);

    $this->assertEquals(new Uri('https://api.languagewire.com/project/v1'), $endpoint->get());
  }

}
