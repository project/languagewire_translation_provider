<?php

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Client\ClientInterface;

/**
 * Available Translation Memories.
 */
final class AvailableTranslationMemories {
  /**
   * Client.
   *
   * @var \Drupal\languagewire_translation_provider\Client\ClientInterface
   */
  private ClientInterface $client;

  /**
   * Constructs a new AvailableTranslationMemory object.
   *
   * @param \Drupal\languagewire_translation_provider\Client\ClientInterface $client
   *   Client.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Get.
   */
  public function get(): \Generator {
    foreach ($this->client->getTranslationMemories() as $translationMemory) {
      yield $translationMemory;
    }
  }

}
