<?php

namespace Drupal\languagewire_translation_provider\Ui;

use Drupal\Core\Form\FormStateInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJob;
use Drupal\languagewire_translation_provider\TranslatorUi;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;

/**
 * LanguageWire Translator Ui.
 */
class LanguageWireTranslatorUi extends TranslatorPluginUiBase {
  use TranslatorUi;

  /**
   * System adapter.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\SystemInterface
   */
  private mixed $system;

  /**
   * Job info UI.
   *
   * @var JobInfoUi
   */
  private mixed $jobInfoUi;

  /**
   * Configuration form UI.
   *
   * @var ConfigurationFormUi
   */
  private mixed $configurationFormUi;

  /**
   * Checkout UI.
   *
   * @var CheckoutUi
   */
  private mixed $checkoutUi;

  /**
   * Constructs a new LanguageWireTranslatorUi object.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $pluginId
   *   Plugin ID.
   * @param mixed $pluginDefinition
   *   Plugin definition.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->system = \Drupal::service('languagewire.drupal.system');
    $this->jobInfoUi = \Drupal::service('languagewire.job_info_ui');
    $this->configurationFormUi = \Drupal::service('languagewire.configuration_form_ui');
    $this->checkoutUi = \Drupal::service('languagewire.checkout_ui');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $form = parent::buildConfigurationForm($form, $formState);
    return $this->configurationFormUi->createConfigurationForm($form, $formState);
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\tmgmt\TMGMTException
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $formState, JobInterface $tmgmtJob): array {
    $form = $this->checkoutUi->buildCheckoutForm($form, $tmgmtJob);
    return parent::checkoutSettingsForm($form, $formState, $tmgmtJob);
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job): array {
    return $this->jobInfoUi->createJobInfoForm(new TmgmtJob($job));
  }

}
