<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models;

use Drupal\languagewire_translation_provider\api\Core\Exception\Error;

/**
 * Factory model.
 */
class FactoryModel {

  /**
   * Creates a new model instance.
   *
   * @throws \Drupal\languagewire_translation_provider\api\Core\Exception\Error
   */
  public static function createNewModel(string $className, array $modelData): ModelInterface {
    if (!class_exists($className) || !is_subclass_of($className, ModelInterface::class)) {
      throw new Error('Invalid Model');
    }

    $model = new $className();
    $model->setFields($modelData);

    return $model->hasRequiredFields() ? $model : throw new Error('Required data criteria not met for this model.');
  }

}
