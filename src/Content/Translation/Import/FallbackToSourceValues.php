<?php

namespace Drupal\languagewire_translation_provider\Content\Translation\Import;

use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\Project;

/**
 * Source Language Fallback translation strategy.
* */
final class FallbackToSourceValues implements TranslationImportFallbackInterface {

  /**
   * Apply translation fallback.
   */
  public function applyTranslationFallback(array $translatedData, Document $document, Project $project): array {
    foreach ($document->getContent() as $sourceContent) {
      if (!array_key_exists($sourceContent->getContentId(), $translatedData)) {
        $translatedData[$sourceContent->getContentId()] = ['#text' => $sourceContent->getText(), '#origin' => 'remote'];
      }
    }

    return $translatedData;
  }

}
