<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Platform\Document;

use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\DocumentStatus;

/**
 * Uploaded Document.
 */
final class UploadedDocument extends DocumentBase {

  /**
   * Platform Id.
   *
   * @var string
   */
  private string $platformId;

  /**
   * Constructs a new UploadedDocument object.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Document $document
   *   Document.
   * @param string $platformId
   *   Platform Id.
   */
  public function __construct(Document $document, string $platformId) {
    parent::__construct($document);
    $this->platformId = $platformId;
  }

  /**
   * Platform Id.
   */
  public function platformId(): string {
    return $this->platformId;
  }

  /**
   * Update Domain Document.
   */
  public function updateDomainDocument(): void {
    $this->document->setPlatformId($this->platformId);
    $this->document->setStatus(DocumentStatus::uploadedContent());
  }

}
