<?php

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\api\Models\LanguageModel;
use Drupal\languagewire_translation_provider\api\Services\LanguageService;

/**
 * All LanguageWire Languages.
* */
final class MachineTranslationLanguages implements PlatformLanguagesInterface {

  /**
   * Is Supported.
   */
  public function isSupported(LanguageMapping $languageMapping): bool {
    return LanguageService::isSupportedByMachineTranslation($languageMapping->remoteLanguage());
  }

  /**
   * Get All Languages.
   */
  public function getAllLanguages(): array {
    $allLanguages = array_flip(LanguageModel::$englishLabels);
    $mtLanguages = array_intersect($allLanguages, LanguageService::machineTranslationLanguages());

    return array_flip($mtLanguages);
  }

}
