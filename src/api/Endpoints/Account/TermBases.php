<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Account;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Termbases endpoint.
 */
class TermBases extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'term-bases';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'id',
          'name',
        ],
  ];

}
