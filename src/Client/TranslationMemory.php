<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Translation Memory configuration item.
 */
final class TranslationMemory implements ConfigurationItemInterface {

  /**
   * ID.
   *
   * @var int
   */
  private int $id;

  /**
   * Name.
   *
   * @var string
   */
  private string $name;

  /**
   * Constructs a new Translation Memory object.
   *
   * @param int $id
   *   ID.
   * @param string $name
   *   Name.
   */
  public function __construct(int $id, string $name) {
    $this->id = $id;
    $this->name = $name;
  }

  /**
   * ID.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Name.
   */
  public function name(): string {
    return $this->name;
  }

  /**
   * From platform translation memory.
   */
  public static function fromPlatformTranslationMemory(array $translationMemory): self {
    return new self($translationMemory['id'], $translationMemory['name']);
  }

}
