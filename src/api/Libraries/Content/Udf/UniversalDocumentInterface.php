<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content\Udf;

use Drupal\languagewire_translation_provider\api\Libraries\Content\DehydrationInterface;

/**
 * Provides an interface for operating on Universal Document.
 *
 * Universal Document reflects a Universal Document Format (UDF).
 * UDF is a message format for creating translation
 * documents that is accepted by LanguageWire API server.
 * It consists of a collection of Frames, which is mandatory,
 * an optional comment, optional Metadata object and
 * optional document preview URI.
 */
interface UniversalDocumentInterface extends DehydrationInterface {

  /**
   * Returns a collection of Frame objects.
   *
   * @return Frame[]
   *   An array of Frame objects.
   */
  public function getFrames(): array;

  /**
   * Returns Universal Document comment.
   *
   * @return string
   *   Universal Document comment.
   */
  public function getComment(): string;

  /**
   * Returns a Metadata associated with the Universal Document object.
   *
   * @return MetadataInterface
   *   Metadata object.
   */
  public function getMetadata(): MetadataInterface;

  /**
   * Return Uri object with a link to preview document.
   *
   * @return Uri|null
   *   Uri object with a link to preview document.
   */
  public function getPreview(): ?Uri;

}
