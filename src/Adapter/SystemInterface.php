<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\JobRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\InvoicingAccountRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\ProjectTemplateRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\ServiceRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\TermBaseRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\TranslationMemoryRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\UserRepositoryInterface;
use Drupal\languagewire_translation_provider\Database\WorkAreaRepositoryInterface;
use Drupal\languagewire_translation_provider\Settings\SettingsInterface;
use Psr\Log\LoggerInterface;

/**
 * Interface for the Drupal system.
 */
interface SystemInterface {

  /**
   * Version of Drupal.
   */
  public function version(): string;

  /**
   * Module version.
   */
  public function moduleVersion(string $moduleName): ?string;

  /**
   * Encode string.
   */
  public function encodeString(string $string): string;

  /**
   * Substring.
   */
  public function substring(string $string, int $startIndex, int $length): string;

  /**
   * Real path.
   */
  public function realPath(string $path): string;

  /**
   * Logger.
   */
  public function logger(): LoggerInterface;

  /**
   * Service repository.
   */
  public function serviceRepository(): ServiceRepositoryInterface;

  /**
   * User repository.
   */
  public function userRepository(): UserRepositoryInterface;

  /**
   * Invoicing account repository.
   */
  public function invoicingAccountRepository(): InvoicingAccountRepositoryInterface;

  /**
   * Translation memory repository.
   */
  public function translationMemoryRepository(): TranslationMemoryRepositoryInterface;

  /**
   * Term base repository.
   */
  public function termBaseRepository(): TermBaseRepositoryInterface;

  /**
   * Work area repository.
   */
  public function workAreaRepository(): WorkAreaRepositoryInterface;

  /**
   * Project template repository.
   */
  public function projectTemplateRepository(): ProjectTemplateRepositoryInterface;

  /**
   * Settings.
   */
  public function settings(): SettingsInterface;

  /**
   * TMGMT job repository.
   */
  public function tmgmtJobRepository(): JobRepositoryInterface;

}
