# Installation guide

1. Clone git repository

    ```bash
    $ git clone https://git.drupalcode.org/project/languagewire_translation_provider.git
    ```

2. Install Drupal core

   Go to the Drupal folder you want to install. Use `drupal9` for Drupal 9
   and `drupal10` for Drupal 10, then use composer commands to install
   the CMS, for example:


    ```bash
    $ cd environment/drupal10
    $ composer update --prefer-dist 
    $ composer dump
    ``` 
The composer scripts are prepared to also install all the required
additional (non-core) Drupal modules, including the Translation
Management Tool (TMGMT).

    After completing all the steps you will have in place everything you need.

3. Set up docker environment

   You can select which Drupal website you want to run with docker.
   To do so, copy `drupal9.env` to `.env` file.
   This will tell docker to set up containers for Drupal 9.

    ```bash
    $ cp drupal9.env .env
    ```

   If you want to use Drupal 10, do the same for `drupal10.env` file.
   Unfortunately, Docker compose tool doesn't accept a parameter
   that would allow user to specify which `env` file to use.

   Now, run the docker compose command:
    ```bash
    $ docker-compose up -d
    ```
   This will download all docker images and set them up for you.

4. Install Drupal

   In your web browser go to `localhost:8080/install.php` and install Drupal.

   Make sure you select correct database for installation
   (it's defined in the `*.env` files).
   By default database name is `drupal9` for Drupal 9 and `drupal10`
   for Drupal 10.
   Because the database runs in a separate docker container,
   make sure to change the default DB URL in "Advanced Options"
   of Drupal installation. Use `mysql` as the DB URL.

   ![Drupal installation database configuration](./docs/images/Drupal7-install-db-config.png)

5. Configure Drupal modules

   Navigate to Drupal's admin panel, to the ["Modules" section](http://localhost:8080/admin/modules). Scroll down to the
   "Translation Management" section and **select all** components listed there.

   ![TMGMT configuration](./docs/images/TMGMT-config.png)

   Apart from that, enable also the following components:
    * Core
        * Contact
        * Forum
        * Syslog
    * Chaos Tool Suite
        * Page manager
    * Multilingual - Entity Translation
        * Entity Translation Menu
        * Entity Translation Upgrade
    * Multilingual - Internationalization
        * Check all except for "User mail translation"

   To make development easier, Drupal has to offer the "devel" module.
   Feel free to enable all of it's components.
   It's especially useful when dealing with templates.

6. Deploy connector to the selected Drupal website

   TODO: Create a script and write instructions.

7. What to do if docker compose can't mount your volumes

    * Stop containers (the best is to use `docker-compose down` command)
    * If have have changed your domain password recently
      then open docker settings, go to Shared Drives and click on
      "Reset credentials" link.
    * Make sure that your drives are shared (checkboxes are checked)  
