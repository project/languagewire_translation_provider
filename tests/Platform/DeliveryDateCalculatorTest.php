<?php

namespace Drupal\languagewire_translation_provider\Test\Platform;

use Drupal\languagewire_translation_provider\Platform\DeliveryDateCalculator;
use PHPUnit\Framework\TestCase;

/**
 * Delivery Date Calculator Test.
 */
class DeliveryDateCalculatorTest extends TestCase {

  /**
   * Returns One When Word Count Is Zero .
   *
   * @test
   */
  public function returnsOneWhenWordCountIsZero() {
    $this->assertSame(1, DeliveryDateCalculator::calculateFor(0));
  }

  /**
   * Returns One When Word Count Is Below Threshold .
   *
   * @test
   */
  public function returnsOneWhenWordCountIsBelowThreshold() {
    $this->assertSame(1, DeliveryDateCalculator::calculateFor(10));
  }

  /**
   * Returns One When Word Count Is Exactly The Threshold .
   *
   * @test
   */
  public function returnsOneWhenWordCountIsExactlyTheThreshold() {
    $this->assertSame(1, DeliveryDateCalculator::calculateFor(DeliveryDateCalculator::WORDS_PER_DAY));
  }

  /**
   * Returns More Than One For Word Count Above Threshold .
   *
   * @test
   */
  public function returnsMoreThanOneForWordCountAboveThreshold() {
    $wordCount = DeliveryDateCalculator::WORDS_PER_DAY + 1;
    $this->assertSame(2, DeliveryDateCalculator::calculateFor($wordCount));
  }

}
