<?php

namespace Drupal\languagewire_translation_provider\Adapter\Logger;

use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\languagewire_translation_provider\LanguageWireConnector;
use Psr\Log\LoggerInterface;

/**
 * Drupal 9 Logger.
 */
final class NineLogger implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * {@inheritDoc}
   */
  public function log($level, $message, array $context = []): void {
    \Drupal::logger(LanguageWireConnector::MODULE_NAME)
      ->log($level, $message, $context);
  }

}
