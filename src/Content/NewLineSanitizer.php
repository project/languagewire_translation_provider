<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * New Line Sanitizer.
 */
final class NewLineSanitizer implements SanitizerInterface {
  private const CARRIAGE_RETURN_REPLACEMENT_TAG = '<hr data-escaped-char="carriage-return">';
  private const LINE_FEED_REPLACEMENT_TAG = '<hr data-escaped-char="line-feed">';

  /**
   * Sanitize.
   */
  public function sanitize(string $content): string {
    return str_replace(["\r", "\n", "\r\n"], [
      self::CARRIAGE_RETURN_REPLACEMENT_TAG,
      self::LINE_FEED_REPLACEMENT_TAG,
      self::CARRIAGE_RETURN_REPLACEMENT_TAG . self::LINE_FEED_REPLACEMENT_TAG,
    ], $content);
  }

  /**
   * Desanitize.
   */
  public function desanitize(string $content): string {
    return str_replace([
      self::CARRIAGE_RETURN_REPLACEMENT_TAG,
      self::LINE_FEED_REPLACEMENT_TAG,
      self::CARRIAGE_RETURN_REPLACEMENT_TAG . self::LINE_FEED_REPLACEMENT_TAG,
    ], ["\r", "\n", "\r\n"], $content);
  }

}
