<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\ContentType;

/**
 * Content octet stream.
 */
class ContentOctetStream extends ContentTypeAbstract {
  protected const CONTENT_TYPE = 'application/octet-stream';

  /**
   * Encode.
   */
  public function encode(array $data): string {
    return http_build_query($data);
  }

  /**
   * Decode.
   */
  public function decode(string $data): array {
    return [$data];
  }

}
