<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

use Psr\Http\Message\StreamInterface;

/**
 * Content interface.
 */
interface ContentInterface {

  /**
   * Get Stream .
   */
  public function getStream(): StreamInterface;

  /**
   * Get Mime Type .
   */
  public function getMimeType(): string;

}
