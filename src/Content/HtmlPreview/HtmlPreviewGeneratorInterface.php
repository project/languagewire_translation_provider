<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Content\HtmlPreview;

use Drupal\languagewire_translation_provider\Adapter\Entity\EntityInterface;
use Drupal\languagewire_translation_provider\Content\HtmlPreview\Result\HtmlPreviewResult;

/**
 * HTML preview generator interface.
 * */
interface HtmlPreviewGeneratorInterface {

  /**
   * Generates HTML preview for a specific page/node.
   *
   * Pre-requisites:
   *  Preview_site module installed.
   *  Private file storage path set up in settings.php file
   *  (see $settings['file_private_path']).
   *
   * We use the preview_site module create a full HTML dump of a page.
   * This extension was not designed for the
   * purpose of being used like we want to use it, though.
   *
   * For each node that dump should be created, a new
   * `PreviewSiteBuild` object must be built and saved.
   *
   * Node grouping is also possible, but it seems like a build
   * is content aware, so we should not be re-using the same build object,
   * unless proven otherwise.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\Entity\EntityInterface $entity
   *   Entity to generate preview for.
   *
   * @return \Drupal\languagewire_translation_provider\Content\HtmlPreview\Result\HtmlPreviewResult
   *   HTML preview result.
   */
  public function generatePreviewForEntity(EntityInterface $entity): HtmlPreviewResult;

}
