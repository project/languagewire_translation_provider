<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\ContentType;

/**
 * Content type.
 */
interface ContentTypeInterface {

  /**
   * Get content type string.
   */
  public function getContentTypeString() : string;

  /**
   * Encode.
   */
  public function encode(array $data): string;

  /**
   * Decode.
   */
  public function decode(string $data): array;

}
