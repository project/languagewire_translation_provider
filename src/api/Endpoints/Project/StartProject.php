<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Project;

use Drupal\languagewire_translation_provider\api\Core\DataPosition;
use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Start project endpoint.
 */
class StartProject extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/do-start';
  protected const METHOD = Method::POST;
  protected const RESPONSE_SUCCESS_CODES = [202];
  protected const REQUEST_STRUCTURE = [
    DataPosition::HEAD => [
      'id',
    ],
  ];

  /**
   * Set project ID.
   */
  public function setProjectId(int $projectID): void {
    $this->setParams(
          [
            DataPosition::HEAD => [
              'id' => $projectID,
            ],
          ]
      );
  }

}
