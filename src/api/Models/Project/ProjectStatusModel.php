<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Project;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * Project status model.
 */
class ProjectStatusModel extends AbstractModel {
  public const DRAFT = 'Draft';
  public const ACTIVE = 'Active';
  public const FINISHED = 'Finished';
  public const CANCELLED = 'Cancelled';

  /**
   * Project status code.
   */
  protected string $status;

  /**
   * Creates new instance of ProjectStatus.
   */
  public static function initialize(string $status): self {
    $instance = new self();

    $instance->status = $status;

    return $instance;
  }

  /**
   * Returns project status code.
   */
  public function getStatus(): string {
    return $this->status;
  }

}
