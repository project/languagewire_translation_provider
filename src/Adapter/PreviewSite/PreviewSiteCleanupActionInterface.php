<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite;

use Drupal\languagewire_translation_provider\Content\HtmlPreview\Result\HtmlPreviewResult;

/**
 * Preview site cleanup action.
 */
interface PreviewSiteCleanupActionInterface {

  /**
   * Clean up result.
   */
  public function cleanUpResult(HtmlPreviewResult $htmlPreviewResult): HtmlPreviewResult;

}
