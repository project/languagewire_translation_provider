<?php

namespace Drupal\languagewire_translation_provider\Test\Content;

use Drupal\languagewire_translation_provider\Content\HtmlPreview\HtmlPreviewUrlProcessor;
use PHPUnit\Framework\TestCase;

/**
 * Html Preview Url Processor Test .
 */
class HtmlPreviewUrlProcessorTest extends TestCase {

  /**
   * When Source Content Does Not Match Any Urls Then No Change Is Made .
   *
   * @test
   */
  public function whenSourceContentDoesNotMatchAnyUrlsThenNoChangeIsMade() {
    $sourceContent = "test";
    $processor = $this->getProcessor();
    $processedContent = $processor->setRelativeUrls($sourceContent);

    $this->assertSame($sourceContent, $processedContent);
  }

  /**
   * When Source Content Matches Core Urls Then They Are Relative .
   *
   * @test
   */
  public function whenSourceContentMatchesCoreUrlsThenTheyAreRelative() {
    $sourceContent = '<a href="/core/2">test</a>';
    $processor = $this->getProcessor();
    $processedContent = $processor->setRelativeUrls($sourceContent);
    $expectedContent = '<a href="./core/2">test</a>';

    $this->assertSame($expectedContent, $processedContent);
  }

  /**
   * Get Processor .
   */
  private function getProcessor(): HtmlPreviewUrlProcessor {
    return new HtmlPreviewUrlProcessor();
  }

}
