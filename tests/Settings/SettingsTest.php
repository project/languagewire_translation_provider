<?php

namespace Drupal\languagewire_translation_provider\Test\Settings;

use Drupal\languagewire_translation_provider\Test\Fakes\FakeSettings;
use PHPUnit\Framework\TestCase;

/**
 * Drupal Settings Test .
 */
class SettingsTest extends TestCase {

  /**
   * It Returns Config For Simple Key .
   *
   * @test
   */
  public function itReturnsConfigForSimpleKey() {
    $settings = new FakeSettings(['simple_key' => 'a value']);

    $result = $settings->getConfig('simple_key');

    $this->assertSame('a value', $result);
  }

  /**
   * It Returns Null If Requested Key Does Not Exist .
   *
   * @test
   */
  public function itReturnsNullIfRequestedKeyDoesNotExist() {
    $settings = new FakeSettings([]);

    $this->assertNull($settings->getConfig('invalid-key'));
  }

  /**
   * It Returns Value For Complex Key .
   *
   * @test
   */
  public function itReturnsValueForComplexKey() {
    $drupalSettings = [
      'level1' => [
        'level2' => [
          'level3' => 42,
        ],
      ],
    ];
    $settings = new FakeSettings($drupalSettings);
    $complexKey = 'level1/level2/level3';

    $result = $settings->getConfig($complexKey);

    $this->assertSame(42, $result);
  }

  /**
   * It Returns Null When Actual Value Is Not As Nested As Complex Key .
   *
   * @test
   */
  public function itReturnsNullWhenActualValueIsNotAsNestedAsComplexKey() {
    $drupalSettings = [
      'level1' => [
        'level2' => 'value',
      ],
    ];
    $settings = new FakeSettings($drupalSettings);
    $complexKey = 'level1/level2/level3';

    $result = $settings->getConfig($complexKey);

    $this->assertNull($result);
  }

  /**
   * It Returns Null For Empty Key .
   *
   * @test
   */
  public function itReturnsNullForEmptyKey() {
    $settings = new FakeSettings([]);

    $this->assertNull($settings->getConfig(''));
  }

  /**
   * It Returns Null For Incorrect Complex Key .
   *
   * @test
   */
  public function itReturnsNullForIncorrectComplexKey() {
    $drupalSettings = [
      'level1' => [
        'level2' => [
          'level3' => 42,
        ],
      ],
    ];
    $settings = new FakeSettings($drupalSettings);
    $complexKey = 'level1/level2/invalid_key';

    $result = $settings->getConfig($complexKey);

    $this->assertNull($result);
  }

}
