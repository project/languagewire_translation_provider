<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Project;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Create project endpoint.
 */
class CreateProject extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects';
  protected const METHOD = Method::POST;
  protected const RESPONSE_SUCCESS_CODES = [200];
  protected const REQUEST_STRUCTURE = [
    'title',
    'briefing',
    'briefingForExperts',
    'purchaseOrderNumber',
    'deadline',
    'templateId',
    'serviceId',
    'translationMemoryId',
    'termBaseId',
    'invoicingAccountId',
    'userId',
    'workAreaId',
    'externalId',
  ];
  protected const RESPONSE_STRUCTURE = [
    'id',
    'externalId',
    'title',
    'briefing',
    'briefingForExperts',
    'purchaseOrderNumber',
    'deadline',
    'creationDate',
    'finishedAt',
    'templateId',
    'serviceId',
    'translationMemoryId',
    'termBaseId',
    'invoicingAccountId',
    'userId',
    'workAreaId',
    'status',
    'platformLink',
    'referenceFiles',
    'files' => [
      [
        'sourceFileId',
        'sourceFileName',
        'sourceLanguage',
        'translations' => [
          [
            'id',
            'fileId ',
            'targetLanguage',
            'status',
            'translationFileName',
            'translationFileLink',
          ],
        ],
      ],
    ],
  ];

  /**
   * Create project.
   */
  public function createProject(
    string $title,
    string $briefing,
    string $briefingForExperts,
    string $purchaseOrderNumber,
    string $deadline,
    ?int $templateID,
    ?int $serviceID,
    ?int $translationMemoryID,
    ?int $termBaseID,
    int $invoicingAccountID,
    int $userID,
    int $workAreaID,
    string $externalID
  ): void {
    $this->setParams(
          [
            'title' => $title,
            'briefing' => $briefing,
            'briefingForExperts' => $briefingForExperts,
            'purchaseOrderNumber' => $purchaseOrderNumber,
            'deadline' => $deadline,
            'templateId' => $templateID,
            'serviceId' => $serviceID,
            'translationMemoryId' => $translationMemoryID,
            'termBaseId' => $termBaseID,
            'invoicingAccountId' => $invoicingAccountID,
            'userId' => $userID,
            'workAreaId' => $workAreaID,
            'externalId' => $externalID,
          ]
      );
  }

}
