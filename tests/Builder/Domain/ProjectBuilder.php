<?php

declare(strict_types=1);
namespace Drupal\languagewire_translation_provider\Test\Builder\Domain;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobInterface;
use Drupal\languagewire_translation_provider\Domain\Project;
use Drupal\languagewire_translation_provider\Domain\ProjectStatus;
use Drupal\languagewire_translation_provider\Test\Builder\TmgmtJobBuilder;
use Laminas\Diactoros\Uri;
use Psr\Http\Message\UriInterface;

/**
 * Project Builder .
 */
final class ProjectBuilder {
  /**
   * Tmgmt Job .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobInterface
   */
  private $tmgmtJob;
  /**
   * Correlation Id .
   *
   * @var string
   */
  private $correlationId = '';
  /**
   * Status .
   *
   * @var \Drupal\languagewire_translation_provider\Domain\ProjectStatus
   */
  private $status;
  /**
   * Documents .
   *
   * @var array
   */
  private $documents = [];
  /**
   * Project Id .
   *
   * @var int
   */
  private $projectId = 1;
  /**
   * Platform Id .
   *
   * @var int
   */
  private $platformId = 1;
  /**
   * Platform Url .
   *
   * @var \Laminas\Diactoros\UriInterface
   */
  private $platformUrl = NULL;

  /**
   * Constructs a new ProjectBuilder object.
   */
  public function __construct() {
    $this->tmgmtJob = (new TmgmtJobBuilder())->build();
    $this->status = ProjectStatus::uploading();
    $this->platformUrl = new Uri('https://example.com');
  }

  /**
   * With Tmgmt Job .
   */
  public function withTmgmtJob(TmgmtJobInterface $job): self {
    $this->tmgmtJob = $job;
    return $this;
  }

  /**
   * With Project Id .
   */
  public function withProjectId(int $projectId): self {
    $this->projectId = $projectId;
    return $this;
  }

  /**
   * With Documents .
   */
  public function withDocuments(...$documents): self {
    $this->documents = $documents;
    return $this;
  }

  /**
   * With Status .
   */
  public function withStatus(ProjectStatus $status): self {
    $this->status = $status;
    return $this;
  }

  /**
   * With Platform Id .
   */
  public function withPlatformId(int $platformId): self {
    $this->platformId = $platformId;
    return $this;
  }

  /**
   * With Platform Url .
   */
  public function withPlatformUrl(?UriInterface $platformUrl): self {
    $this->platformUrl = $platformUrl;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): Project {
    return new Project($this->tmgmtJob, $this->correlationId, $this->status, $this->documents, $this->projectId, $this->platformId, $this->platformUrl);
  }

}
