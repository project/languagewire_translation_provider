<?php

namespace Drupal\languagewire_translation_provider\Test\Platform\Services;

use Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface;
use Drupal\languagewire_translation_provider\Platform\Services\DemoTranslation;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use Drupal\languagewire_translation_provider\Settings\OrderType;
use PHPUnit\Framework\TestCase;

/**
 * Demo Translation Test .
 */
class DemoTranslationTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Deadline Is Two Minutes Despite Of JobSettings DeadlineValue.
   *
   * @test
   */
  public function deadlineIsTwoMinutesIntoTheFutureDespiteOfJobSettingsDeadlineValue(): void {
    $platformLanguages = $this->prophesize(PlatformLanguagesInterface::class)->reveal();
    $service = new DemoTranslation($platformLanguages);
    $jobSettings = new JobSettings(
        [
          'user' => 1,
          'translation_memory' => 1,
          'work_area' => 1,
          'invoicing_account' => 1,
          'order_type' => OrderType::ORDER,
          'deadline' => '2019-01-01',
        ]
    );
    $service->configure($jobSettings);

    $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    $twoMinutesAhead = new \DateTimeImmutable('+2 minutes', new \DateTimeZone('UTC'));
    $deadline = $service->deadline()->toUtc();

    $nowDiff = $deadline->diff($now);
    // Minutes.
    $this->assertSame(2, $nowDiff->i);
    // Years.
    $this->assertSame(0, $nowDiff->y);
    // Months.
    $this->assertSame(0, $nowDiff->m);
    // Days.
    $this->assertSame(0, $nowDiff->d);
    // Hours.
    $this->assertSame(0, $nowDiff->h);
    // Seconds.
    $this->assertSame(0, $nowDiff->s);

    $twoMinutesDiff = $deadline->diff($twoMinutesAhead);
    $this->assertSame(0, $twoMinutesDiff->i);
    $this->assertSame(0, $twoMinutesDiff->y);
    $this->assertSame(0, $twoMinutesDiff->m);
    $this->assertSame(0, $twoMinutesDiff->d);
    $this->assertSame(0, $twoMinutesDiff->h);
    $this->assertSame(0, $twoMinutesDiff->s);
  }

}
