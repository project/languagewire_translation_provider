<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Unknown Variable Placeholder Prefixing Type exception.
 */
class UnknownVariablePlaceholderPrefixingType extends \InvalidArgumentException {
}
