<?php

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

/**
 * Serializable interface.
 *
 * Provides methods for serialization/deserialization
 * of objects implementing DehydrationInterface.
 */
interface SerializerInterface {

  /**
   * Serializes objects implementing DehydrationInterface.
   *
   * Serializes objects implementing DehydrationInterface
   * into specific string format.
   *
   * @param DehydrationInterface $object
   *   Object implementing DehydrationInterface.
   *
   * @return string
   *   Serialized object.
   */
  public function serialize(DehydrationInterface $object): string;

  /**
   * Deserializes specific string format into object or array.
   *
   * @param string $data
   *   Serialized object.
   *
   * @return mixed
   *   Deserialized object or array.
   */
  public function deserialize(string $data): mixed;

}
