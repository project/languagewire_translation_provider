<?php

namespace Drupal\languagewire_translation_provider\Test\Platform\Document;

use Drupal\languagewire_translation_provider\Content\SanitizerInterface;
use Drupal\languagewire_translation_provider\Platform\Document\TranslatedDocument;
use Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary\FrameBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary\PlatformDocumentBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\Domain\DocumentBuilder as DomainDocumentBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\TranslatedContentBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Translated Document Test .
 */
class TranslatedDocumentTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * It Desanitizes Content .
   *
   * @test
   */
  public function itDesanitizesContent(): void {
    $sanitizedContent = 'sanitized';
    $sanitizer = $this->prophesize(SanitizerInterface::class);
    $sanitizer->desanitize($sanitizedContent)->willReturn('desanitized');
    $document = $this->translatedDocument($sanitizedContent);

    $document->desanitizeContent($sanitizer->reveal());

    $desanitizedContent = $document->translatedContent()[0];
    $this->assertSame('desanitized', $desanitizedContent->translatedContent());
  }

  /**
   * It Constructs Itself From Platform Document .
   *
   * @test
   */
  public function itConstructsItselfFromPlatformDocument(): void {
    $platformFrame = (new FrameBuilder())->withText('translated')->withMetadata(['contentId' => 'ContentID'])->build();
    $platformDocument = (new PlatformDocumentBuilder())->withFrames($platformFrame)->build();
    $domainDocument = (new DomainDocumentBuilder())->build();

    $translatedDocument = TranslatedDocument::fromPlatformDocument($platformDocument, $domainDocument);

    $translatedContent = $translatedDocument->translatedContent()[0];
    $this->assertSame('translated', $translatedContent->translatedContent());
  }

  /**
   * Translated Document .
   */
  private function translatedDocument(string $content): TranslatedDocument {
    $translatedContent = (new TranslatedContentBuilder())->withText($content)->build();
    $domainDocument = (new DomainDocumentBuilder())->build();
    return new TranslatedDocument([$translatedContent], $domainDocument);
  }

}
