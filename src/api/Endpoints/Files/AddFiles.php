<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Files;

use Drupal\languagewire_translation_provider\api\Core\ContentType\ContentJson;
use Drupal\languagewire_translation_provider\api\Core\DataPosition;
use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;
use Drupal\languagewire_translation_provider\api\Helpers\Strings;

/**
 * Add files endpoint.
 */
class AddFiles extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/files';
  protected const METHOD = Method::POST;
  protected const RESPONSE_SUCCESS_CODES = [201];
  protected const REQUEST_STRUCTURE = [
    DataPosition::HEAD => [
      'id',
      'filename',
      'sourceLanguage',
      'targetLanguages',
    ],
  ];
  protected const RESPONSE_STRUCTURE = [
    'fileId',
    'fileName',
    'projectId',
  ];

  /**
   * Add file.
   */
  public function addFile(int $projectID, string $filename, string $sourceLanguage, string $targetLanguages, mixed $data): void {
    $this->setParams(
          [
            DataPosition::HEAD => [
              'id' => $projectID,
              'filename' => Strings::cleanUp($filename),
              'sourceLanguage' => $sourceLanguage,
              'targetLanguages' => $targetLanguages,
            ],
            DataPosition::BODY => $data,
          ]
      );
  }

  /**
   * Execute override.
   */
  public function execute(): ?array {
    $this->setRequestContentType(new ContentJson());
    return parent::execute();
  }

}
