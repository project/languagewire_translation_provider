<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Settings\SettingsInterface;
use Drupal\languagewire_translation_provider\Test\Fakes\FakeSettings;

/**
 * Drupal Settings Builder.
 * */
class SettingsBuilder {

  /**
   * Settings .
   *
   * @var array
   */
  private $settings = ['languagewire' => []];

  /**
   * With Endpoint .
   */
  public function withEndpoint(string $endpointUrl): self {
    $this->settings['languagewire']['endpoint'] = $endpointUrl;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): SettingsInterface {
    return new FakeSettings($this->settings);
  }

}
