<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Adapter\System;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Platform\Document\UploadableDocument;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use Drupal\languagewire_translation_provider\Test\Builder\Domain\DocumentBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Uploadable Document Builder .
 */
class UploadableDocumentBuilder extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Document .
   *
   * @var \Drupal\languagewire_translation_provider\Domain\Document
   */
  private $document;

  /**
   * Language Mapping.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  private $languageMapping;

  /**
   * System.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  private $system;

  /**
   * Job Settings.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  private $jobSettings;

  /**
   * Constructs a new UploadedDocumentBuilder object.
   */
  public function __construct() {
    $jobSettings = $this->prophesize(JobSettings::class);
    $jobSettings->isItemContentEnabledForTranslation(Argument::type('int'), Argument::type('string'))->willReturn(TRUE);

    $this->document = (new DocumentBuilder())->build();
    $this->languageMapping = $this->prophesize(LanguageMapping::class)->reveal();
    $this->system = $this->prophesize(System::class)->reveal();
    $this->jobSettings = $jobSettings->reveal();
  }

  /**
   * With Document .
   */
  public function withDocument(Document $document): self {
    $this->document = $document;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): UploadableDocument {
    return new UploadableDocument(
      $this->document,
      $this->jobSettings,
      $this->languageMapping,
      $this->system
    );
  }

}
