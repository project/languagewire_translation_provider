<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Files;

/**
 * File Not Found Exception.
 *
 * File Not Found Exception is thrown when trying
 * to instantiate a ReferenceFile object from a file not present in
 * the filesystem.
 */
final class FileNotFoundException extends \InvalidArgumentException {
}
