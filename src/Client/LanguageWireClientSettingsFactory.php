<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\api\ProjectAPILibraryFactory;

/**
 * LanguageWire Client Settings Factory.
 */
class LanguageWireClientSettingsFactory {

  /**
   * System.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\SystemInterface
   */
  private SystemInterface $system;

  /**
   * Constructs a new LanguageWireClientSettingsFactory object.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\SystemInterface $system
   *   System.
   */
  public function __construct(SystemInterface $system) {
    $this->system = $system;
  }

  /**
   * Create for TMGMT translator.
   *
   * @throws \Drupal\languagewire_translation_provider\api\Core\Exception\Error
   */
  public function createForTmgmtTranslator(TmgmtTranslatorInterface $translator): LanguageWireClientSettings {
    $authenticationToken = ProjectAPILibraryFactory::createNewAuthenticationToken($translator->getClientId(), $translator->getClientSecret());
    return new LanguageWireClientSettings($authenticationToken, $this->system);
  }

  /**
   * Create dummy for TMGMT translator.
   */
  public function createDummy(): LanguageWireClientSettings {
    return new LanguageWireClientSettings("LanguageWire", $this->system);
  }

}
