<?php

declare(strict_types=1);
namespace Drupal\languagewire_translation_provider\Test\Builder\Domain;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface;
use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\DocumentStatus;
use Drupal\languagewire_translation_provider\Test\Builder\JobItemBuilder;

/**
 * Document Builder .
 */
final class DocumentBuilder {
  /**
   * Project Id .
   *
   * @var int
   */
  private $projectId = 10;
  /**
   * Tmgmt Job Item .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface
   */
  private $tmgmtJobItem;
  /**
   * Status .
   *
   * @var \Drupal\languagewire_translation_provider\Domain\DocumentStatus
   */
  private $status;
  /**
   * Id .
   *
   * @var int
   */
  private $id = 1;
  /**
   * Platform Id .
   *
   * @var string
   *
   * @todo this should default to null
   */
  private $platformId = '100';

  /**
   * Errors .
   *
   * @var array
   */
  private $errors = [];

  /**
   * Constructs a new DocumentBuilder object.
   */
  public function __construct() {
    $this->tmgmtJobItem = (new JobItemBuilder())->build();
    $this->status = DocumentStatus::uploadingContent();
  }

  /**
   * With Id .
   */
  public function withId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * With Platform Id .
   */
  public function withPlatformId(?string $platformId): self {
    $this->platformId = $platformId;
    return $this;
  }

  /**
   * With Tmgmt Job Item .
   */
  public function withTmgmtJobItem(TmgmtJobItemInterface $item): self {
    $this->tmgmtJobItem = $item;
    return $this;
  }

  /**
   * With Status .
   */
  public function withStatus(DocumentStatus $status): self {
    $this->status = $status;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): Document {
    return new Document($this->projectId, $this->tmgmtJobItem, $this->status, $this->id, $this->platformId, $this->errors);
  }

}
