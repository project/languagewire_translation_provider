<?php

namespace Drupal\languagewire_translation_provider\Test\Builder\Adapter\PreviewSite;

use Drupal\languagewire_translation_provider\Adapter\PreviewSite\PreviewStrategy;
use Prophecy\Prophet;

/**
 * Preview Strategy Builder .
 */
final class PreviewStrategyBuilder {

  /**
   * Id .
   *
   * @var string
   */
  private $id = 1;

  /**
   * With Id .
   */
  public function withId(string $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): PreviewStrategy {
    $prophet = new Prophet();
    $strategy = $prophet->prophesize(PreviewStrategy::class);
    $strategy->getId()->willReturn($this->id);

    return $strategy->reveal();
  }

}
