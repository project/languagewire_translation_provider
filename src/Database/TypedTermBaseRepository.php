<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Client\TermBase;

/**
 * Term Base Repository.
 */
final class TypedTermBaseRepository extends TypedConfigurationItemRepositoryBase implements TermBaseRepositoryInterface {
  /**
   * The type of the configuration item.
   */
  private const TYPE = 'term_base';

  /**
   * {@inheritDoc}
   */
  protected function type(): string {
    return self::TYPE;
  }

  /**
   * {@inheritDoc}
   */
  protected function map(\stdClass $termBase): ConfigurationItemInterface {
    return new TermBase($termBase->item_id, $termBase->name);
  }

}
