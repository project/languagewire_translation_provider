<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Work area configuration item.
 */
final class WorkArea implements ConfigurationItemInterface {

  /**
   * ID.
   *
   * @var int
   */
  private int $id;

  /**
   * Name.
   *
   * @var string
   */
  private string $name;

  /**
   * Constructs a new WorkArea object.
   *
   * @param int $id
   *   ID.
   * @param string $name
   *   Name.
   */
  public function __construct(int $id, string $name) {
    $this->id = $id;
    $this->name = $name;
  }

  /**
   * ID.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Name.
   */
  public function name(): string {
    return $this->name;
  }

  /**
   * From platform work area.
   */
  public static function fromPlatformWorkArea(array $workArea): self {
    return new self($workArea['id'], $workArea['name']);
  }

}
