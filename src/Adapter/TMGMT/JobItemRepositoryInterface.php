<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

/**
 * Job item repository.
 * */
interface JobItemRepositoryInterface {

  /**
   * Get job item by ID.
   */
  public function getById(int $jobItemId): ?TmgmtJobItemInterface;

}
