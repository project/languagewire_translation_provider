<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Client\Assignment as PlatformAssignment;
use Drupal\languagewire_translation_provider\Client\AssignmentStatus;
use Drupal\languagewire_translation_provider\Test\Fakes\FakePlatformAssignment;

/**
 * Platform Assignment Builder .
 */
class PlatformAssignmentBuilder {

  /**
   * Status .
   *
   * @var \Drupal\languagewire_translation_provider\Client\AssignmentStatus
   */
  private $status;

  /**
   * Target Document Ids .
   *
   * @var array
   */
  private $targetDocumentIds = [];

  /**
   * Constructs a new PlatformAssignmentBuilder object.
   */
  public function __construct() {
    $this->status = new AssignmentStatus(AssignmentStatus::PENDING);
  }

  /**
   * Build .
   */
  public function build(): PlatformAssignment {
    return new FakePlatformAssignment($this->status, $this->targetDocumentIds);
  }

}
