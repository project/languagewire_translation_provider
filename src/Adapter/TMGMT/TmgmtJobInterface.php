<?php

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

use Drupal\languagewire_translation_provider\Settings\JobSettings;

/**
 * TMGMT Job Adapter Interface.
 * */
interface TmgmtJobInterface {

  /**
   * Settings.
   */
  public function settings(): JobSettings;

  /**
   * Translator.
   */
  public function translator(): TmgmtTranslatorInterface;

  /**
   * Word count.
   */
  public function wordCount(): int;

  /**
   * Job ID.
   */
  public function jobId(): int;

  /**
   * Job label.
   */
  public function title(): string;

  /**
   * Job source language.
   */
  public function sourceLanguage(): LanguageMapping;

  /**
   * Job target language.
   */
  public function targetLanguage(): LanguageMapping;

  /**
   * Submit.
   */
  public function submit(string $message): void;

  /**
   * Cancel.
   */
  public function cancel(string $message, array $context = []): void;

  /**
   * Add message.
   */
  public function addMessage(string $message, array $context = []): void;

  /**
   * Has message.
   */
  public function hasMessage(string $message, string $searchOperator = '='): bool;

  /**
   * Count message.
   */
  public function countMessage(string $message, string $searchOperator = '='): int;

  /**
   * Make unprocessed.
   */
  public function makeUnprocessed(): void;

  /**
   * Make active.
   */
  public function makeActive(): void;

  /**
   * Make items active.
   */
  public function makeItemsActive(): void;

  /**
   * Save.
   */
  public function save(): void;

  /**
   * Get job item by ID.
   */
  public function itemById(int $itemId): ?TmgmtJobItemInterface;

  /**
   * Get job items.
   *
   * @return TmgmtJobItemInterface[]
   *   The job items.
   */
  public function items(): array;

  /**
   * Apply translations.
   */
  public function applyTranslations(array $data): void;

  /**
   * Finish.
   */
  public function finish(string $message): void;

  /**
   * Get order type.
   */
  public function getOrderType(): string;

  /**
   * Get briefing.
   */
  public function getBriefing(): string;

  /**
   * Get service ID.
   */
  public function getServiceId(): ?int;

  /**
   * Get user ID.
   */
  public function getUserId(): ?int;

  /**
   * Get project template platform ID.
   */
  public function getProjectTemplatePlatformId(): ?int;

  /**
   * Get deadline.
   */
  public function getDeadline(): \DateTimeImmutable;

}
