<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\Exception;

/**
 * Exception error handler.
 */
class Error extends \Exception {
  // Silence is gold.
}
