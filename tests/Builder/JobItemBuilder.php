<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface;
use Drupal\languagewire_translation_provider\Test\Fakes\FakeTmgmtJobItem;

/**
 * Job Item Builder .
 */
class JobItemBuilder {

  /**
   * Item Id .
   *
   * @var int
   */
  private $id = 1;

  /**
   * Label .
   *
   * @var string
   */
  private $label = 'Foo';

  /**
   * Content .
   *
   * @var array
   */
  private $content;

  /**
   * Raw Data .
   *
   * @var array
   */
  private $rawData = [];

  /**
   * Wrapped Object Entity Type .
   *
   * @var string
   */
  private $wrappedObjectEntityType = 'node';

  /**
   * Wrapped Object Entity Id .
   *
   * @var int
   */
  private $wrappedObjectEntityId = 1;

  /**
   * Constructs a new JobItemBuilder object.
   */
  public function __construct() {
    $this->content = [(new TMGMTContentBuilder())->build()];
  }

  /**
   * With Label .
   */
  public function withLabel(string $label): self {
    $this->label = $label;
    return $this;
  }

  /**
   * With Item Id .
   */
  public function withItemId(int $id): self {
    $this->id = $id;
    return $this;
  }

  /**
   * With Content .
   */
  public function withContent(array $content): self {
    $this->content = $content;
    return $this;
  }

  /**
   * With Raw Data .
   */
  public function withRawData(array $data): self {
    $this->rawData = $data;
    return $this;
  }

  /**
   * With Wrapped Object Entity Type .
   */
  public function withWrappedObjectEntityType(string $entityType): self {
    $this->wrappedObjectEntityType = $entityType;
    return $this;
  }

  /**
   * With Wrapped Object Entity Id .
   */
  public function withWrappedObjectEntityId(string $entityId): self {
    $this->wrappedObjectEntityId = $entityId;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): TmgmtJobItemInterface {
    return new FakeTmgmtJobItem(
          $this->id,
          $this->label,
          $this->content,
          $this->rawData,
          $this->wrappedObjectEntityType,
          $this->wrappedObjectEntityId
      );
  }

}
