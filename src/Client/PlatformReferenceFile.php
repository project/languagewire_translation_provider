<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Platform Reference File.
 */
final class PlatformReferenceFile {

  /**
   * Server ID.
   *
   * @var string
   */
  private string $serverId;

  /**
   * Filename.
   *
   * @var string
   */
  private string $filename;

  /**
   * Platform Document ID.
   *
   * @var ?string
   */
  private ?string $platformDocumentId;

  /**
   * Constructs a new PlatformReferenceFile object.
   *
   * @param string $serverId
   *   Server ID.
   * @param string $filename
   *   Filename.
   * @param string|null $platformDocumentId
   *   Platform Document ID.
   */
  public function __construct(string $serverId, string $filename, ?string $platformDocumentId = NULL) {
    $this->serverId = $serverId;
    $this->filename = $filename;
    $this->platformDocumentId = $platformDocumentId;
  }

  /**
   * Server ID.
   */
  public function getServerId(): string {
    return $this->serverId;
  }

  /**
   * Filename.
   */
  public function getFilename(): string {
    return $this->filename;
  }

  /**
   * Platform Document ID.
   */
  public function getPlatformDocumentId(): ?string {
    return $this->platformDocumentId;
  }

}
