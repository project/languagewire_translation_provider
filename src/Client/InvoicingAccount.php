<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Invoicing Account.
 */
final class InvoicingAccount implements ConfigurationItemInterface {

  /**
   * ID.
   *
   * @var int
   */
  private int $id;

  /**
   * Name.
   *
   * @var string
   */
  private string $name;

  /**
   * Constructs a new InvoicingAccount object.
   *
   * @param int $id
   *   ID.
   * @param string $name
   *   Name.
   */
  public function __construct(int $id, string $name) {
    $this->id = $id;
    $this->name = $name;
  }

  /**
   * ID.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Name.
   */
  public function name(): string {
    return $this->name;
  }

  /**
   * From platform invoicing account.
   */
  public static function fromPlatformInvoicingAccount(array $invoicingAccount): self {
    return new self($invoicingAccount['id'], $invoicingAccount['name']);
  }

}
