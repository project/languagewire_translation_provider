<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\General;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Supported services endpoint.
 */
class Services extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'services';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'id',
          'name',
        ],
  ];

}
