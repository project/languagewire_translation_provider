<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Content\HtmlPreview\Result;

/**
 * Html preview result.
 */
final class HtmlPreviewResult {

  /**
   * Constructs a new HtmlPreviewResult object.
   *
   * @param string $finalPreviewFolderPath
   *   Final preview folder path.
   * @param string $temporaryPreviewFolderPath
   *   Temporary preview folder path.
   */
  public function __construct(private string $finalPreviewFolderPath, private string $temporaryPreviewFolderPath) {
  }

  /**
   * Get html preview folder name.
   */
  public function getHtmlPreviewFolderName(): string {
    return basename($this->finalPreviewFolderPath);
  }

  /**
   * Get html preview folder path.
   */
  public function getHtmlPreviewFolderPath(): string {
    return $this->finalPreviewFolderPath;
  }

  /**
   * Get temporary preview folder path.
   */
  public function getTemporaryPreviewFolderPath(): string {
    return $this->temporaryPreviewFolderPath;
  }

}
