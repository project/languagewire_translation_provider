<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

use Psr\Http\Message\StreamInterface;

/**
 * Stream Content.
 *
 * Represents content stored in a stream of bytes.
 * Usage:
 * ```
 * $fileStream = new Stream(fopen('path/to/file.txt', 'rb'));
 * $streamContent = new StreamContent($fileStream, MediaType::PLAIN_TEXT);
 * ```
 */
final class StreamContent implements ContentInterface {
  /**
   * Stream content.
   *
   * @var \Psr\Http\Message\StreamInterface
   */
  private StreamInterface $stream;
  /**
   * Media Type of the file.
   *
   * @var string
   */
  private string $mimeType;

  /**
   * Constructs a new StreamContent object.
   *
   * @param \Psr\Http\Message\StreamInterface $stream
   *   Stream content.
   * @param string $mimeType
   *   Media Type of the file.
   */
  public function __construct(StreamInterface $stream, string $mimeType) {
    if (!MediaType::isSupported($mimeType)) {
      throw new \InvalidArgumentException("Media type \"$mimeType\" is not supported.");
    }

    $this->stream = $stream;
    $this->mimeType = $mimeType;
  }

  /**
   * Get Stream .
   */
  public function getStream(): StreamInterface {
    return $this->stream;
  }

  /**
   * Get Mime Type .
   */
  public function getMimeType(): string {
    return $this->mimeType;
  }

}
