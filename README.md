# LanguageWire Translation Provider

**LanguageWire Translation Provider** is a [Drupal](https://www.drupal.org/) module that uses [TMGMT](https://www.drupal.org/project/tmgmt) module to offer the option of new provider (LanguageWire) and this way gives the user the option to use LanguageWire translation services, to provide more effective and professional translations.

For a full description of the module, visit the [project page](https://www.drupal.org/project/languagewire_translation_provider).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/languagewire_translation_provider).

## Table of Contents

- Requirements
- Installation
- Configuration
- Contributing
- License
- Credits
- Reporting Issues
- Support

## Requirements

This module requires Drupal 9/10.

## Installation

### Composer mode

Run composer installer using the command:
`composer require drupal/languagewire_translation_provider`

### Manual mode

1. Download the module from [Drupal.org]
   (https://www.drupal.org/project/languagewire_translation_provider).
2. Extract the contents to the `/sites/all/modules`
   directory of your Drupal installation.
3. Enable the module through the Drupal administration interface.

## Configuration

1. Go to translation menu and select providers
2. Create a new provider and insert all the data provided by LanguageWire
3. Go to translation menu and select the pages to translate
4. Add to basket and follow the checkout steps.

## Contributing

If you want to contribute to this project, please read the [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## License

This project is licensed under the GPL 2.0 - see the [LICENSE.md](LICENSE.md) file for details.

## Credits

- [LanguageWire](https://www.languagewire.com)

## Reporting Issues

Please report any issues or bugs to the [issue tracker](https://www.drupal.org/project/issues/languagewire_translation_provider).

## Support

For any questions or support regarding this module, please visit the [Drupal.org project page](https://www.drupal.org/project/languagewire_translation_provider).
