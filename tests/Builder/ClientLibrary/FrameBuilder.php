<?php

namespace Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary;

use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\Frame;
use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\FrameInterface;
use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\Metadata;

/**
 * Frame Builder .
 */
class FrameBuilder {

  /**
   * Text .
   *
   * @var string
   */
  private $text = 'Lorem ipsum';

  /**
   * Comment .
   *
   * @var string
   */
  private $comment = 'Item 1 field 1';

  /**
   * Metadata .
   *
   * @var array
   */
  private $metadata = [];

  /**
   * With Text .
   */
  public function withText(string $text): self {
    $this->text = $text;
    return $this;
  }

  /**
   * With Metadata .
   */
  public function withMetadata(array $keyValuePairs): self {
    $this->metadata = $keyValuePairs;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): FrameInterface {
    return new Frame($this->text, $this->comment, new Metadata($this->metadata));
  }

}
