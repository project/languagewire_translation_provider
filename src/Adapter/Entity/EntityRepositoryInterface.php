<?php

namespace Drupal\languagewire_translation_provider\Adapter\Entity;

/**
 * Drupal Entity Repository interface.
 * */
interface EntityRepositoryInterface {

  /**
   * Get an entity object.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $entityId
   *   The entity id.
   *
   * @return \Drupal\languagewire_translation_provider\Adapter\Entity\EntityInterface|null
   *   The entity object or null if not found.
   */
  public function getEntity(string $entityTypeId, string $entityId): ?EntityInterface;

}
