<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Assignments;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * AssignmentStatus object.
 *
 * This object holds information about the
 * assignment status in the LanguageWire platform.
 */
class AssignmentStatusModel extends AbstractModel {
  public const IN_PROGRESS = 0;
  public const FINISHED = 1;
  public const CANCELLED = 2;

  /**
   * Status.
   *
   * @var int
   */
  protected int $status;

  /**
   * Constructs a new AssignmentStatus object.
   *
   * @param int $status
   *   Status.
   */
  public function __construct(int $status) {
    $this->status = $status;
  }

  /**
   * Get Status .
   *
   * @return int
   *   Status.
   */
  public function getStatus(): int {
    return $this->status;
  }

  /**
   * Returns the string representation of the object.
   *
   * @param string $status
   *   Status.
   *
   * @return static
   *   The object.
   *
   * @throws \Exception
   */
  public static function fromString(string $status): self {
    $targetStatus = match ($status) {
      'Draft', 'InConfiguration', 'InProgress' => self::IN_PROGRESS,
      'Finished' => self::FINISHED,
      'Cancelled' => self::CANCELLED,
      default => throw new \Exception('Invalid status provided!'),
    };

    return new self($targetStatus);
  }

}
