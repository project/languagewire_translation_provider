<?php

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;

/**
 * Platform Languages interface.
 * */
interface PlatformLanguagesInterface {

  /**
   * Is Supported.
   */
  public function isSupported(LanguageMapping $languageMapping): bool;

  /**
   * Get All Languages.
   */
  public function getAllLanguages(): array;

}
