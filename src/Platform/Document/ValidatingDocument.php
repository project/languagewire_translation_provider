<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Platform\Document;

use Drupal\languagewire_translation_provider\Domain\DocumentStatus;

/**
 * Validating Document.
 */
final class ValidatingDocument extends DocumentBase {

  /**
   * Get Platform Id.
   */
  public function platformId(): string {
    return $this->document->getPlatformId();
  }

  /**
   * From Uploaded Document.
   */
  public static function fromUploadedDocument(UploadedDocument $uploadedDocument): self {
    return new self($uploadedDocument->getDocument());
  }

  /**
   * Update Domain Document.
   */
  public function updateDomainDocument(): void {
    $this->document->setStatus(DocumentStatus::uploadingContent());
  }

}
