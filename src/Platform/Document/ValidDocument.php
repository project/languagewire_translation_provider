<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Platform\Document;

use Drupal\languagewire_translation_provider\Domain\DocumentStatus;

/**
 * Valid Document.
 */
final class ValidDocument extends DocumentBase {

  /**
   * Get platform Id.
   */
  public function platformId(): string {
    return $this->document->getPlatformId();
  }

  /**
   * Update domain document.
   */
  public function updateDomainDocument(): void {
    $this->document->setStatus(DocumentStatus::uploadingContent());
  }

}
