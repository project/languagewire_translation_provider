<?php

namespace Drupal\languagewire_translation_provider\Test\Fakes;

use Drupal\languagewire_translation_provider\Client\Assignment as PlatformAssignment;
use Drupal\languagewire_translation_provider\Client\AssignmentStatus;

/**
 * Fake Platform Assignment.
 */
class FakePlatformAssignment extends PlatformAssignment {

  /**
   * Status .
   *
   * @var \Drupal\languagewire_translation_provider\Client\AssignmentStatus
   */
  private $status;

  /**
   * Platform Document Id Pairs .
   *
   * @var array
   */
  private $documentIdPairs;

  /**
   * Constructs a new FakePlatformAssignment object.
   */
  public function __construct(AssignmentStatus $status, array $targetDocumentIds) {
    $this->status = $status;
    $this->documentIdPairs = $targetDocumentIds;
  }

  /**
   * Status .
   */
  public function status(): AssignmentStatus {
    return $this->status;
  }

  /**
   * Get Platform Document Id Pairs .
   */
  public function getPlatformDocumentIdPairs(): array {
    return $this->documentIdPairs;
  }

}
