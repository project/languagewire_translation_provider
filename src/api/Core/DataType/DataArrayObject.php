<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\DataType;

/**
 * Data array object.
 */
class DataArrayObject extends DataTypeAbstract {

  /**
   * Validate.
   */
  public function validate(array $data, ?string $position = NULL): bool {
    return $this->validateStructureRecursively($this->getStructure($position), $data);
  }

}
