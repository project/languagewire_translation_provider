<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * User configuration item.
 */
final class User implements ConfigurationItemInterface {
  /**
   * ID.
   *
   * @var int
   */
  private int $id;

  /**
   * Name.
   *
   * @var string
   */
  private string $name;

  /**
   * Constructs a new Translation Memory object.
   *
   * @param int $id
   *   ID.
   * @param string $name
   *   Name.
   */
  public function __construct(int $id, string $name) {
    $this->id = $id;
    $this->name = $name;
  }

  /**
   * ID.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Name.
   */
  public function name(): string {
    return $this->name;
  }

  /**
   * From platform user.
   */
  public static function fromPlatformUser(array $user): self {
    return new self($user['id'], $user['firstName'] . ' ' . $user['lastName']);
  }

}
