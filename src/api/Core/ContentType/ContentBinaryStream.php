<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\ContentType;

/**
 * Content binary stream.
 */
class ContentBinaryStream extends ContentTypeAbstract {
  protected const CONTENT_TYPE = 'application/x-binary';

  /**
   * Encode.
   */
  public function encode(array $data): string {
    if (count($data) !== 1) {
      throw new \InvalidArgumentException('ContentBinaryStream can only encode a single value');
    }

    $stream = $data[0];

    if (!is_string($stream)) {
      throw new \InvalidArgumentException('Data should be a string binary content');
    }

    return $stream;
  }

  /**
   * Decode.
   */
  public function decode(string $data): array {
    return [$data];
  }

}
