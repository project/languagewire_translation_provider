<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Domain;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface;

/**
 * Document factory.
 *
 * @todo This is _not_ a domain object;
 * it belongs to the application (service) layer.
 */
final class DocumentFactory {

  /**
   * Create documents for project.
   */
  public static function createDocumentsForTmgmtJobItems(array $items, int $projectId): array {
    return array_map(function (TmgmtJobItemInterface $jobItem) use ($projectId): Document {
      return new Document(
        $projectId,
        $jobItem,
        DocumentStatus::uploadingContent()
      );
    }, $items);
  }

}
