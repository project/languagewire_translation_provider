<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Client\InvoicingAccount;
use PHPUnit\Framework\TestCase;

/**
 * Invoicing Account Test .
 */
class InvoicingAccountTest extends TestCase {

  /**
   * It Creates New Object Instance From Platform Invoicing Account .
   *
   * @test
   */
  public function itCreatesNewObjectInstanceFromPlatformInvoicingAccount(): void {
    $platformInvoicingAccount = ['id' => 1, 'name' => 'IA'];
    $invoicingAccount = InvoicingAccount::fromPlatformInvoicingAccount($platformInvoicingAccount);

    $this->assertSame(1, $invoicingAccount->id());
    $this->assertSame('IA', $invoicingAccount->name());
  }

}
