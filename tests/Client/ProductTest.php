<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Client\Product;
use PHPUnit\Framework\TestCase;

/**
 * Product Test .
 */
class ProductTest extends TestCase {

  /**
   * It Creates New Object Instance From Platform Product .
   *
   * @test
   */
  public function itCreatesNewObjectInstanceFromPlatformProduct(): void {
    $platformProduct = ['id' => 1, 'name' => 'Product'];
    $product = Product::fromPlatformProduct($platformProduct);

    $this->assertSame(1, $product->id());
    $this->assertSame('Product', $product->name());
  }

}
