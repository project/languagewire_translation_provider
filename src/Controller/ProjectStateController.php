<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\languagewire_translation_provider\Database\ProjectRepositoryInterface;
use Drupal\languagewire_translation_provider\Domain\DocumentStatus;
use Drupal\languagewire_translation_provider\Domain\ProjectStatus;
use Drupal\languagewire_translation_provider\Service\ProjectStateHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Project state controller.
 */
class ProjectStateController extends ControllerBase implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * Project state handler.
   *
   * @var \Drupal\languagewire_translation_provider\Service\ProjectStateHandler
   */
  private ProjectStateHandler $projectStateHandler;

  /**
   * Project repository.
   *
   * @var \Drupal\languagewire_translation_provider\Database\ProjectRepositoryInterface
   */
  private ProjectRepositoryInterface $projectRepository;

  private const REIMPORT_JOB_MESSAGE = 'Project has been manually reimported.';

  /**
   * Constructs a new PlatformConfigurationItemsController object.
   *
   * @param \Drupal\languagewire_translation_provider\Service\ProjectStateHandler $projectStateHandler
   *   Project state handler.
   * @param \Drupal\languagewire_translation_provider\Database\ProjectRepositoryInterface $projectRepository
   *   Project repository.
   */
  public function __construct(ProjectStateHandler $projectStateHandler, ProjectRepositoryInterface $projectRepository) {
    $this->projectStateHandler = $projectStateHandler;
    $this->projectRepository = $projectRepository;
  }

  /**
   * Reimport projects from job.
   */
  public function reimportProjectsFromJob(Request $request): RedirectResponse {
    // @todo Redirect back to tmgmt jobs route
    // (use <route_name> instead of hardcoded path)
    $tmgmtJobId = intval($request->query->get('job_id'));

    $projectId = intval($request->query->get('project_id'));

    if ($projectId == 0) {
      return new RedirectResponse("/admin/tmgmt/jobs");
    }

    $project = $this->projectRepository->getById($projectId);

    if ($project == NULL || !$project->canBeReimported()) {
      return new RedirectResponse("/admin/tmgmt/jobs");
    }

    $project->setStatus(ProjectStatus::importing());
    $project->updateAllDocumentsStatus(DocumentStatus::uploadedContent());

    $this->projectRepository->save($project);

    $project->getTmgmtJob()->addMessage(self::REIMPORT_JOB_MESSAGE);
    $this->messenger()->addMessage($this->t('Project has been queued for reimport.'));

    return new RedirectResponse("/admin/tmgmt/jobs/$tmgmtJobId");
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public static function create(ContainerInterface $container): ProjectStateController {
    return new static(
          $container->get('languagewire.project_state_handler') ?? throw new \Exception('Project state handler not found.'),
          $container->get('languagewire.project_repository') ?? throw new \Exception('Project repository not found.')
      );
  }

}
