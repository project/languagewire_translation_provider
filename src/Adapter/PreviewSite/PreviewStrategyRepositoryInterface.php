<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite;

/**
 * Preview strategy repository.
 * */
interface PreviewStrategyRepositoryInterface {

  /**
   * Get preview strategy by ID.
   */
  public function getById(string $strategyId): ?PreviewStrategy;

  /**
   * Get all preview strategies.
   *
   * @param string $strategyId
   *   Internal ID of the strategy.
   * @param string $label
   *   Admin-viewable name of the strategy.
   * @param string $fileDeploymentPrefix
   *   Name of the folder that will contain the generated files.
   *
   * @return PreviewStrategy|null
   *   The created preview strategy.
   */
  public function create(string $strategyId, string $label, string $fileDeploymentPrefix): ?PreviewStrategy;

  /**
   * Delete preview strategy by ID.
   */
  public function deleteById(string $strategyId): void;

}
