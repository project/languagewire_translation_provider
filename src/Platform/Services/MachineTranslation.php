<?php

namespace Drupal\languagewire_translation_provider\Platform\Services;

use Drupal\languagewire_translation_provider\Date;

/**
 * Machine Translation service.
 */
final class MachineTranslation extends TranslationService {

  /**
   * Get deadline.
   */
  public function deadline(): Date {
    $tomorrow = new \DateTimeImmutable('+1 day');
    return new Date($tomorrow);
  }

}
