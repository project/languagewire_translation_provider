<?php

namespace Drupal\languagewire_translation_provider\Database;

/**
 * Contains all tables used by the module.
 */
interface TablesInterface {
  public const CONFIGURATION_ITEM = 'languagewire_translation_provider_configuration_item';
  public const TMGMT_TRANSLATOR = 'tmgmt_translator';
  public const PROJECT_DRAFT = 'languagewire_translation_provider_project_draft';
  public const PROJECTS = 'languagewire_translation_provider_projects';
  public const PROJECT_TEMPLATES = 'languagewire_translation_provider_project_templates';
  public const DOCUMENTS = 'languagewire_translation_provider_documents';

}
