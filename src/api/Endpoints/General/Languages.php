<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\General;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Supported languages endpoint.
 */
class Languages extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'languages';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'languageCode',
          'languageName',
        ],
  ];

}
