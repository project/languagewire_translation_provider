<?php

namespace Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary;

use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\FrameInterface;
use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\UniversalDocument;
use Drupal\languagewire_translation_provider\api\Libraries\Content\UdfContent;
use Drupal\languagewire_translation_provider\api\Libraries\Document\PlatformDocument;
use Drupal\languagewire_translation_provider\api\Models\LanguageModel;

/**
 * Platform Document Builder .
 */
class PlatformDocumentBuilder {

  /**
   * Language .
   *
   * @var string
   */
  private $language = LanguageModel::POLISH;

  /**
   * Title .
   *
   * @var string
   */
  private $title = 'Foo';

  /**
   * Id .
   *
   * @var int
   */
  private $id = 1;

  /**
   * Frames .
   *
   * @var array
   */
  private $frames = [];

  /**
   * With Frames .
   */
  public function withFrames(FrameInterface ...$frames): self {
    $this->frames = $frames;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): PlatformDocument {
    return new PlatformDocument(
          $this->id,
          $this->getUdf(),
          $this->language,
          $this->title
      );
  }

  /**
   * Get Udf .
   */
  private function getUdf(): UdfContent {
    if (empty($this->frames)) {
      $this->frames[] = (new FrameBuilder())->build();
    }

    return new UdfContent(new UniversalDocument($this->frames));
  }

}
