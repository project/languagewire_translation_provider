<?php

namespace Drupal\languagewire_translation_provider\api\Libraries\Files;

use Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface;

/**
 * Interface ReferenceFile.
 */
interface ReferenceFileInterface {

  /**
   * Returns reference file filename.
   *
   * @return string|null
   *   Filename.
   */
  public function getFilename(): ?string;

  /**
   * Returns content associated with the reference file.
   *
   * @return \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface
   *   Content.
   */
  public function getContent(): ContentInterface;

}
