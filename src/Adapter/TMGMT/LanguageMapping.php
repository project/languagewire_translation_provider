<?php

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

/**
 * Language Mapping.
 */
final class LanguageMapping {

  /**
   * Local language.
   *
   * @var string
   */
  private string $localLanguage;

  /**
   * Remote language.
   *
   * @var string
   */
  private string $remoteLanguage;

  /**
   * Constructs a new LanguageMapping object.
   *
   * @param string $localLanguage
   *   Local language.
   * @param string $remoteLanguage
   *   Remote language.
   */
  public function __construct(string $localLanguage, string $remoteLanguage) {
    $this->localLanguage = $localLanguage;
    $this->remoteLanguage = $remoteLanguage;
  }

  /**
   * Local language.
   */
  public function localLanguage(): string {
    return $this->localLanguage;
  }

  /**
   * Remote language.
   */
  public function remoteLanguage(): string {
    return $this->remoteLanguage;
  }

}
