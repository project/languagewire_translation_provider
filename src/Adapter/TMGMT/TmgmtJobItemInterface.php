<?php

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

/**
 * TMGMT Job Item interface.
 * */
interface TmgmtJobItemInterface {

  /**
   * Label.
   */
  public function label(): string;

  /**
   * Item ID.
   */
  public function itemId(): int;

  /**
   * Add message.
   */
  public function addMessage(string $message): void;

  /**
   * Add error.
   */
  public function addError(string $message): void;

  /**
   * Set as active.
   */
  public function setAsActive(): void;

  /**
   * Get content.
   *
   * @return \Drupal\languagewire_translation_provider\Content\TMGMTContent[]
   *   Contents.
   */
  public function getContent(): array;

  /**
   * Get raw data.
   */
  public function getRawData(): array;

  /**
   * Get wrapped objects entity type.
   */
  public function getWrappedObjectsEntityType(): string;

  /**
   * Get wrapped objects entity ID.
   */
  public function getWrappedObjectsEntityId(): string;

}
