<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\ContentType;

/**
 * Content form url.
 */
class ContentFormUrlEncoded extends ContentTypeAbstract {
  protected const CONTENT_TYPE = 'application/x-www-form-urlencoded';

  /**
   * Encode.
   */
  public function encode(array $data): string {
    return http_build_query($data);
  }

  /**
   * Decode.
   */
  public function decode(string $data): array {
    parse_str(parse_url($data, PHP_URL_QUERY), $array);

    return $array;
  }

}
