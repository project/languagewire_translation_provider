<?php

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

/**
 * Job Repository interface.
 */
interface JobRepositoryInterface {

  /**
   * Get job by ID.
   */
  public function getById(int $jobId): ?TmgmtJobInterface;

}
