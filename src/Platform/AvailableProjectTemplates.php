<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Client\ClientInterface;
use Drupal\languagewire_translation_provider\Domain\ProjectTemplate;

/**
 * Available Project Templates.
 */
final class AvailableProjectTemplates {

  /**
   * Client.
   *
   * @var \Drupal\languagewire_translation_provider\Client\ClientInterface
   */
  private ClientInterface $client;

  /**
   * Constructs a new AvailableProjectTemplates object.
   *
   * @param \Drupal\languagewire_translation_provider\Client\ClientInterface $client
   *   Client.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Get.
   */
  public function get(): array {
    return $this->client->getProjectTemplates();
  }

  /**
   * Get by server id.
   */
  public function getByServerId(int $projectTemplateServerId): ?ProjectTemplate {
    foreach ($this->get() as $projectTemplate) {
      if ($projectTemplate->getServerId() == $projectTemplateServerId) {
        return $projectTemplate;
      }
    }

    return NULL;
  }

}
