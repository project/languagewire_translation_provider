<?php

namespace Drupal\languagewire_translation_provider\Test\Content;

use Drupal\languagewire_translation_provider\Content\EmojiSanitizer;
use PHPUnit\Framework\TestCase;

/**
 * Tests EmojiSanitizer.
 */
class EmojiSanitizerTest extends TestCase {

  /**
   * When Emojis Are Contained In Text Then Emojis Are Removed .
   *
   * @test
   */
  public function whenEmojisAreContainedInTextThenEmojisAreRemoved() {
    $unsanitized = "Has emojis: 👩‍💼🧑‍💼👨‍💼👩‍🔧🧑‍🔧👨‍🔧👩‍🔬🧑‍🔬👨‍🔬👩‍🎨🧑‍🎨👨‍🎨👩‍🚒🧑‍🚒👨‍🚒👩‍✈🧑‍✈👨‍✈👩‍🚀🧑‍🚀👨‍🚀👩‍⚖🧑‍⚖👨‍⚖👰‍👰👰‍🤵‍🤵🤵‍👸🤴🦸‍🦸⛹️‍🤺🤾‍🤾🤾‍🏌️‍🏌🏌️‍🏇🧘‍🧘🧘🏄‍🏄🏄‍🏊‍";
    $sanitizer = new EmojiSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = "Has emojis: ";

    $this->assertSame($expected, $sanitized);
  }

  /**
   * When Danish Text Doesnt Contain Emojis Then Text Is Not Modified .
   *
   * @test
   */
  public function whenDanishTextDoesntContainEmojisThenTextIsNotModified() {
    $unsanitized = <<<TEXT
DS30 og LanguageWire går sammen om at styrke oversættelsesprocessen på rekordtid
Vi har alle oplevet besværet med at håndtere hundredvis af versioner af grafisk materiale i regneark og kompleks indeksering, håndtere oversættelser i løbet af ingen tid på grund af deadlines eller haft prisproblemer på grund af skiftende valutaer. DS30-værktøjet blev udviklet til at fjerne disse byrder fra fagfolk. Værktøjet giver brugere som projektledere, grafiske designere og andre fagfolk mulighed for at redigere, oversætte og oprette prislister, brochurer og meget mere effektivt og visuelt.

DS30 og LanguageWire gik sammen om at styrke oversættelsesprocessen. Oversættelse i platformen blev en realitet, hvilket gjorde det muligt for redaktører at skabe nye sprogversioner af indholdet på en meget effektiv måde. Integrationen mellem LanguageWire og DS30-systemet blev udført efter 50 timers rekordarbejde – fra en separat connector til en integreret løsning.
TEXT;
    $sanitizer = new EmojiSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = $unsanitized;

    $this->assertSame($expected, $sanitized);
  }

  /**
   * When Russian Text Doesnt Contain Emojis Then Text Is Not Modified .
   *
   * @test
   */
  public function whenRussianTextDoesntContainEmojisThenTextIsNotModified() {
    $unsanitized = <<<TEXT
DS30 и LanguageWire объединяют усилия, чтобы укрепить процесс перевода в рекордно короткое время
Мы все испытывали трудности с управлением сотнями версий графических материалов в электронных таблицах и сложным индексированием, с переносом данных в кратчайшие сроки из-за крайних сроков или проблем с ценообразованием из-за постоянно меняющихся валют. Инструмент DS30 был создан для того, чтобы снять эти нагрузки с профессионалов. С помощью этого инструмента пользователи, такие как менеджеры проектов, графические дизайнеры и другие специалисты, могут редактировать, переводить и создавать прайс-листы, брошюры и намного более эффективно и наглядно.

DS30 и LanguageWire объединили усилия, чтобы усилить процесс перевода. Перевод внутри платформы стал реальностью, что позволило редакторам создавать новые языковые версии содержания в высокоэффективном виде. Интеграция между системой LanguageWire и системой DS30 была выполнена через 50 часов работы - от отдельного разъема до интегрированного решения.
TEXT;
    $sanitizer = new EmojiSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = $unsanitized;

    $this->assertSame($expected, $sanitized);
  }

  /**
   * When Japanese Text Doesnt Contain Emojis Then Text Is Not Modified .
   *
   * @test
   */
  public function whenJapaneseTextDoesntContainEmojisThenTextIsNotModified() {
    $unsanitized = <<<TEXT
DS30 と LanguageWire は、記録破りな時間に翻訳プロセスを強化します。
私たち全員が、表計算ソフトや複雑な索引作成において、何百ものバージョンのグラフィック資料を管理するのに苦労し、締め切りによる時間的余裕のある翻訳の取り扱いや、絶えず変化する通貨による価格設定上の問題を抱えていました。 DS30 ツールは、これらの負担を専門家から取り除くために開発されました。 このツールにより、プロジェクト マネージャー、グラフィック デザイナー、その他の専門家などのユーザーは、価格表、カタログなどを、より効率的かつ視覚的に編集、翻訳、作成することができます。

DS30 と LanguageWire は、翻訳プロセスを強化するためにチームを組みました。 プラットフォーム内での翻訳が現実となったため、エディターはコンテンツの新しい言語バージョンを非常に効率的な方法で作成できるようになりました。 LanguageWire と DS30 システムの統合は、別個のコネクタから統合ソリューションまで、 50 時間のレコードブレーク後に行われました。
TEXT;
    $sanitizer = new EmojiSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = $unsanitized;

    $this->assertSame($expected, $sanitized);
  }

  /**
   * When Chinese Text Doesnt Contain Emojis Then Text Is Not Modified .
   *
   * @test
   */
  public function whenChineseTextDoesntContainEmojisThenTextIsNotModified() {
    $unsanitized = <<<TEXT
DS30 和 LanguageWire 团队共同努力 , 在创纪录的时代加强翻译流程
我们都经历过管理电子表格和复杂索引中数百个版本图形材料的麻烦 , 处理由于截止日期引起的时间紧迫的翻译 , 或者由于货币的不断变化而出现定价问题。 DS30 工具旨在消除专业人士的这些负担。 该工具使项目经理、平面设计师和其他专业人员等用户能够更高效、更直观地编辑、翻译和创建价目表、手册。

DS30 和 LanguageWire 携手加强翻译流程。 平台内的翻译成为现实 , 使编辑人员能够以高效的方式创建新的语言版本的内容。 LanguageWire 和 DS30 系统之间的集成是在破纪录的 50 个工作小时后完成的 - 从单独的连接器到集成解决方案。
TEXT;
    $sanitizer = new EmojiSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = $unsanitized;

    $this->assertSame($expected, $sanitized);
  }

  /**
   * When Greek Text Doesnt Contain Emojis Then Text Is Not Modified .
   *
   * @test
   */
  public function whenGreekTextDoesntContainEmojisThenTextIsNotModified() {
    $unsanitized = <<<TEXT
DS30 και ομάδα LanguageWire για να ενδυναμώσει τη διαδικασία μετάφρασης σε χρόνο ρεκόρ
Όλοι μας έχουμε εμπειρία στη διαχείριση εκατοντάδες εκδόσεων του γραφικού υλικού σε υπολογιστικά φύλλα και σύνθετη δειγματοληψία, στον χειρισμό μεταφράσεων στο νου των χρόνων λόγω προθεσμιών ή είχε προβλήματα τιμολόγησης λόγω ανά πάσα στιγμή μεταβαλλόμενων νομισμάτων. Το εργαλείο DS30 δημιουργήθηκε για την αφαίρεση αυτών των φορτίων από επαγγελματίες. Το εργαλείο επιτρέπει στους χρήστες, όπως οι διαχειριστές έργων, οι γραφικοί σχεδιαστές και άλλοι επαγγελματίες να επεξεργάζονται, να μεταφράζουν και να δημιουργούν τιμοκαταλόγους, φυλλάδια και πολλά άλλα, πιο αποτελεσματικά και οπτικά.

Το DS30 και το LanguageWire συνεργάστηκαν για να ενισχύσουν τη διαδικασία μετάφρασης. Η μετάφραση εντός της πλατφόρμας έγινε πραγματικότητα, καθιστώντας δυνατή τη δημιουργία νέων γλωσσικών εκδόσεων του περιεχομένου με πολύ αποτελεσματικό τρόπο για τους συντάκτες. Η ενσωμάτωση μεταξύ του LanguageWire και του συστήματος DS30 έγινε μετά από 50 ώρες λειτουργίας καταγραφής - από έναν ξεχωριστό σύνδεσμο σε μια ενσωματωμένη λύση.
TEXT;
    $sanitizer = new EmojiSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = $unsanitized;

    $this->assertSame($expected, $sanitized);
  }

}
