<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Project\Exception;

/**
 * Empty Parameter exception.
 *
 * Empty Parameter exception is thrown when
 * trying to pass an empty value argument to
 * a method which requires non-empty argument.
* */
final class EmptyParameterException extends \InvalidArgumentException {

  /**
   * From Empty Parameter .
   */
  public static function fromEmptyParameter(string $parameterName): self {
    return new EmptyParameterException(sprintf('%s cannot be empty', $parameterName));
  }

}
