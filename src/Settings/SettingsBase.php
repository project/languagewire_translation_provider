<?php

namespace Drupal\languagewire_translation_provider\Settings;

/**
 * Drupal Settings Base.
 * */
abstract class SettingsBase implements SettingsInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfig(string $key) {
    $drupalSettings = $this->getDrupalSettings();
    $complexKey = explode('/', $key);

    return $this->arrayValueByComplexKey($drupalSettings, $complexKey);
  }

  /**
   * Get Drupal settings.
   */
  abstract protected function getDrupalSettings(): array;

  /**
   * Get array value by complex key.
   */
  protected function arrayValueByComplexKey(array $array, array $complexKey) {
    $key = array_shift($complexKey);

    if ($key === '' || $key === NULL || !isset($array[$key])) {
      return NULL;
    }

    if (empty($complexKey)) {
      return $array[$key];
    }

    $value = $array[$key];

    return is_array($value)
            ? $this->arrayValueByComplexKey($array[$key], $complexKey)
            : NULL;
  }

}
