<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Files;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * File model.
 */
class FileModel extends AbstractModel {

  /**
   * Source file ID.
   *
   * @var string
   */
  protected string $sourceFileId;

  /**
   * Source file name.
   *
   * @var string
   */
  protected string $sourceFileName;

  /**
   * Source language.
   *
   * @var string
   */
  protected string $sourceLanguage;

  /**
   * Source content.
   *
   * @var string
   */
  protected string $sourceContent;

  /**
   * Translations.
   *
   * @var \Drupal\languagewire_translation_provider\api\Models\TranslationModel[]
   */
  protected array $translations;

  /**
   * Initializes the model.
   */
  public static function initialize(
    string $sourceFileID,
    string $sourceFileName,
    string $sourceLanguage,
    string $sourceContent,
    array $translations
  ): FileModel {
    $instance = new self();

    $instance->sourceFileId = $sourceFileID;
    $instance->sourceFileName = $sourceFileName;
    $instance->sourceLanguage = $sourceLanguage;
    $instance->sourceContent = $sourceContent;
    $instance->translations = $translations;

    return $instance;
  }

  /**
   * Get source file ID.
   */
  public function getSourceFileId(): string {
    return $this->sourceFileId;
  }

  /**
   * Set source file ID.
   */
  protected function setSourceFileId(string $sourceFileID): void {
    $this->sourceFileId = $sourceFileID;
  }

  /**
   * Get source file name.
   */
  public function getSourceFileName(): string {
    return $this->sourceFileName;
  }

  /**
   * Set source file name.
   */
  protected function setSourceFileName(string $sourceFileName): void {
    $this->sourceFileName = $sourceFileName;
  }

  /**
   * Get source language.
   */
  public function getSourceLanguage(): string {
    return $this->sourceLanguage;
  }

  /**
   * Set source language.
   */
  protected function setSourceLanguage(string $sourceLanguage): void {
    $this->sourceLanguage = $sourceLanguage;
  }

  /**
   * Get source content.
   */
  public function getSourceContent(): string {
    return $this->sourceContent;
  }

  /**
   * Set source content.
   */
  protected function setSourceContent(string $content): void {
    $this->sourceContent = $content;
  }

  /**
   * Get translations.
   *
   * @return \Drupal\languagewire_translation_provider\api\Models\TranslationModel[]
   *   Translations.
   */
  public function getTranslations(): array {
    return $this->translations;
  }

  /**
   * Set translations.
   *
   * @param \Drupal\languagewire_translation_provider\api\Models\TranslationModel[] $translations
   *   Translations.
   */
  protected function setTranslations(array $translations): void {
    $this->translations = $translations;
  }

}
