<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Domain;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface;

/**
 * Document domain object.
* */
final class Document {

  /**
   * Project ID.
   *
   * @var int
   */
  private int $projectId;

  /**
   * TMGMT item.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface
   */
  private TmgmtJobItemInterface $tmgmtItem;

  /**
   * Status.
   *
   * @var DocumentStatus
   */
  private DocumentStatus $status;

  /**
   * ID.
   *
   * @var int|null
   */
  private ?int $id;

  /**
   * Platform ID.
   *
   * @var string|null
   */
  private ?string $platformId;

  /**
   * Errors.
   *
   * @var array
   */
  private array $errors;

  /**
   * Constructs a new Document object.
   *
   * @param int $projectId
   *   Project ID.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface $tmgmtItem
   *   TMGMT item.
   * @param \Drupal\languagewire_translation_provider\Domain\DocumentStatus $status
   *   Status.
   * @param int|null $id
   *   ID.
   * @param string|null $platformId
   *   Platform ID.
   * @param array $errors
   *   Errors.
   */
  public function __construct(int $projectId, TmgmtJobItemInterface $tmgmtItem, DocumentStatus $status, ?int $id = NULL, ?string $platformId = NULL, array $errors = []) {
    $this->projectId = $projectId;
    $this->tmgmtItem = $tmgmtItem;
    $this->status = $status;
    $this->id = $id;
    $this->platformId = $platformId;
    $this->errors = $errors;
  }

  /**
   * Get project ID.
   */
  public function getProjectId(): int {
    return $this->projectId;
  }

  /**
   * Get TMGMT item.
   */
  public function getTmgmtItem(): TmgmtJobItemInterface {
    return $this->tmgmtItem;
  }

  /**
   * Get status.
   */
  public function getStatus(): DocumentStatus {
    return $this->status;
  }

  /**
   * Set status.
   */
  public function setStatus(DocumentStatus $newStatus): void {
    $this->status = $newStatus;
  }

  /**
   * Get ID.
   */
  public function getId(): ?int {
    return $this->id;
  }

  /**
   * Get platform ID.
   */
  public function getPlatformId(): ?string {
    return $this->platformId;
  }

  /**
   * Set platform ID.
   */
  public function setPlatformId(?string $platformId): void {
    $this->platformId = $platformId;
  }

  /**
   * Get errors.
   */
  public function getErrors(): array {
    return $this->errors;
  }

  /**
   * Get TMGMT item ID.
   */
  public function getTmgmtItemId(): int {
    return $this->tmgmtItem->itemId();
  }

  /**
   * Get content.
   *
   * @return \Drupal\languagewire_translation_provider\Content\TMGMTContent[]
   *   Content.
   */
  public function getContent(): array {
    return $this->tmgmtItem->getContent();
  }

  /**
   * Add error.
   */
  public function addError(string $message): void {
    $this->errors[] = $message;
    $this->tmgmtItem->addError($message);
  }

  /**
   * Get title.
   */
  public function getTitle(): string {
    return $this->tmgmtItem->label();
  }

}
