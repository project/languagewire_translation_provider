<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Platform\Document;

use Drupal\languagewire_translation_provider\Domain\Document;

/**
 * Document base.
 * */
abstract class DocumentBase {

  /**
   * Document.
   *
   * @var \Drupal\languagewire_translation_provider\Domain\Document
   */
  protected Document $document;

  /**
   * Constructs a new DocumentBase object.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Document $document
   *   Document.
   */
  public function __construct(Document $document) {
    $this->document = $document;
  }

  /**
   * Get Document.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Document
   *   Document.
   */
  public function getDocument(): Document {
    return $this->document;
  }

  /**
   * Update Domain Document.
   */
  abstract public function updateDomainDocument(): void;

}
