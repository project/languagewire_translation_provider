<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Project;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Cancel project endpoint.
 */
class CancelProject extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id';
  protected const METHOD = Method::DELETE;
  protected const RESPONSE_SUCCESS_CODES = [204];
  protected const REQUEST_STRUCTURE = [
    'id',
  ];

  /**
   * Set project ID.
   */
  public function setProjectId(int $projectID): void {
    $this->setParams(
          [
            'id' => $projectID,
          ]
      );
  }

}
