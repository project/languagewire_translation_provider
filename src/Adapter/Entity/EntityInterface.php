<?php

namespace Drupal\languagewire_translation_provider\Adapter\Entity;

use Drupal\Core\Entity\EntityInterface as DrupalEntityInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;

/**
 * Drupal Entity interface.
 * */
interface EntityInterface {

  /**
   * Get the entity data.
   */
  public function getData(): array;

  /**
   * Get the entity.
   */
  public function getEntity(): DrupalEntityInterface;

  /**
   * Get the entity id.
   */
  public function getEntityId(): string;

  /**
   * Get the entity URL.
   */
  public function getEntityUrl(): string;

  /**
   * Get translation.
   */
  public function getTranslation(LanguageMapping $language): ?EntityInterface;

}
