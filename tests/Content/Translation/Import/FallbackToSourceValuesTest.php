<?php

namespace Drupal\languagewire_translation_provider\Test\Content\Translation\Import;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobInterface;
use Drupal\languagewire_translation_provider\Content\Translation\Import\FallbackToSourceValues;
use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\DocumentStatus;
use Drupal\languagewire_translation_provider\Domain\Project;
use Drupal\languagewire_translation_provider\Domain\ProjectStatus;
use Drupal\languagewire_translation_provider\Test\Builder\JobItemBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\TMGMTContentBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Fall back to source values.
 */
class FallbackToSourceValuesTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Excluded fields are not present in the intermediate array .
   *
   * @test
   */
  public function copiesSourceContentToExcludedFields(): void {
    $job = $this->prophesize(TmgmtJobInterface::class)->reveal();
    $content = (new TMGMTContentBuilder())->withContentId('foo')->withText('bar')->build();
    $jobItem = (new JobItemBuilder())->withContent([$content])->build();
    $data = [];
    $fallback = new FallbackToSourceValues();

    $document = new Document(1, $jobItem, DocumentStatus::uploadingContent());
    $project = new Project($job, '1', ProjectStatus::uploading());
    $result = $fallback->applyTranslationFallback($data, $document, $project);

    $expectedResult = [
      'foo' => [
        '#text' => 'bar',
        '#origin' => 'remote',
      ],
    ];
    $this->assertEquals($expectedResult, $result);
  }

  /**
   * Does Not Overwrite Values From Platform .
   *
   * @test
   */
  public function doesNotOverwriteValuesFromPlatform(): void {
    $data = [
      'foo' => [
        '#text' => 'bar',
        '#origin' => 'remote',
      ],
    ];
    $project = $this->prophesize(Project::class)->reveal();
    $content = (new TMGMTContentBuilder())->withContentId('foo')->withText('NOT bar')->build();
    $jobItem = (new JobItemBuilder())->withContent([$content])->build();
    $fallback = new FallbackToSourceValues();

    $result = $fallback->applyTranslationFallback($data, new Document(1, $jobItem, DocumentStatus::uploadingContent()), $project);

    $this->assertEquals($data, $result);
  }

}
