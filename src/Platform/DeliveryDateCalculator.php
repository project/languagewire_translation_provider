<?php

namespace Drupal\languagewire_translation_provider\Platform;

/**
 * Delivery Date Calculator.
 */
final class DeliveryDateCalculator {
  public const WORDS_PER_DAY = 1500;

  /**
   * Calculate.
   */
  public static function calculateFor(int $wordCount): int {
    return $wordCount == 0
            ? 1
            : (int) ceil($wordCount / self::WORDS_PER_DAY);
  }

}
