<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

/**
 * Draft Document.
 *
 * Draft is used to upload content to LanguageWire platform.
 */
final class DraftDocument extends DocumentBase {
}
