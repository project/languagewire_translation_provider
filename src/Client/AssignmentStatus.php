<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentStatusModel;

/**
 * Assignment Status.
 */
final class AssignmentStatus {

  /**
   * Pending.
   */
  public const PENDING = 0;

  /**
   * Finished.
   */
  public const FINISHED = 1;

  /**
   * Cancelled.
   */
  public const CANCELLED = 2;

  /**
   * Status.
   *
   * @var int
   *   Status.
   */
  private int $status;

  /**
   * Constructs a new AssignmentStatus object.
   *
   * @param int $status
   *   Status.
   */
  public function __construct(int $status) {
    $this->status = $status;
  }

  /**
   * Is pending.
   */
  public function isPending(): bool {
    return $this->status == self::PENDING;
  }

  /**
   * Is finished.
   */
  public function isFinished(): bool {
    return $this->status == self::FINISHED;
  }

  /**
   * Is cancelled.
   */
  public function isCancelled(): bool {
    return $this->status == self::CANCELLED;
  }

  /**
   * Value.
   */
  public function value(): int {
    return $this->status;
  }

  /**
   * From platform status.
   */
  public static function fromPlatformStatus(AssignmentStatusModel $status): self {
    return match ($status->getStatus()) {
      AssignmentStatusModel::IN_PROGRESS => new self(self::PENDING),
      AssignmentStatusModel::FINISHED => new self(self::FINISHED),
      AssignmentStatusModel::CANCELLED => new self(self::CANCELLED),
      default => throw new \InvalidArgumentException("Assignment status {$status->getStatus()} is not recognized."),
    };
  }

}
