<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Account;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Invoicing accounts endpoint.
 */
class InvoicingAccounts extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'invoicing-accounts';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'id',
          'name',
        ],
  ];

}
