<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite;

use Drupal\preview_site\Entity\PreviewSiteBuild as DrupalPreviewSiteBuild;

/**
 * Preview site build.
 */
final class PreviewSiteBuild {

  /**
   * Constructs a new PreviewSiteBuild object.
   *
   * @param \Drupal\preview_site\Entity\PreviewSiteBuild $build
   *   Build.
   */
  public function __construct(private DrupalPreviewSiteBuild $build) {
  }

  /**
   * Get the build ID.
   */
  public function getId(): int {
    return (int) $this->build->id();
  }

  /**
   * Get the build name.
   */
  public function getName(): string {
    return $this->getStrategy()->getId();
  }

  /**
   * Get the build strategy.
   */
  public function getStrategy(): PreviewStrategy {
    return new PreviewStrategy($this->build->getStrategy());
  }

  /**
   * Get the build folder path.
   */
  public function getFolderPath(): string {
    return sprintf('public://%s', $this->getName());
  }

  /**
   * Get temporary artifacts base path.
   */
  public function getTemporaryArtifactsBasePath(): string {
    return $this->build->getStrategy()->getGeneratePlugin()->getArtifactBasePath($this->build);
  }

}
