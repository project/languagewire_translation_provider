<?php

namespace Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary;

use Drupal\languagewire_translation_provider\api\Models\Project\ProjectModel;
use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentModel;
use Drupal\languagewire_translation_provider\api\Models\Project\ProjectStatusModel;
use Drupal\languagewire_translation_provider\Client\ProjectStatus;

/**
 * Project Builder .
 */
class ProjectBuilder {
  /**
   * Correlation Id .
   *
   * @var string Correlation Id
   */
  private $correlationId = 'uuid';

  /**
   * Title .
   *
   * @var string Title
   */
  private $title = 'Foo';

  /**
   * Platform Id .
   *
   * @var int Platform Id
   */
  private $platformId = 1;

  /**
   * Platform Url .
   *
   * @var string Platform Url
   */
  private $platformUrl = 'https://example.com/project/1';

  /**
   * Status .
   *
   * @var string Status
   */
  private $status = ProjectStatusModel::ACTIVE;

  /**
   * Assignments .
   *
   * @var array Assignments
   */
  private $assignments = [];

  /**
   * With Correlation Id .
   */
  public function withAssignment(AssignmentModel $assignment): self {
    $this->assignments[] = $assignment;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): ProjectModel {
    if (empty($this->assignments)) {
      $this->assignments[] = $this->getAssignment();
    }

    return ProjectModel::initialize(
      1,
      $this->platformId,
      $this->title,
      'Briefing',
      'Briefing for experts',
      '123',
      '2021-01-01',
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      ProjectStatusModel::initialize($this->status),
      $this->platformUrl
    );
  }

  /**
   * Get Assignment .
   */
  private function getAssignment(): AssignmentModel {
    return (new AssignmentBuilder())->build();
  }

}
