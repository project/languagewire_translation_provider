<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Platform\Document\UploadedDocument;
use Drupal\languagewire_translation_provider\Test\Builder\Domain\DocumentBuilder;

/**
 * Uploaded Document Builder .
 */
class UploadedDocumentBuilder {

  /**
   * Document .
   *
   * @var \Drupal\languagewire_translation_provider\Domain\Document
   */
  private $document;

  /**
   * Platform Id .
   *
   * @var string
   */
  private $platformId = "99";

  /**
   * Constructs a new UploadedDocumentBuilder object.
   */
  public function __construct() {
    $this->document = (new DocumentBuilder())->build();
  }

  /**
   * With Document .
   */
  public function withDocument(Document $document): self {
    $this->document = $document;
    return $this;
  }

  /**
   * With Platform Id .
   */
  public function withPlatformId(string $platformId): self {
    $this->platformId = $platformId;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): UploadedDocument {
    return new UploadedDocument($this->document, $this->platformId);
  }

}
