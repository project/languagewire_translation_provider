<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

use Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface;

/**
 * Platform Document.
 */
final class PlatformDocument extends DocumentBase {
  /**
   * Document ID.
   *
   * @var string
   */
  private string $documentId;

  /**
   * Constructs a new PlatformDocument object.
   *
   * @param string $documentId
   *   Document ID.
   * @param \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface|null $content
   *   Content.
   * @param string|null $language
   *   Language.
   * @param string $title
   *   Title.
   */
  public function __construct(string $documentId, ?ContentInterface $content, ?string $language, string $title) {
    parent::__construct($content, $language, $title);
    $this->documentId = $documentId;
  }

  /**
   * Get Document Id .
   *
   * @return string
   *   Document ID.
   */
  public function getDocumentId(): string {
    return $this->documentId;
  }

  /**
   * Create new instance from not uploaded document and document platform ID.
   *
   * @param string $documentId
   *   Document platform ID.
   * @param DocumentInterface $document
   *   Document.
   *
   * @return self
   *   New instance.
   */
  public static function fromDocument(string $documentId, DocumentInterface $document): self {
    return new self(
          $documentId,
          $document->getContent(),
          $document->getLanguage(),
          $document->getTitle()
      );
  }

}
