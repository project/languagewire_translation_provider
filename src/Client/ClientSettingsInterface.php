<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\api\Core\Settings;

/**
 * Client Settings.
 */
interface ClientSettingsInterface {

  /**
   * Get.
   */
  public function get(): Settings;

}
