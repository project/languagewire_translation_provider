<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Account;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Project template endpoint.
 */
class ProjectTemplate extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'project-templates/:id';
  protected const METHOD = Method::GET;
  protected const REQUEST_STRUCTURE = [
    'id',
  ];
  protected const RESPONSE_STRUCTURE = [
    'id',
    'name',
    'description',
    'projectName',
    'sourceLanguage',
    'targetLanguages',
    'translationMemoryId',
    'termBaseId',
    'invoicingAccountId',
    'workAreaId',
    'desiredDeadlineDate',
    'desiredDeadlineComment',
    'briefingToLanguageWire',
    'briefingForExperts',
    'referenceFiles',
  ];

  /**
   * Set project template ID.
   */
  public function setProjectTemplateId(int $projectTemplateID): void {
    $this->setParams(['id' => $projectTemplateID]);
  }

}
