<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Platform\Document\ValidDocument;
use Drupal\languagewire_translation_provider\Test\Builder\Domain\DocumentBuilder;

/**
 * Valid Document Builder .
 */
class ValidDocumentBuilder {

  /**
   * Document .
   *
   * @var \Drupal\languagewire_translation_provider\Domain\Document
   */
  private $document;

  /**
   * Constructs a new ValidDocumentBuilder object.
   */
  public function __construct() {
    $this->document = (new DocumentBuilder())->build();
  }

  /**
   * With Document .
   */
  public function withDocument(Document $document): self {
    $this->document = $document;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): ValidDocument {
    return new ValidDocument($this->document);
  }

}
