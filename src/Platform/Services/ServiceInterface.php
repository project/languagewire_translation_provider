<?php

namespace Drupal\languagewire_translation_provider\Platform\Services;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Date;
use Drupal\languagewire_translation_provider\Settings\JobSettings;

/**
 * Service interface.
 */
interface ServiceInterface extends ConfigurationItemInterface {

  /**
   * Get user id.
   */
  public function userId(): int;

  /**
   * Get template id.
   */
  public function templateId(): ?int;

  /**
   * Get translation memory id.
   */
  public function translationMemoryId(): ?int;

  /**
   * Get term base id.
   */
  public function termBaseId(): ?int;

  /**
   * Get invoicing account id.
   */
  public function invoicingAccountId(): int;

  /**
   * Get work area id.
   */
  public function workAreaId(): int;

  /**
   * Get deadline.
   */
  public function deadline(): Date;

  /**
   * Configure job settings.
   */
  public function configure(JobSettings $jobSettings): void;

  /**
   * Is language supported.
   */
  public function isLanguageSupported(LanguageMapping $languageMapping): bool;

}
