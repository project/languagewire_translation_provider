<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Configuration Item interface.
 */
interface ConfigurationItemInterface {

  /**
   * ID.
   */
  public function id(): int;

  /**
   * Name.
   */
  public function name(): string;

}
