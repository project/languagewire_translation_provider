<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\ContentType;

/**
 * Content type.
 */
abstract class ContentTypeAbstract implements ContentTypeInterface {
  protected const CONTENT_TYPE = NULL;

  /**
   * Get content type string.
   */
  public function getContentTypeString() : string {
    return static::CONTENT_TYPE ?? '';
  }

}
