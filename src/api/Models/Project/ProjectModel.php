<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Project;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * Project model.
 */
class ProjectModel extends AbstractModel {

  /**
   * ID.
   *
   * @var int
   */
  protected int $id;

  /**
   * External ID.
   *
   * @var string
   */
  protected string $externalId;

  /**
   * Title.
   *
   * @var string
   */
  protected string $title;

  /**
   * Briefing.
   *
   * @var string
   */
  protected string $briefing;

  /**
   * Briefing for experts.
   *
   * @var string
   */
  protected string $briefingForExperts;

  /**
   * Purchase order number.
   *
   * @var string
   */
  protected string $purchaseOrderNumber;

  /**
   * Deadline.
   *
   * @var string
   */
  protected string $deadline;

  /**
   * Creation date.
   *
   * @var string
   */
  protected string $creationDate;

  /**
   * Finished at.
   *
   * @var string
   */
  protected string $finishedAt;

  /**
   * Template ID.
   *
   * @var int|null
   */
  protected ?int $templateId;

  /**
   * Service ID.
   *
   * @var int|null
   */
  protected ?int $serviceId;

  /**
   * Translation memory ID.
   *
   * @var int|null
   */
  protected ?int $translationMemoryId;

  /**
   * Term base ID.
   *
   * @var int|null
   */
  protected ?int $termBaseId;

  /**
   * Invoicing account ID.
   *
   * @var int
   */
  protected int $invoicingAccountId;

  /**
   * User ID.
   *
   * @var int
   */
  protected int $userId;

  /**
   * Work area ID.
   *
   * @var int
   */
  protected int $workAreaId;

  /**
   * Status.
   *
   * @var \Drupal\languagewire_translation_provider\api\Models\Project\ProjectStatusModel
   */
  protected ProjectStatusModel $status;

  /**
   * Platform link.
   *
   * @var string
   */
  protected string $platformLink;

  /**
   * Files.
   *
   * @var \Drupal\languagewire_translation_provider\api\Models\Files\FileModel[]
   */
  protected array $files;

  /**
   * Reference files.
   *
   * @var \Drupal\languagewire_translation_provider\api\Models\Files\FileModel[]
   */
  protected array $referenceFiles;

  /**
   * Initializes the model.
   */
  public static function initialize(
    int $id,
    string $externalID,
    string $title,
    string $briefing,
    string $briefingForExperts,
    string $purchaseOrderNumber,
    string $deadline,
    ?int $templateID,
    ?int $serviceID,
    ?int $translationMemoryID,
    ?int $termBaseID,
    int $invoicingAccountID,
    int $userID,
    int $workAreaID,
    ProjectStatusModel $status,
    string $platformLink,
    array $files = [],
    array $referenceFiles = []
  ): self {
    $instance = new self();

    $instance->id = $id;
    $instance->externalId = $externalID;
    $instance->title = $title;
    $instance->briefing = $briefing;
    $instance->briefingForExperts = $briefingForExperts;
    $instance->purchaseOrderNumber = $purchaseOrderNumber;
    $instance->deadline = $deadline;
    $instance->templateId = $templateID;
    $instance->serviceId = $serviceID;
    $instance->translationMemoryId = $translationMemoryID;
    $instance->termBaseId = $termBaseID;
    $instance->invoicingAccountId = $invoicingAccountID;
    $instance->userId = $userID;
    $instance->workAreaId = $workAreaID;
    $instance->status = $status;
    $instance->platformLink = $platformLink;
    $instance->files = $files;
    $instance->referenceFiles = $referenceFiles;

    return $instance;
  }

  /**
   * Get ID.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Set ID.
   */
  protected function setId(int $id): void {
    $this->id = $id;
  }

  /**
   * Get external ID.
   */
  public function getExternalId(): string {
    return $this->externalId;
  }

  /**
   * Set external ID.
   */
  protected function setExternalId(string $externalID): void {
    $this->externalId = $externalID;
  }

  /**
   * Get title.
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * Set title.
   */
  protected function setTitle(string $title): void {
    $this->title = $title;
  }

  /**
   * Get briefing.
   */
  public function getBriefing(): string {
    return $this->briefing;
  }

  /**
   * Set briefing.
   */
  protected function setBriefing(string $briefing): void {
    $this->briefing = $briefing;
  }

  /**
   * Get briefing for experts.
   */
  public function getBriefingForExperts(): string {
    return $this->briefingForExperts;
  }

  /**
   * Set briefing for experts.
   */
  protected function setBriefingForExperts(string $briefingForExperts): void {
    $this->briefingForExperts = $briefingForExperts;
  }

  /**
   * Get purchase order number.
   */
  public function getPurchaseOrderNumber(): string {
    return $this->purchaseOrderNumber;
  }

  /**
   * Set purchase order number.
   */
  protected function setPurchaseOrderNumber(string $purchaseOrderNumber): void {
    $this->purchaseOrderNumber = $purchaseOrderNumber;
  }

  /**
   * Get deadline.
   */
  public function getDeadline(): string {
    return $this->deadline;
  }

  /**
   * Set deadline.
   */
  protected function setDeadline(string $deadline): void {
    $this->deadline = $deadline;
  }

  /**
   * Get creation date.
   */
  public function getCreationDate(): string {
    return $this->creationDate;
  }

  /**
   * Set creation date.
   */
  protected function setCreationDate(string $creationDate): void {
    $this->creationDate = $creationDate;
  }

  /**
   * Get finished at.
   */
  public function getFinishedAt(): string {
    return $this->finishedAt;
  }

  /**
   * Set finished at.
   */
  protected function setFinishedAt(string $finishedAt): void {
    $this->finishedAt = $finishedAt;
  }

  /**
   * Get template ID.
   */
  public function getTemplateId(): ?int {
    return $this->templateId;
  }

  /**
   * Set template ID.
   */
  protected function setTemplateId(?int $templateID): void {
    $this->templateId = $templateID;
  }

  /**
   * Get service ID.
   */
  public function getServiceId(): ?int {
    return $this->serviceId;
  }

  /**
   * Set service ID.
   */
  protected function setServiceId(?int $serviceID): void {
    $this->serviceId = $serviceID;
  }

  /**
   * Get translation memory ID.
   */
  public function getTranslationMemoryId(): ?int {
    return $this->translationMemoryId;
  }

  /**
   * Set translation memory ID.
   */
  protected function setTranslationMemoryId(?int $translationMemoryID): void {
    $this->translationMemoryId = $translationMemoryID;
  }

  /**
   * Get term base ID.
   */
  public function getTermBaseId(): ?int {
    return $this->termBaseId;
  }

  /**
   * Set term base ID.
   */
  protected function setTermBaseId(?int $termBaseID): void {
    $this->termBaseId = $termBaseID;
  }

  /**
   * Get invoicing account ID.
   */
  public function getInvoicingAccountId(): int {
    return $this->invoicingAccountId;
  }

  /**
   * Set invoicing account ID.
   */
  protected function setInvoicingAccountId(int $invoicingAccountID): void {
    $this->invoicingAccountId = $invoicingAccountID;
  }

  /**
   * Get user ID.
   */
  public function getUserId(): int {
    return $this->userId;
  }

  /**
   * Set user ID.
   */
  protected function setUserId(int $userID): void {
    $this->userId = $userID;
  }

  /**
   * Get work area ID.
   */
  public function getWorkAreaId(): int {
    return $this->workAreaId;
  }

  /**
   * Set work area ID.
   */
  protected function setWorkAreaId(int $workAreaID): void {
    $this->workAreaId = $workAreaID;
  }

  /**
   * Get status.
   */
  public function getStatus(): ProjectStatusModel {
    return $this->status;
  }

  /**
   * Set status.
   */
  protected function setStatus(ProjectStatusModel $status): void {
    $this->status = $status;
  }

  /**
   * Get platform link.
   */
  public function getPlatformLink(): string {
    return $this->platformLink;
  }

  /**
   * Set platform link.
   */
  protected function setPlatformLink(string $platformLink): void {
    $this->platformLink = $platformLink;
  }

  /**
   * Get files.
   *
   * @return \Drupal\languagewire_translation_provider\api\Models\Files\FileModel[]
   *   Files.
   */
  public function getFiles(): array {
    return $this->files;
  }

  /**
   * Set files.
   *
   * @param \Drupal\languagewire_translation_provider\api\Models\Files\FileModel[] $files
   *   Files.
   */
  protected function setFiles(array $files): void {
    $this->files = $files;
  }

  /**
   * Get reference files.
   *
   * @return \Drupal\languagewire_translation_provider\api\Models\Files\FileModel[]
   *   Reference files.
   */
  public function getReferenceFiles(): array {
    return $this->referenceFiles;
  }

  /**
   * Set reference files.
   *
   * @param \Drupal\languagewire_translation_provider\api\Models\Files\FileModel[] $referenceFiles
   *   Reference files.
   */
  protected function setReferenceFiles(array $referenceFiles): void {
    $this->referenceFiles = $referenceFiles;
  }

}
