<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Variable Placeholder Prefixing Convention interface.
 */
interface VariablePlaceholderPrefixingConventionInterface {
  public const TYPE_SECURE = 'secure';
  public const TYPE_SECURE_WITH_PLACEHOLDER = 'secure_placeholder';
  public const TYPE_INSECURE = 'insecure';

  /**
   * Prefix.
   */
  public function prefix(string $variableName, string $type): string;

}
