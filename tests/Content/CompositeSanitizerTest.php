<?php

namespace Drupal\languagewire_translation_provider\test\Content;

use Drupal\languagewire_translation_provider\Content\CompositeSanitizer;
use Drupal\languagewire_translation_provider\Content\SanitizerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Composite Sanitizer Test .
 */
class CompositeSanitizerTest extends TestCase {

  /**
   * Sanitizes Content For All Sanitizers .
   *
   * @test
   */
  public function sanitizesContentForAllSanitizers() {
    $content = '1 plus 1 minus 0 equals 2';
    $sanitizer = $this->compositeSanitizer();

    $sanitized = $sanitizer->sanitize($content);

    $this->assertSame('One PLUS One MINUS Zero EQUALS Two', $sanitized);
  }

  /**
   * Desanitizes Content For All Sanitizers .
   *
   * @test
   */
  public function desanitizesContentForAllSanitizers() {
    $content = 'One PLUS One MINUS Zero EQUALS Two';
    $sanitizer = $this->compositeSanitizer();

    $desanitized = $sanitizer->desanitize($content);

    $this->assertSame('1 plus 1 minus 0 equals 2', $desanitized);
  }

  /**
   * Composite Sanitizer .
   */
  private function compositeSanitizer() {
    return new CompositeSanitizer($this->uppercaseSanitizer(), $this->numberSanitizer());
  }

  /**
   * Uppercase Sanitizer .
   */
  private function uppercaseSanitizer() {
    return new class implements SanitizerInterface
        {
      /**
       * Sanitize .
       */

      /**
       * Sanitize .
       */
      public function sanitize(string $content): string {
        return strtoupper($content);
      }

      /**
       * Desanitize .
       */

      /**
       * Desanitize .
       */
      public function desanitize(string $content): string {
        return strtolower($content);
      }

    };
  }

  /**
   * Number Sanitizer .
   */
  private function numberSanitizer() {
    return new class implements SanitizerInterface
        {
      /**
       * Sanitize .
       */

      /**
       * Sanitize .
       */
      public function sanitize(string $content): string {
        return str_replace([0, 1, 2], ['Zero', 'One', 'Two'], $content);
      }

      /**
       * Desanitize .
       */

      /**
       * Desanitize .
       */
      public function desanitize(string $content): string {
        return str_replace(['Zero', 'One', 'Two'], [0, 1, 2], $content);
      }

    };
  }

}
