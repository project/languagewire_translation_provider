<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\api\Core\Settings;
use Drupal\languagewire_translation_provider\Client\LanguageWireClient;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientFactory;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientSettings;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientSettingsFactory;
use Drupal\languagewire_translation_provider\Client\ProjectSpecificationFactory;
use PHPUnit\Framework\TestCase;

/**
 * Language Wire Client Factory Test .
 */
class LanguageWireClientFactoryTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * When Given Tmgmt Job Translator Then It Creates Client Object .
   *
   * @test
   */
  public function whenGivenTmgmtJobTranslatorThenItCreatesClientObject(): void {
    $translator = $this->prophesize(TmgmtTranslatorInterface::class)->reveal();

    $clientSettings = $this->prophesize(LanguageWireClientSettings::class);
    $projectSpecificationFactory = $this->prophesize(ProjectSpecificationFactory::class);

    $clientSettings->get()->willReturn(new Settings('http://example.com', 'token'));
    $clientSettingsFactory = $this->prophesize(LanguageWireClientSettingsFactory::class);
    $clientSettingsFactory->createForTmgmtTranslator($translator)->willReturn($clientSettings->reveal());

    $sut = new LanguageWireClientFactory($clientSettingsFactory->reveal(), $projectSpecificationFactory->reveal());

    $client = $sut->createForTmgmtTranslator($translator);

    $this->assertInstanceOf(LanguageWireClient::class, $client);
  }

}
