<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Assignments;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * Assignment model.
 */
class AssignmentModel extends AbstractModel {
  /**
   * Assignment unique ID.
   *
   * @var string
   */
  protected string $assignmentId;

  /**
   * Assignment source language.
   *
   * @var string
   */
  protected string $sourceLanguage;


  /**
   * Assignment target language.
   *
   * @var string
   */
  protected string $targetLanguage;


  /**
   * Assignment status code.
   *
   * @var AssignmentStatusModel
   */
  protected AssignmentStatusModel $status;

  /**
   * A list of AssignmentDocument objects.
   *
   * @var AssignmentDocumentModel[]
   */
  protected array $assignmentDocuments = [];

  /**
   * Creates new instance of an Assignment.
   *
   * @param string $assignmentID
   *   The assignment's unique ID.
   * @param string $sourceLanguage
   *   The assignment's source language.
   * @param string $targetLanguage
   *   The assignment's target language.
   * @param AssignmentStatusModel $status
   *   The assignment's status code.
   * @param AssignmentDocumentModel[] $documents
   *   A list of the AssignmentDocument objects.
   */
  public function __construct(
    string $assignmentID,
    string $sourceLanguage,
    string $targetLanguage,
    AssignmentStatusModel $status,
    array $documents
  ) {
    $this->assignmentId = $assignmentID;
    $this->sourceLanguage = $sourceLanguage;
    $this->targetLanguage = $targetLanguage;
    $this->status = $status;
    $this->assignmentDocuments = $documents;
  }

  /**
   * Returns the assignment's unique ID.
   *
   * @return string
   *   The assignment's unique ID.
   */
  public function getAssignmentId(): string {
    return $this->assignmentId;
  }

  /**
   * Returns the assignment's source language.
   *
   * @return string
   *   The assignment's source language.
   */
  public function getSourceLanguage(): string {
    return $this->sourceLanguage;
  }

  /**
   * Returns the assignment's target language.
   *
   * @return string
   *   The assignment's target language.
   */
  public function getTargetLanguage(): string {
    return $this->targetLanguage;
  }

  /**
   * Returns the assignment's status code.
   *
   * @return AssignmentStatusModel
   *   The assignment's status code.
   */
  public function getStatus(): AssignmentStatusModel {
    return $this->status;
  }

  /**
   * Returns a list of the AssignmentDocument objects.
   *
   * @return AssignmentDocumentModel[]
   *   A list of the AssignmentDocument objects.
   */
  public function getDocuments(): array {
    return $this->assignmentDocuments;
  }

}
