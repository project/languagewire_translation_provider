<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientSettings;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientSettingsFactory;
use PHPUnit\Framework\TestCase;

/**
 * LanguageWire Client Settings Factory Test .
 */
class LanguageWireClientSettingsFactoryTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * System .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  private $system;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->system = $this->prophesize(SystemInterface::class);
  }

  /**
   * When Given Tmgmt Job Translator Then It Creates Settings Object .
   *
   * @test
   */
  public function whenGivenTmgmtJobTranslatorThenItCreatesSettingsObject(): void {
    $sut = new LanguageWireClientSettingsFactory($this->system->reveal());

    $settings = $sut->createDummy();

    $this->assertInstanceOf(LanguageWireClientSettings::class, $settings);
  }

}
