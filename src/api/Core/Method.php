<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core;

/**
 * HTTP method.
 */
class Method {
  public const GET = 'GET';
  public const POST = 'POST';
  public const PATH = 'PATCH';
  public const DELETE = 'DELETE';

}
