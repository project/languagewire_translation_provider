<?php

namespace Drupal\languagewire_translation_provider\Settings;

/**
 * Order type.
 */
final class OrderType {
  /**
   * Order type.
   */
  public const ORDER = 'order';

  /**
   * Quote order type.
   */
  public const QUOTATION_ORDER = 'quotation_order';

  /**
   * Get all order types.
   */
  public static function getAll(): array {
    return [
      self::ORDER           => 'Project',
      self::QUOTATION_ORDER => 'Quote',
    ];
  }

  /**
   * Check if the key is an order type.
   */
  public static function isQuotationOrder(string $key): bool {
    return $key === self::QUOTATION_ORDER;
  }

}
