<?php

namespace Drupal\languagewire_translation_provider\Content;

use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\Frame;

/**
 * Translated Content.
 */
final class TranslatedContent {

  /**
   * Text.
   *
   * @var string
   */
  private string $text;

  /**
   * Content ID.
   *
   * @var string
   */
  private string $contentId;

  /**
   * Constructs a new TranslatedContent object.
   *
   * @param string $text
   *   Text.
   * @param string $contentId
   *   Content ID.
   */
  public function __construct(string $text, string $contentId) {
    $this->text = $text;
    $this->contentId = $contentId;
  }

  /**
   * Text.
   */
  public function translatedContent(): string {
    return $this->text;
  }

  /**
   * Content ID.
   */
  public function contentId(): string {
    return $this->contentId;
  }

  /**
   * From Platform Frame.
   */
  public static function fromPlatformFrame(Frame $platformFrame): self {
    return new self($platformFrame->getText(), $platformFrame->getMetadata()['contentId']);
  }

  /**
   * Desanitize.
   */
  public function desanitize(SanitizerInterface $sanitizer): self {
    $clone = clone $this;
    $clone->text = $sanitizer->desanitize($this->text);

    return $clone;
  }

}
