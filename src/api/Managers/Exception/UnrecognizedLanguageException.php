<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Managers\Exception;

/**
 * Unrecognized Language exception.
 */
final class UnrecognizedLanguageException extends \InvalidArgumentException {

  /**
   * Creates new instance with unrecognized language code.
   *
   * @param string $languageCode
   *   Language code.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromUnrecognizedLanguage(string $languageCode): self {
    return new UnrecognizedLanguageException(sprintf('Language code %s could not be recognized', $languageCode));
  }

  /**
   * Creates new instance with unrecognized language codes.
   *
   * @param string[] $languageCodes
   *   Language codes.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromUnrecognizedLanguages(array $languageCodes): UnrecognizedLanguageException {
    return (count($languageCodes) == 1) ?
            self::fromUnrecognizedLanguage($languageCodes[0]) :
            new UnrecognizedLanguageException(sprintf('Language codes [%s] could not be recognized', implode(",", $languageCodes)));
  }

}
