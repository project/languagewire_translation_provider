<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\api\Models\Project\ProjectModel;
use Drupal\languagewire_translation_provider\api\Models\Project\ProjectStatusModel;
use Drupal\languagewire_translation_provider\Platform\Services\DemoTranslation;
use Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface;

/**
 * Api Project Specification.
 */
final class LanguageWireProjectSpecification implements ProjectSpecificationInterface {

  /**
   * Source documents.
   *
   * @var int[]
   */
  protected array $sourceDocuments;

  /**
   * Project UUID.
   *
   * @var string
   */
  private string $projectUuid;

  /**
   * Title.
   *
   * @var string
   */
  protected string $title;

  /**
   * Service.
   *
   * @var \Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface
   */
  private ServiceInterface $service;

  /**
   * Briefing.
   *
   * @var string
   */
  protected string $briefing;

  /**
   * Constructs a new LanguageWireProjectSpecification object.
   *
   * @param int[] $sourceDocuments
   *   Source documents.
   * @param string $projectUuid
   *   Project UUID.
   * @param string $title
   *   Title.
   * @param \Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface $service
   *   Service.
   * @param string $briefing
   *   Briefing.
   */
  public function __construct(
    array $sourceDocuments,
    string $projectUuid,
    string $title,
    ServiceInterface $service,
    string $briefing
  ) {
    $this->sourceDocuments = $sourceDocuments;
    $this->projectUuid = $projectUuid;
    $this->title = $title;
    $this->service = $service;
    $this->briefing = $briefing;
  }

  /**
   * {@inheritdoc}
   */
  public function toSpecification(ClientInterface $client): ProjectModel {
    $translationMemoryID = $this->service->translationMemoryId();
    $termBaseID = $this->service->termBaseId();

    return ProjectModel::initialize(
      $this->service->id(),
      $this->projectUuid,
      $this->title,
      $this->briefing,
      (string) NULL,
      (string) NULL,
      $this->service->deadline()->toUtc()->format('Y-m-d'),
      $this->service->templateId(),
      $this->getServiceId($client),
      $translationMemoryID === 0 ? NULL : $translationMemoryID,
      $termBaseID === 0 ? NULL : $termBaseID,
      $this->service->invoicingAccountId(),
      $this->service->userId(),
      $this->service->workAreaId(),
      ProjectStatusModel::initialize(ProjectStatusModel::DRAFT),
      (string) NULL
    );
  }

  /**
   * Get service ID.
   */
  private function getServiceId(ClientInterface $client): int {
    if (!$this->service instanceof DemoTranslation) {
      return $this->service->id();
    }

    $platformServices = $client->getProducts();
    return $platformServices[0]->id();
  }

  /**
   * {@inheritdoc}
   */
  public function projectUuid(): string {
    return $this->projectUuid;
  }

  /**
   * {@inheritdoc}
   */
  public function title(): string {
    return $this->title;
  }

}
