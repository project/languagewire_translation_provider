<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

/**
 * Media Types supported by LanguageWire platform.
 */
abstract class MediaType {
  public const ADOBE_ICML = 'application/vnd.adobe.incopy';
  public const ADOBE_IDML = 'application/vnd.adobe.indesign-idml-package';
  public const ADOBE_INDD = 'application/vnd.adobe.indesign';
  public const HTML = 'text/html';
  public const JSON = 'application/json';
  public const MICROSOFT_DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
  public const MICROSOFT_PPTX = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
  public const MICROSOFT_RESX = 'text/microsoft-resx';
  public const MICROSOFT_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  public const MONOLINGUAL_XLIFF = 'application/x-xliff+xml';
  public const MONOLINGUAL_STDXLIFF = 'application/x-sdlxliff+xml';
  public const PLAIN_TEXT = 'text/plain';
  public const UDF = 'application/vnd.lw.api+json';
  public const XML = 'application/xml';
  public const BINARY = 'application/binary';

  /**
   * Supported Types .
   *
   * @var string[]
   */
  private static array $supportedTypes = [
    'icml'     => self::ADOBE_ICML,
    'idml'     => self::ADOBE_IDML,
    'indd'     => self::ADOBE_INDD,
    'html'     => self::HTML,
    'json'     => self::JSON,
    'docx'     => self::MICROSOFT_DOCX,
    'pptx'     => self::MICROSOFT_PPTX,
    'resx'     => self::MICROSOFT_RESX,
    'xlsx'     => self::MICROSOFT_XLSX,
    'xliff'    => self::MONOLINGUAL_STDXLIFF,
    'sdlxliff' => self::MONOLINGUAL_XLIFF,
    'txt'      => self::PLAIN_TEXT,
    'udf'      => self::UDF,
    'xml'      => self::XML,
    'bin'      => self::BINARY,
  ];

  /**
   * Is Supported .
   */
  public static function isSupported(string $mediaType): bool {
    return in_array($mediaType, self::$supportedTypes);
  }

  /**
   * Get Supported Types For Upload .
   *
   * @return string[]
   *   Supported types for upload.
   */
  public static function getSupportedTypesForUpload(): array {
    $supportedTypes = self::$supportedTypes;
    unset($supportedTypes[array_search(self::JSON, $supportedTypes)]);
    return $supportedTypes;
  }

  /**
   * Get Extension From Type .
   */
  public static function getExtensionFromType(string $mediaType): ?string {
    return array_search($mediaType, self::$supportedTypes) ?: NULL;
  }

}
