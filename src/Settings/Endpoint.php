<?php

namespace Drupal\languagewire_translation_provider\Settings;

use Laminas\Diactoros\Uri;
use Psr\Http\Message\UriInterface;

/**
 * LanguageWire API endpoint.
 */
final class Endpoint {
  private const PRODUCTION = 'https://api.languagewire.com/project/v1';

  /**
   * Drupal settings.
   *
   * @var SettingsInterface
   */
  private SettingsInterface $drupalSettings;

  /**
   * Constructs a new Endpoint object.
   *
   * @param \Drupal\languagewire_translation_provider\Settings\SettingsInterface $drupalSettings
   *   Drupal settings.
   */
  public function __construct(SettingsInterface $drupalSettings) {
    $this->drupalSettings = $drupalSettings;
  }

  /**
   * Get endpoint.
   */
  public function get(): UriInterface {
    $configuredEndpoint = $this->drupalSettings->getConfig('languagewire/endpoint');

    return new Uri($configuredEndpoint ?: self::PRODUCTION);
  }

}
