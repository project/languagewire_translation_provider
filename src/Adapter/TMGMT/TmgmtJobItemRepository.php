<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

use Drupal\tmgmt\Entity\JobItem;

/**
 * Drupal TMGMT Job Item.
* */
final class TmgmtJobItemRepository implements JobItemRepositoryInterface {

  /**
   * Get job item by ID.
   */
  public function getById(int $jobItemId): ?TmgmtJobItemInterface {
    $jobItem = JobItem::load($jobItemId);
    return $jobItem ? new TmgmtJobItem($jobItem) : NULL;
  }

}
