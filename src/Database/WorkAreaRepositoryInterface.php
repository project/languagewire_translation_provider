<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * Work Area Repository interface.
 */
interface WorkAreaRepositoryInterface extends ConfigurationItemRepositoryInterface {

  /**
   * Save all.
   *
   * @param \Drupal\languagewire_translation_provider\Client\WorkArea[] $items
   *   Work area items.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function saveAll(array $items, TmgmtTranslatorInterface $translator): void;

  /**
   * Get all.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   *
   * @return \Drupal\languagewire_translation_provider\Client\WorkArea[]
   *   Work area items.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

}
