<?php

namespace Drupal\languagewire_translation_provider;

/**
 * Date connector object.
* */
final class Date {

  /**
   * Date.
   *
   * @var \DateTimeImmutable
   */
  private \DateTimeImmutable $date;

  /**
   * Constructs a new Date object.
   *
   * @param \DateTimeImmutable $date
   *   Date.
   */
  public function __construct(\DateTimeImmutable $date) {
    $this->date = $date;
  }

  /**
   * To utc.
   */
  public function toUtc(): \DateTimeImmutable {
    return $this->date->setTimezone(new \DateTimeZone('UTC'));
  }

}
