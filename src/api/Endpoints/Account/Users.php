<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Account;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * List users endpoint.
 */
class Users extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'users';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'id',
          'firstName',
          'lastName',
        ],
  ];

}
