<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Services;

use Drupal\languagewire_translation_provider\api\Models\LanguageModel;

/**
 * Language Service.
 */
final class LanguageService {

  /**
   * Is supported.
   *
   * @param string $language
   *   Language.
   *
   * @return bool
   *   If is supported.
   */
  public static function isSupported(string $language): bool {
    // Start accepting all languages.
    return $language !== '';
  }

  /**
   * Is supported by MT.
   *
   * @param string $language
   *   Language.
   *
   * @return bool
   *   If is supported.
   */
  public static function isSupportedByMachineTranslation(string $language): bool {
    // Start accepting all languages.
    return $language !== '';
  }

  /**
   * Platform languages.
   *
   * @return string[]
   *   Platform languages supported.
   */
  public static function platformLanguages(): array {
    return array_keys(LanguageModel::$englishLabels);
  }

  /**
   * MT languages.
   *
   * @return string[]
   *   MT translation languages supported
   */
  public static function machineTranslationLanguages(): array {
    return [
      LanguageModel::AFRIKAANS,
      LanguageModel::ARABIC_UAE,
      LanguageModel::ARABIC_BAHRAIN,
      LanguageModel::ARABIC_ALGERIA,
      LanguageModel::ARABIC_EGYPT,
      LanguageModel::ARABIC_JORDAN,
      LanguageModel::ARABIC_KUWAIT,
      LanguageModel::ARABIC_LEBANON,
      LanguageModel::ARABIC_LIBYA,
      LanguageModel::ARABIC_MOROCCO,
      LanguageModel::ARABIC_OMAN,
      LanguageModel::ARABIC_QATAR,
      LanguageModel::ARABIC_SAUDI_ARABIA,
      LanguageModel::ARABIC_SYRIA,
      LanguageModel::ARABIC_TUNISIA,
      LanguageModel::ARABIC_YEMEN,
      LanguageModel::BULGARIAN,
      LanguageModel::CATALAN,
      LanguageModel::CZECH,
      LanguageModel::DANISH,
      LanguageModel::GERMAN_AUSTRIA,
      LanguageModel::GERMAN_SWITZERLAND,
      LanguageModel::GERMAN_GERMANY,
      LanguageModel::GERMAN_LIECHTENSTEIN,
      LanguageModel::GERMAN_LUXEMBOURG,
      LanguageModel::GREEK,
      LanguageModel::ENGLISH_AUSTRALIA,
      LanguageModel::ENGLISH_BELIZE,
      LanguageModel::ENGLISH_CANADA,
      LanguageModel::ENGLISH_UNITED_KINGDOM,
      LanguageModel::ENGLISH_IRELAND,
      LanguageModel::ENGLISH_JAMAICA,
      LanguageModel::ENGLISH_NEW_ZEALAND,
      LanguageModel::ENGLISH_TRINIDAD_TOBAGO,
      LanguageModel::ENGLISH_UNITED_STATES,
      LanguageModel::ENGLISH_SOUTH_AFRICA,
      LanguageModel::SPANISH_ARGENTINA,
      LanguageModel::SPANISH_BOLIVIA,
      LanguageModel::SPANISH_CHILE,
      LanguageModel::SPANISH_COLOMBIA,
      LanguageModel::SPANISH_COSTA_RICA,
      LanguageModel::SPANISH_DOMINICAN_REPUBLIC,
      LanguageModel::SPANISH_ECUADOR,
      LanguageModel::SPANISH_SPAIN,
      LanguageModel::SPANISH_GUATEMALA,
      LanguageModel::SPANISH_HONDURAS,
      LanguageModel::SPANISH_MEXICO,
      LanguageModel::SPANISH_NICARAGUA,
      LanguageModel::SPANISH_PANAMA,
      LanguageModel::SPANISH_PERU,
      LanguageModel::SPANISH_PUERTO_RICO,
      LanguageModel::SPANISH_PARAGUAY,
      LanguageModel::SPANISH_EL_SALVADOR,
      LanguageModel::SPANISH_URUGUAY,
      LanguageModel::SPANISH_BOLIVARIAN_REPUBLIC_OF_VENEZUELA,
      LanguageModel::ESTONIAN,
      LanguageModel::PERSIAN,
      LanguageModel::FINNISH,
      LanguageModel::FRENCH_BELGIUM,
      LanguageModel::FRENCH_CANADA,
      LanguageModel::FRENCH_SWITZERLAND,
      LanguageModel::FRENCH_FRANCE,
      LanguageModel::FRENCH_LUXEMBOURG,
      LanguageModel::HEBREW,
      LanguageModel::HINDI,
      LanguageModel::CROATIAN,
      LanguageModel::HUNGARIAN,
      LanguageModel::INDONESIAN,
      LanguageModel::ITALIAN_SWITZERLAND,
      LanguageModel::ITALIAN_ITALY,
      LanguageModel::JAPANESE,
      LanguageModel::KOREAN,
      LanguageModel::LITHUANIAN,
      LanguageModel::LATVIAN,
      LanguageModel::MALAY,
      LanguageModel::MALTESE,
      LanguageModel::NORWEGIAN_BOKMAL,
      LanguageModel::DUTCH_BELGIUM,
      LanguageModel::DUTCH_NETHERLANDS,
      LanguageModel::NORWEGIAN_NYNORSK,
      LanguageModel::POLISH,
      LanguageModel::PORTUGUESE_BRAZIL,
      LanguageModel::PORTUGUESE_PORTUGAL,
      LanguageModel::ROMANIAN,
      LanguageModel::RUSSIAN,
      LanguageModel::SLOVAK,
      LanguageModel::SLOVENIAN,
      LanguageModel::SERBIAN_CYRILLIC,
      LanguageModel::SERBIAN_LATIN,
      LanguageModel::SWEDISH_FINLAND,
      LanguageModel::SWEDISH_SWEDEN,
      LanguageModel::THAI,
      LanguageModel::TURKISH,
      LanguageModel::UKRAINIAN,
      LanguageModel::URDU,
      LanguageModel::VIETNAMESE,
      LanguageModel::CHINESE_SIMPLIFIED_CHINA,
      LanguageModel::CHINESE_TRADITIONAL_HONG_KONG_SAR,
      LanguageModel::CHINESE_SIMPLIFIED_SINGAPORE,
      LanguageModel::CHINESE_TRADITIONAL_TAIWAN,
    ];
  }

}
