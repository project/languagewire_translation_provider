<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Adapter on the API Project.
 */
class Project {

  /**
   * Platform project id.
   *
   * @var int
   */
  private int $platformId;

  /**
   * Platform project url.
   *
   * @var string
   */
  private string $platformUrl;

  /**
   * Project status.
   *
   * @var ProjectStatus
   */
  private ProjectStatus $status;

  /**
   * Project assignments.
   *
   * @var Assignment[]
   */
  private array $assignments;

  public function __construct(int $platformId, string $platformUrl, string $status, array $assignments) {
    $this->platformId = $platformId;
    $this->platformUrl = $platformUrl;
    $this->status = ProjectStatus::fromPlatformStatusValue($status);
    $this->assignments = $assignments;
  }

  /**
   * ID.
   */
  public function id(): int {
    return $this->platformId;
  }

  /**
   * Platform URL.
   */
  public function platformUrl(): string {
    return $this->platformUrl;
  }

  /**
   * Status.
   */
  public function status(): ProjectStatus {
    return $this->status;
  }

  /**
   * Assignments.
   *
   * @return Assignment[]
   *   The assignments.
   */
  public function assignments(): array {
    return $this->assignments;
  }

}
