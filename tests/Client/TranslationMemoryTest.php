<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Client\TranslationMemory;
use PHPUnit\Framework\TestCase;

/**
 * Translation Memory Test .
 */
class TranslationMemoryTest extends TestCase {

  /**
   * It Creates New Object Instance From Platform Translation Memory .
   *
   * @test
   */
  public function itCreatesNewObjectInstanceFromPlatformTranslationMemory(): void {
    $platformTranslationMemory = ['id' => 1, 'name' => 'Translation Memory'];
    $translationMemory = TranslationMemory::fromPlatformTranslationMemory($platformTranslationMemory);

    $this->assertSame(1, $translationMemory->id());
    $this->assertSame('Translation Memory', $translationMemory->name());
  }

}
