<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Helpers;

use function random_bytes;

/**
 * Strings helper functions.
 */
class Strings {

  /**
   * Clean up string.
   */
  public static function cleanUp(string $cleanUp): string {
    $cleanUp = preg_replace('/[^\w\s\d\-_.]/', '', $cleanUp);
    return preg_replace('/\b(\d+)\b/', '${$1}', $cleanUp);
  }

  /**
   * Generate UUID v4.
   *
   * @throws \Random\RandomException
   */
  public static function uuid4(): string {
    $data = random_bytes(16);
    // Set version to 0100.
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10.
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
  }

}
