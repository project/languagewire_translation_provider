<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\api\Models\Project\ProjectModel;

/**
 * Project Specification interface.
 */
interface ProjectSpecificationInterface {

  /**
   * Get project uuid.
   */
  public function projectUuid(): string;

  /**
   * Get project title.
   */
  public function title(): string;

  /**
   * Convert to specification.
   */
  public function toSpecification(ClientInterface $client): ProjectModel;

}
