<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models\Files;

use Drupal\languagewire_translation_provider\api\Models\AbstractModel;

/**
 * File metadata model.
 */
class FileMetadataModel extends AbstractModel {

  /**
   * File ID.
   *
   * @var int
   */
  protected int $fileId;

  /**
   * File name.
   *
   * @var string
   */
  protected string $fileName;

  /**
   * Content type.
   *
   * @var string
   */
  protected string $contentType;

  /**
   * File type.
   *
   * @var string
   */
  protected string $fileType;

  /**
   * File size.
   *
   * @var string
   */
  protected string $fileSize;

  /**
   * Project ID.
   *
   * @var string
   */
  protected string $projectId;

  /**
   * Upload date.
   *
   * @var string
   */
  protected string $uploadDate;

  /**
   * Last access date.
   *
   * @var string
   */
  protected string $lastAccessDate;

  /**
   * Initialize the model.
   */
  public static function initialize(
    int $fileID,
    string $fileName,
    string $contentType,
    string $fileType,
    string $fileSize,
    string $projectID,
    string $uploadDate,
    string $lastAccessDate
  ): self {
    $instance = new self();

    $instance->fileId = $fileID;
    $instance->fileName = $fileName;
    $instance->contentType = $contentType;
    $instance->fileType = $fileType;
    $instance->fileSize = $fileSize;
    $instance->projectId = $projectID;
    $instance->uploadDate = $uploadDate;
    $instance->lastAccessDate = $lastAccessDate;

    return $instance;
  }

  /**
   * Get the file ID.
   */
  public function getFileId(): int {
    return $this->fileId;
  }

  /**
   * Set the file ID.
   */
  protected function setFileId(int $fileID): void {
    $this->fileId = $fileID;
  }

  /**
   * Get the file name.
   */
  public function getFileName(): string {
    return $this->fileName;
  }

  /**
   * Set the file name.
   */
  protected function setFileName(string $fileName): void {
    $this->fileName = $fileName;
  }

  /**
   * Get the content type.
   */
  public function getContentType(): string {
    return $this->contentType;
  }

  /**
   * Set the content type.
   */
  protected function setContentType(string $contentType): void {
    $this->contentType = $contentType;
  }

  /**
   * Get the file type.
   */
  public function getFileType(): string {
    return $this->fileType;
  }

  /**
   * Set the file type.
   */
  protected function setFileType(string $fileType): void {
    $this->fileType = $fileType;
  }

  /**
   * Get the file size.
   */
  public function getFileSize(): string {
    return $this->fileSize;
  }

  /**
   * Set the file size.
   */
  protected function setFileSize(string $fileSize): void {
    $this->fileSize = $fileSize;
  }

  /**
   * Get the project ID.
   */
  public function getProjectId(): string {
    return $this->projectId;
  }

  /**
   * Set the project ID.
   */
  protected function setProjectId(string $projectID): void {
    $this->projectId = $projectID;
  }

  /**
   * Get the upload date.
   */
  public function getUploadDate(): string {
    return $this->uploadDate;
  }

  /**
   * Set the upload date.
   */
  protected function setUploadDate(string $uploadDate): void {
    $this->uploadDate = $uploadDate;
  }

  /**
   * Get the last access date.
   */
  public function getLastAccessDate(): string {
    return $this->lastAccessDate;
  }

  /**
   * Set the last access date.
   */
  protected function setLastAccessDate(string $lastAccessDate): void {
    $this->lastAccessDate = $lastAccessDate;
  }

}
