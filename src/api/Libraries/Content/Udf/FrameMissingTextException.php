<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content\Udf;

/**
 * Class MissingTextException.
 */
final class FrameMissingTextException extends \OutOfBoundsException {
}
