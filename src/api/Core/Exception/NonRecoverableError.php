<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\Exception;

/**
 * Non recoverable error.
 */
class NonRecoverableError extends \Exception {
  // Silence is gold.
}
