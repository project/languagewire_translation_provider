<?php

namespace Drupal\languagewire_translation_provider\Test\Platform\Document;

use Drupal\languagewire_translation_provider\Platform\Document\InvalidDocument;
use Drupal\languagewire_translation_provider\Test\Builder\Domain\DocumentBuilder;
use Exception;
use PHPUnit\Framework\TestCase;

/**
 * Invalid Document Test .
 */
class InvalidDocumentTest extends TestCase {

  /**
   * When Creating From Upload Exception Then Creates New Instance Is Made .
   *
   * @test
   * @dataProvider uploadExceptionDataProvider
   */
  public function whenCreatingFromUploadExceptionThenCreatesNewInstanceIsMade(Exception $exception, string $expectedMessage) {
    $domainDocument = (new DocumentBuilder())->build();
    $document = InvalidDocument::fromUploadException($exception, $domainDocument);

    $this->assertInstanceOf(InvalidDocument::class, $document);
    $this->assertEquals($domainDocument, $document->getDocument());

    $expectedReason = sprintf($expectedMessage, $domainDocument->getTitle());
    $this->assertSame($expectedReason, $document->getReason());
  }

  /**
   * Upload Exception Data Provider .
   */
  public function uploadExceptionDataProvider(): array {
    return [
          [
            new Exception(''),
            'Could not upload "%s" item to LanguageWire platform: .',
          ],
    ];
  }

}
