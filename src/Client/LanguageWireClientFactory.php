<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * LanguageWire Client Factory.
 */
class LanguageWireClientFactory {

  /**
   * Client settings factory.
   *
   * @var LanguageWireClientSettingsFactory
   */
  private LanguageWireClientSettingsFactory $clientSettingsFactory;

  /**
   * Project specification factory.
   *
   * @var ProjectSpecificationFactory
   */
  private ProjectSpecificationFactory $projectSpecificationFactory;

  /**
   * Constructs a new LanguageWireClientFactory object.
   *
   * @param \Drupal\languagewire_translation_provider\Client\LanguageWireClientSettingsFactory $clientSettingsFactory
   *   Client settings factory.
   * @param \Drupal\languagewire_translation_provider\Client\ProjectSpecificationFactory $projectSpecificationFactory
   *   Project specification factory.
   */
  public function __construct(LanguageWireClientSettingsFactory $clientSettingsFactory, ProjectSpecificationFactory $projectSpecificationFactory) {
    $this->clientSettingsFactory = $clientSettingsFactory;
    $this->projectSpecificationFactory = $projectSpecificationFactory;
  }

  /**
   * Create for TMGMT translator.
   *
   * @throws \Drupal\languagewire_translation_provider\api\Core\Exception\Error
   */
  public function createForTmgmtTranslator(TmgmtTranslatorInterface $translator): LanguageWireClient {
    $settings = $this->clientSettingsFactory->createForTmgmtTranslator($translator);
    return new LanguageWireClient($settings->get(), $this->projectSpecificationFactory);
  }

  /**
   * Create dummy for TMGMT translator.
   */
  public function createDummy(): LanguageWireClient {
    $settings = $this->clientSettingsFactory->createDummy();
    return new LanguageWireClient($settings->get(), $this->projectSpecificationFactory);
  }

}
