<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Api limits.
 */
class ApiLimits {
  /**
   * Limits.
   *
   * @var array
   *   Limits.
   */
  private array $limits;

  /**
   * Constructs a new api limits object.
   *
   * @param array $limits
   *   Api limits.
   */
  public function __construct(array $limits) {
    $this->limits = $limits;
  }

  /**
   * Convert from platform limits.
   */
  public static function fromPlatformLimits(array $limits): self {
    return new self($limits);
  }

  /**
   * Get max documents per project.
   */
  public function getMaxDocumentsPerProject() {
    return $this->limits['maxDocumentsPerProject'];
  }

}
