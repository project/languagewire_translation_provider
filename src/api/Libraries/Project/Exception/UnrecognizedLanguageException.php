<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Project\Exception;

/**
 * Unrecognized Language exception.
 *
 * Unrecognized Language exception is thrown when
 * trying to create a project with not recognized
 * language code.
* */
final class UnrecognizedLanguageException extends \InvalidArgumentException {

  /**
   * Creates new instance of itself.
   *
   * Creates new instance of itself with
   * an error message about unrecognized
   * language code.
   *
   * @param string $languageCode
   *   Language code that could not be recognized.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromUnrecognizedLanguage(string $languageCode): self {
    return new UnrecognizedLanguageException(sprintf('Language code %s could not be recognized', $languageCode));
  }

  /**
   * Creates new instance of itself.
   *
   * Creates new instance of itself with
   * an error message about multiple
   * unrecognized language codes.
   *
   * @param string[] $languageCodes
   *   Language codes that could
   *   not be recognized.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromUnrecognizedLanguages(array $languageCodes): UnrecognizedLanguageException {
    return (count($languageCodes) == 1) ?
            self::fromUnrecognizedLanguage($languageCodes[0]) :
            new UnrecognizedLanguageException(sprintf('Language codes [%s] could not be recognized', implode(",", $languageCodes)));
  }

}
