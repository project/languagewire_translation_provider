<?php

namespace Drupal\languagewire_translation_provider\Adapter\Content;

use Drupal\languagewire_translation_provider\Content\VariablePlaceholderPrefixingConventionBase;
use Drupal\languagewire_translation_provider\Content\VariablePlaceholderPrefixingConventionInterface;

/**
 * Variable Placeholder Prefixing Convention for Drupal.
 *
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Render%21FormattableMarkup.php/function/FormattableMarkup%3A%3AplaceholderFormat/8.2.x
 */
final class VariablePlaceholderPrefixingConvention extends VariablePlaceholderPrefixingConventionBase {

  /**
   * {@inheritDoc}
   */
  protected function mapToPrefix(string $type): ?string {
    return match ($type) {
      VariablePlaceholderPrefixingConventionInterface::TYPE_SECURE => '@',
      VariablePlaceholderPrefixingConventionInterface::TYPE_SECURE_WITH_PLACEHOLDER => '%',
      VariablePlaceholderPrefixingConventionInterface::TYPE_INSECURE => ':',
      default => NULL,
    };
  }

}
