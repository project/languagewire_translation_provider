<?php

namespace Drupal\languagewire_translation_provider\api\Libraries\Project;

use Drupal\languagewire_translation_provider\api\Libraries\Content\DehydrationInterface;

/**
 * Project Specification Interface.
 *
 * Project Specification Interface for creating
 * specifications by which projects are created.
 */
interface ProjectSpecificationBaseInterface extends DehydrationInterface {
  public const DEADLINE_FORMAT = 'Y-m-d\TH:i:s\Z';

  /**
   * Get endpoint used to create the project.
   *
   * @return string
   *   The endpoint.
   */
  public function getCreateEndpoint(): string;

  /**
   * Returns project correlation ID.
   *
   * @return string
   *   The correlation ID.
   */
  public function getCorrelationId(): string;

  /**
   * Returns project title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string;

  /**
   * Returns source documents.
   *
   * @return int[]
   *   The source documents.
   */
  public function getSourceDocuments(): array;

  /**
   * Returns project briefing.
   *
   * @return string
   *   The briefing.
   */
  public function getBriefing(): string;

  /**
   * Returns project deadline.
   *
   * @return string
   *   The deadline.
   */
  public function getDeadline(): string;

}
