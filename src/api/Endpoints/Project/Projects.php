<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Project;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Projects endpoint api.
 */
class Projects extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects';
  protected const METHOD = Method::GET;
  protected const REQUEST_STRUCTURE = [
    'status',
    'limit',
  ];
  protected const RESPONSE_STRUCTURE = [
    'projects' => [
            [
              'id',
              'externalId',
              'title',
              'userId',
              'companyId',
              'status',
              'creationDate',
              'deadline',
              'finishedAt',
            ],
    ],
  ];

  /**
   * Set project status.
   */
  public function setProjectStatus(string $status, int $limit = 100): void {
    $this->setParams(
          [
            'status' => $status,
            'limit' => $limit,
          ]
      );
  }

}
