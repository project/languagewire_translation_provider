<?php

namespace Drupal\languagewire_translation_provider\Adapter\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface as DrupalEntityInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;

/**
 * Drupal Entity adapter.
* */
final class Entity implements EntityInterface {

  /**
   * Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private DrupalEntityInterface $entity;

  /**
   * Constructs a new Entity object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   */
  public function __construct(DrupalEntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get data.
   */
  public function getData(): array {
    return $this->entity->toArray();
  }

  /**
   * Get entity.
   */
  public function getEntity(): DrupalEntityInterface {
    return $this->entity;
  }

  /**
   * Get entity ID.
   */
  public function getEntityId(): string {
    return (string) $this->entity->id();
  }

  /**
   * Get entity type.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getEntityUrl(): string {
    return $this->entity->toUrl()->toString();
  }

  /**
   * Get entity type.
   */
  public function getTranslation(LanguageMapping $language): ?EntityInterface {
    return ($this->entity instanceof ContentEntityBase && $this->entity->hasTranslation($language->localLanguage())) ? new self($this->entity->getTranslation($language->localLanguage())) : NULL;
  }

}
