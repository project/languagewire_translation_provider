<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Files;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Download reference file endpoint.
 */
class DownloadReferenceFile extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/reference-files/:fileID';
  protected const METHOD = Method::GET;
  protected const REQUEST_STRUCTURE = [
    'id',
    'fileID',
  ];

  /**
   * Set project ID and file ID.
   */
  public function setFileData(int $projectID, string $fileID): void {
    $this->setParams(
          [
            'id' => $projectID,
            'fileID' => $fileID,
          ]
      );
  }

  /**
   * Executes the request.
   */
  public function execute(): ?array {
    $referenceContent = parent::execute();

    if (!$referenceContent) {
      return NULL;
    }

    return [$referenceContent];
  }

}
