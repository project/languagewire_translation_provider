<?php

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Client\ClientInterface;

/**
 * Available Work Areas.
 */
final class AvailableWorkAreas {
  /**
   * Client.
   *
   * @var \Drupal\languagewire_translation_provider\Client\ClientInterface
   */
  private ClientInterface $client;

  /**
   * Constructs a new AvailableWorkAreas object.
   *
   * @param \Drupal\languagewire_translation_provider\Client\ClientInterface $client
   *   Client.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Get.
   */
  public function get(): \Generator {
    foreach ($this->client->getWorkAreas() as $workArea) {
      yield $workArea;
    }
  }

}
