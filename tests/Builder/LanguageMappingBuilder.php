<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;

/**
 * Language Mapping Builder .
 */
class LanguageMappingBuilder {

  /**
   * Local Language .
   *
   * @var string
   */
  private $localLanguage = 'en';

  /**
   * Remote Language .
   *
   * @var string
   */
  private $remoteLanguage = 'en-US';

  /**
   * With Local Language .
   */
  public function withLocalLanguage(string $localLanguage): self {
    $this->localLanguage = $localLanguage;
    return $this;
  }

  /**
   * With Remote Language .
   */
  public function withRemoteLanguage(string $remoteLanguage): self {
    $this->remoteLanguage = $remoteLanguage;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): LanguageMapping {
    return new LanguageMapping($this->localLanguage, $this->remoteLanguage);
  }

}
