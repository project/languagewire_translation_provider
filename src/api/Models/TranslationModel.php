<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models;

/**
 * Translation model.
 */
class TranslationModel extends AbstractModel {

  /**
   * ID.
   *
   * @var int
   */
  protected int $id;

  /**
   * File ID.
   *
   * @var string
   */
  protected string $fileID;

  /**
   * Target language.
   *
   * @var string
   */
  protected string $targetLanguage;

  /**
   * Status.
   *
   * @var string
   */
  protected string $status;

  /**
   * Translation file name.
   *
   * @var string
   */
  protected string $translationFileName;

  /**
   * Translation file link.
   *
   * @var string
   */
  protected string $translationFileLink;

  /**
   * Get ID.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Set ID.
   */
  public function setId(int $id): void {
    $this->id = $id;
  }

  /**
   * Get the file ID.
   */
  public function getFileId(): string {
    return $this->fileID;
  }

  /**
   * Set the file ID.
   */
  public function setFileId(string $fileID): void {
    $this->fileID = $fileID;
  }

  /**
   * Get the target language.
   */
  public function getTargetLanguage(): string {
    return $this->targetLanguage;
  }

  /**
   * Set the target language.
   */
  public function setTargetLanguage(string $targetLanguage): void {
    $this->targetLanguage = $targetLanguage;
  }

  /**
   * Get the status.
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * Set the status.
   */
  public function setStatus(string $status): void {
    $this->status = $status;
  }

  /**
   * Get the translation file name.
   */
  public function getTranslationFileName(): string {
    return $this->translationFileName;
  }

  /**
   * Set the translation file name.
   */
  public function setTranslationFileName(string $translationFileName): void {
    $this->translationFileName = $translationFileName;
  }

  /**
   * Get the translation file link.
   */
  public function getTranslationFileLink(): string {
    return $this->translationFileLink;
  }

  /**
   * Set the translation file link.
   */
  public function setTranslationFileLink(string $translationFileLink): void {
    $this->translationFileLink = $translationFileLink;
  }

}
