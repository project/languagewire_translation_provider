<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobInterface;
use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use PHPUnit\Framework\TestCase;

/**
 * TMGMT Job Builder.
 */
final class TmgmtJobBuilder extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Job Id .
   *
   * @var int
   */
  private $jobId = 0;

  /**
   * Job Items .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobItemInterface[]
   */
  private $jobItems = [];

  /**
   * Translator .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface
   */
  private $translator;

  /**
   * Service Id .
   *
   * @var int
   */
  private $serviceId = 1;

  /**
   * Project Template Id .
   *
   * @var int
   */
  private $projectTemplateId = NULL;

  /**
   * Source Language .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping
   */
  private $sourceLanguage;

  /**
   * Target Language .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping
   */
  private $targetLanguage;

  /**
   * Title .
   *
   * @var string
   */
  private $title = 'Foo project';

  /**
   * Briefing .
   *
   * @var string
   */
  private $briefing = 'Briefing';

  /**
   * Settings .
   *
   * @var \Drupal\languagewire_translation_provider\Settings\JobSettings
   */
  private $settings = NULL;

  /**
   * Deadline .
   *
   * @var \DateTimeImmutable
   */
  private $deadline = NULL;

  /**
   * Constructs a new TmgmtJobBuilder object.
   */
  public function __construct() {
    $this->translator = $this->prophesize(TmgmtTranslatorInterface::class)->reveal();
    $this->sourceLanguage = (new LanguageMappingBuilder())->build();
    $this->targetLanguage = (new LanguageMappingBuilder())->build();
    $this->settings = new JobSettings([]);
    $this->deadline = new \DateTimeImmutable("now");
  }

  /**
   * With Job Id .
   */
  public function withJobId(int $jobId): self {
    $this->jobId = $jobId;
    return $this;
  }

  /**
   * With Job Items .
   */
  public function withJobItems(array $jobItems): self {
    $this->jobItems = $jobItems;
    return $this;
  }

  /**
   * With Source Language .
   */
  public function withSourceLanguage(LanguageMapping $sourceLanguage): self {
    $this->sourceLanguage = $sourceLanguage;
    return $this;
  }

  /**
   * With Target Language .
   */
  public function withTargetLanguage(LanguageMapping $targetLanguage): self {
    $this->targetLanguage = $targetLanguage;
    return $this;
  }

  /**
   * With Project Template Id .
   */
  public function withProjectTemplateId(int $projectTemplateId): self {
    $this->projectTemplateId = $projectTemplateId;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): TmgmtJobInterface {
    $job = $this->prophesize(TmgmtJobInterface::class);
    $job->jobId()->willReturn($this->jobId);
    $job->settings()->willReturn($this->settings);
    $job->items()->willReturn($this->jobItems);
    $job->translator()->willReturn($this->translator);
    $job->getServiceId()->willReturn($this->serviceId);
    $job->getProjectTemplatePlatformId()->willReturn($this->projectTemplateId);
    $job->sourceLanguage()->willReturn($this->sourceLanguage);
    $job->targetLanguage()->willReturn($this->targetLanguage);
    $job->title()->willReturn($this->title);
    $job->getOrderType()->willReturn('order');
    $job->getBriefing()->willReturn($this->briefing);
    $job->getDeadline()->willReturn($this->deadline);
    return $job->reveal();
  }

}
