<?php

namespace Drupal\languagewire_translation_provider\Service\ProjectContentUploader;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * Document selector factory.
 */
final class DocumentSelectorFactory {

  /**
   * Create from translator.
   */
  public function createFromTranslator(TmgmtTranslatorInterface $translator): DocumentSelectorInterface {
    return new FixedLimitDocumentSelector($translator);
  }

}
