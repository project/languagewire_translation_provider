<?php

namespace Drupal\languagewire_translation_provider\Settings;

/**
 * Drupal Settings.
 * */
interface SettingsInterface {

  /**
   * Get Drupal setting.
   */
  public function getConfig(string $key);

}
