<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Drupal\languagewire_translation_provider\api\Core\Settings;
use Drupal\languagewire_translation_provider\Client\ClientSettingsInterface;
use Drupal\languagewire_translation_provider\Client\LanguageWireClientSettings;
use Drupal\languagewire_translation_provider\LanguageWireConnector;
use Drupal\languagewire_translation_provider\Test\Builder\SettingsBuilder;
use PHPUnit\Framework\TestCase;

/**
 * LanguageWire Client Settings Test .
 */
class LanguageWireClientSettingsTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * System .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  private $system;

  /**
   * API Token .
   *
   * @var string
   */
  private $apiToken = 'API TOKEN';

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $drupalSettings = (new SettingsBuilder())->withEndpoint('https://example.com/')->build();
    $this->system = $this->prophesize(SystemInterface::class);
    $this->system->version()->willReturn('7.x');
    $this->system->moduleVersion(LanguageWireConnector::MODULE_NAME)->willReturn('7.x-1.0');
    $this->system->settings()->willReturn($drupalSettings);
  }

  /**
   * Provides Client Settings .
   *
   * @test
   */
  public function providesClientSettings(): void {
    $platformSettings = $this->settings()->get();

    $this->assertInstanceOf(Settings::class, $platformSettings);
    $this->assertSame('https://example.com/', $platformSettings->getEndpointUri());
    $this->assertSame($this->apiToken, $platformSettings->getAuthorizationToken());
    $this->assertSame('drupal-connector', $platformSettings->getIdentifierName());
    $this->assertSame('undefined', $platformSettings->getIdentifierVersion());

  }

  /**
   * Settings .
   */
  private function settings(): ClientSettingsInterface {
    return new LanguageWireClientSettings($this->apiToken, $this->system->reveal());
  }

}
