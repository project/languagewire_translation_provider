<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Domain;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtJobInterface;
use Drupal\languagewire_translation_provider\api\Helpers\Strings;

/**
 * Project Factory.
 *
 * @todo This is _not_ a domain object;
 * it belongs to the application (service) layer.
 */
final class ProjectFactory {

  /**
   * Create project from job.
   *
   * @throws \Random\RandomException
   */
  public static function createProjectFromJob(TmgmtJobInterface $job): Project {
    return new Project($job, Strings::uuid4(), ProjectStatus::underCreation());
  }

}
