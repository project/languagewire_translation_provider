<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Variable Placeholder Prefixing Convention Base.
 * */
abstract class VariablePlaceholderPrefixingConventionBase implements VariablePlaceholderPrefixingConventionInterface {

  /**
   * Prefix.
   */
  public function prefix(string $variableName, string $type): string {
    $allowedTypes = $this->getTypes();

    if (!in_array($type, $allowedTypes)) {
      $supportedTypesList = implode(', ', $this->getTypes());
      throw new UnknownVariablePlaceholderPrefixingType("Unknown prefixing type $type. Supported types are $supportedTypesList");
    }

    $prefix = $this->mapToPrefix($type);

    return "$prefix$variableName";
  }

  /**
   * Map to prefix.
   */
  abstract protected function mapToPrefix(string $type): ?string;

  /**
   * Get types.
   */
  private function getTypes(): array {
    $reflectedClass = new \ReflectionClass(static::class);
    return $reflectedClass->getConstants();
  }

}
