<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Managers\Exception;

/**
 * Empty Parameter exception.
 */
final class EmptyParameterException extends \InvalidArgumentException {

  /**
   * Create an exception for an empty parameter.
   */
  public static function fromEmptyParameter(string $parameterName): self {
    return new EmptyParameterException(sprintf('%s cannot be empty', $parameterName));
  }

}
