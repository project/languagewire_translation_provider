<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Sanitizer interface.
 */
interface SanitizerInterface {

  /**
   * Sanitize.
   */
  public function sanitize(string $content): string;

  /**
   * Desanitize.
   */
  public function desanitize(string $content): string;

}
