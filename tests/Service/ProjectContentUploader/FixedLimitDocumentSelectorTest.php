<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Service\ProjectContentUploader;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Service\ProjectContentUploader\FixedLimitDocumentSelector;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Class FixedLimitDocumentSelectorTest .
 */
class FixedLimitDocumentSelectorTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Fixed Limit Document Selector .
   *
   * @var \Drupal\languagewire_translation_provider\Service\ProjectContentUploader\FixedLimitDocumentSelector
   */
  private $fixedLimitDocumentSelector;

  /**
   * Translator .
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface
   */
  private $translator;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->translator = $this->prophesize(TmgmtTranslatorInterface::class);
  }

  /**
   * When Limit Is Positive Number Then Resulting Array Is Sliced .
   *
   * @test
   */
  public function whenLimitIsPositiveNumberThenResultingArrayIsSliced(): void {
    $this->translatorHasItemUploadBatchSize(5);

    $input = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    $expectedOutput = [1, 2, 3, 4, 5];
    $result = $this->fixedLimitDocumentSelector->selectDocuments($input);

    $this->assertEquals($expectedOutput, $result);
  }

  /**
   * When Limit Is Zero Then Exception Is Thrown .
   *
   * @test
   */
  public function whenLimitIsZeroThenExceptionIsThrown(): void {
    $this->expectException(InvalidArgumentException::class);
    $this->translatorHasItemUploadBatchSize(0);
  }

  /**
   * When Limit Is Negative Then Exception Is Thrown .
   *
   * @test
   */
  public function whenLimitIsNegativeThenExceptionIsThrown(): void {
    $this->expectException(InvalidArgumentException::class);
    $this->translatorHasItemUploadBatchSize(-5);
  }

  /**
   * Translator Has Item Upload Batch Size .
   */
  private function translatorHasItemUploadBatchSize(int $limit): void {
    $this->translator->getItemUploadBatchSize()->willReturn($limit);
    $this->fixedLimitDocumentSelector = new FixedLimitDocumentSelector($this->translator->reveal());
  }

}
