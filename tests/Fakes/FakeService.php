<?php

namespace Drupal\languagewire_translation_provider\Test\Fakes;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\Date;
use Drupal\languagewire_translation_provider\Platform\Services\ServiceInterface;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use Drupal\languagewire_translation_provider\Settings\OrderType;

/**
 * Fake Service.
 */
class FakeService implements ServiceInterface {

  /**
   * ID .
   *
   * @var int
   */
  private $id;

  /**
   * Name .
   *
   * @var string
   */
  private $name;

  /**
   * Template .
   *
   * @var int
   */
  private $template;

  /**
   * Translation Memory .
   *
   * @var int
   */
  private $translationMemory;

  /**
   * Term Base .
   *
   * @var int
   */
  private $termBase;

  /**
   * Invoicing Account .
   *
   * @var int
   */
  private $invoicingAccount;

  /**
   * Work Area .
   *
   * @var int
   */
  private $workArea;

  /**
   * User .
   *
   * @var int
   */
  private $user;

  /**
   * Deadline .
   *
   * @var \Drupal\languagewire_translation_provider\Date
   */
  private $deadline;

  /**
   * Constructs a new FakeService object.
   */
  public function __construct(
    int $id,
    string $name,
    int $template,
    int $translationMemory,
    int $termBase,
    int $invoicingAccount,
    int $workArea,
    int $user,
    Date $deadline
  ) {

    $this->id = $id;
    $this->name = $name;
    $this->template = $template;
    $this->translationMemory = $translationMemory;
    $this->termBase = $termBase;
    $this->invoicingAccount = $invoicingAccount;
    $this->workArea = $workArea;
    $this->user = $user;
    $this->deadline = $deadline;
  }

  /**
   * Id .
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Name .
   */
  public function name(): string {
    return $this->name;
  }
  public function templateId(): ?int
  {
    return $this->template;
  }

  /**
   * Translation Memory Id.
   */
  public function translationMemoryId(): int {
    return $this->translationMemory;
  }
  public function termBaseId(): int
  {
    return $this->termBase;
  }

  /**
   * Invoicing Account Id .
   */
  public function invoicingAccountId(): int {
    return $this->invoicingAccount;
  }

  /**
   * Work Area Id .
   */
  public function workAreaId(): int {
    return $this->workArea;
  }
  public function userId(): int
  {
    return $this->user;
  }

  /**
   * Deadline .
   */
  public function deadline(): Date {
    return $this->deadline;
  }

  /**
   * Configure .
   */
  public function configure(JobSettings $jobSettings): void {
    $dateNow = (new \DateTimeImmutable())
      ->setTimezone(new \DateTimeZone('UTC'));

    $this->user = $jobSettings->getUser() ?? 0;
    $this->template = $jobSettings->getProjectTemplate();
    $this->translationMemory = $jobSettings->getTranslationMemory();
    $this->termBase = $jobSettings->getTermBase();
    $this->invoicingAccount = $jobSettings->getInvoicingAccount() ?? 0;
    $this->workArea = $jobSettings->getWorkArea() ?? 0;
    $this->deadline = $jobSettings->getDeadline() ? $jobSettings->getDeadlineAsDate() : new Date($dateNow);
  }

  /**
   * Is Language Supported .
   */
  public function isLanguageSupported(LanguageMapping $languageMapping): bool {
    // Trust system pre-definition.
    return TRUE;
  }
}
