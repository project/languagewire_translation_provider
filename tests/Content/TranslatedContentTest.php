<?php

namespace Drupal\languagewire_translation_provider\Test\Content;

use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\Frame;
use Drupal\languagewire_translation_provider\Content\SanitizerInterface;
use Drupal\languagewire_translation_provider\Content\TranslatedContent;
use Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary\FrameBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Translated Content Test .
 */
class TranslatedContentTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * It Creates New Instance From Platform Frame Object .
   *
   * @test
   */
  public function itCreatesNewInstanceFromPlatformFrameObject(): void {
    $text = 'translated content';
    $contentId = 'content ID';
    $platformFrame = $this->platformFrame($text, $contentId);
    $content = TranslatedContent::fromPlatformFrame($platformFrame);

    $this->assertSame($text, $content->translatedContent());
    $this->assertSame($contentId, $content->contentId());
  }

  /**
   * It Desanitizes Translated Text .
   *
   * @test
   */
  public function itDesanitizesTranslatedText(): void {
    $content = new TranslatedContent('sanitized', 'contentID');
    $sanitizer = $this->prophesize(SanitizerInterface::class);
    $sanitizer->desanitize('sanitized')->willReturn('desanitized');

    $desanitizedContent = $content->desanitize($sanitizer->reveal());

    $this->assertNotSame($desanitizedContent, $content);
    $this->assertNotSame($content->translatedContent(), $desanitizedContent->translatedContent());
    $this->assertSame('desanitized', $desanitizedContent->translatedContent());
  }

  /**
   * Platform Frame .
   */
  private function platformFrame(string $text, string $contentId): Frame {
    return (new FrameBuilder())->withText($text)->withMetadata(['contentId' => $contentId])->build();
  }

}
