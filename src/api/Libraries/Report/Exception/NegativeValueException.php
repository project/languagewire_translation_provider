<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Report\Exception;

/**
 * Negative Value exception.
 *
 * Negative Value exception is thrown
 * when trying to pass negative value
 * argument to a method.
* */
final class NegativeValueException extends \InvalidArgumentException {
}
