<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Managers;

use Drupal\languagewire_translation_provider\api\Endpoints\General\Languages;

/**
 * Language manager.
 */
class LanguageManager extends AbstractManager {

  /**
   * Get available languages.
   */
  public function getAvailableLanguages(): array {
    $languagesEndpoint = new Languages($this->settings);
    $languages = $languagesEndpoint->execute();

    if (!$languages || count($languages) === 0) {
      return [];
    }

    $orderedLanguages = [];
    foreach ($languages as $language) {
      $orderedLanguages[$language['languageCode']] = $language['languageName'];
    }

    asort($orderedLanguages);

    return $orderedLanguages;
  }

}
