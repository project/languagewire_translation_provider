<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core;

/**
 * Connection settings.
 */
final class Settings {

  /**
   * Endpoint URI.
   *
   * @var string
   */
  private string $endpointUri;

  /**
   * Authorization token.
   *
   * @var string
   */
  private string $authorizationToken;

  /**
   * Identifier name.
   *
   * @var string
   */
  private string $identifierName;

  /**
   * Identifier version.
   *
   * @var string
   */
  private string $identifierVersion;

  public function __construct(
    string $endpointUri,
    string $authorizationToken,
    string $identifierName = 'unknown',
    string $identifierVersion = 'unknown',
  ) {
    $this->endpointUri = $endpointUri;
    $this->authorizationToken = $authorizationToken;
    $this->identifierName = $identifierName;
    $this->identifierVersion = $identifierVersion;
  }

  /**
   * Get endpoint uri.
   */
  public function getEndpointUri(): string {
    return $this->endpointUri;
  }

  /**
   * Get authorization token.
   */
  public function getAuthorizationToken(): string {
    return $this->authorizationToken;
  }

  /**
   * Get identifier name.
   */
  public function getIdentifierName(): string {
    return $this->identifierName;
  }

  /**
   * Get identifier version.
   */
  public function getIdentifierVersion(): string {
    return $this->identifierVersion;
  }

}
