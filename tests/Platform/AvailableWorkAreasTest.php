<?php

namespace Drupal\languagewire_translation_provider\Test\Platform;

use Drupal\languagewire_translation_provider\Client\ClientInterface;
use Drupal\languagewire_translation_provider\Client\WorkArea;
use Drupal\languagewire_translation_provider\Platform\AvailableWorkAreas;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * Available Work Areas Test .
 */
class AvailableWorkAreasTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Client .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $client;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->client = $this->prophesize(ClientInterface::class);
  }

  /**
   * Returns Generator Yielding Work Areas .
   *
   * @test
   */
  public function returnsGeneratorYieldingWorkAreas(): void {
    $workArea = new WorkArea(1, 'WA');
    $this->client->getWorkAreas()->willReturn([$workArea]);
    $service = new AvailableWorkAreas($this->client->reveal());
    $workAreas = $service->get();

    $this->assertInstanceOf(Generator::class, $workAreas);

    $workAreas->rewind();
    $retrievedWorkArea = $workAreas->current();
    $this->assertSame($workArea->id(), $retrievedWorkArea->id());
    $this->assertSame($workArea->name(), $retrievedWorkArea->name());
  }

}
