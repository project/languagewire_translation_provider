<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content\Udf;

use Drupal\languagewire_translation_provider\api\Libraries\Content\DehydrationInterface;

/**
 * Frame object interface.
 *
 * Frames are objects that represent a translation unit,
 * so in this context they are an atomic representation of
 * piece of text that needs to be translated. Frame object
 * must be immutable, meaning that data once set cannot be
 * changed. Any changes should result in creation of a new Frame object.
 * Frame must contain text. It may contain a comment to the text as
 * well as metadata. Metadata is a set of arbitrary key-value pairs.
 */
interface FrameInterface extends DehydrationInterface {

  /**
   * Returns Frame text.
   *
   * @return string
   *   Frame text.
   */
  public function getText(): string;

  /**
   * Returns Frame comment.
   *
   * @return string
   *   Frame comment.
   */
  public function getComment(): string;

  /**
   * Returns Frame Metadata object.
   *
   * @return MetadataInterface
   *   Frame Metadata object.
   */
  public function getMetadata(): MetadataInterface;

}
