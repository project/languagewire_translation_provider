<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

/**
 * Invalid Language Exception.
 *
 * Invalid Language Exception is thrown when
 * trying to instantiate Document object with
 * invalid (not supported) language code.
 */
final class InvalidLanguageException extends \InvalidArgumentException {
}
