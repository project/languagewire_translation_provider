<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Managers\Exception;

/**
 * Invalid Deadline exception.
 */
final class InvalidDeadlineException extends \InvalidArgumentException {

  /**
   * Creates new instance with deadline not being in the future.
   *
   * @param \DateTimeInterface $dateTime
   *   Deadline date.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromNonFutureDate(\DateTimeInterface $dateTime): self {
    return new InvalidDeadlineException(
          sprintf(
              'Deadline provided is not pointing to a future date. Given date was %s',
              $dateTime->format('Y-m-d H:i:s')
          )
      );
  }

  /**
   * Creates new instance with deadline not being in UTC timezone.
   *
   * @param \DateTimeInterface $dateTime
   *   Deadline date.
   *
   * @return static
   *   New instance of itself.
   */
  public static function fromNonUtcDate(\DateTimeInterface $dateTime): self {
    return new InvalidDeadlineException(
          sprintf('Deadline date must be in UTC time zone. Provided was %s', $dateTime->getTimezone()->getName())
      );
  }

}
