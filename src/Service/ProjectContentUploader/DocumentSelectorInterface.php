<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Service\ProjectContentUploader;

/**
 * Document selector.
 */
interface DocumentSelectorInterface {

  /**
   * Select documents.
   *
   * @param \Drupal\languagewire_translation_provider\Platform\Document\UploadableDocument[] $documents
   *   Uploadable documents.
   *
   * @return \Drupal\languagewire_translation_provider\Platform\Document\UploadableDocument[]
   *   Uploadable documents.
   */
  public function selectDocuments(array $documents): array;

}
