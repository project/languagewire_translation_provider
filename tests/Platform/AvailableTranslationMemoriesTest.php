<?php

namespace Drupal\languagewire_translation_provider\Test\Platform;

use Drupal\languagewire_translation_provider\Client\ClientInterface;
use Drupal\languagewire_translation_provider\Client\TranslationMemory;
use Drupal\languagewire_translation_provider\Platform\AvailableTranslationMemories;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * Tests AvailableTranslationMemories.
 */
class AvailableTranslationMemoriesTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Client .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $client;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->client = $this->prophesize(ClientInterface::class);
  }

  /**
   * Returns Generator Yielding Translation Memories.
   *
   * @test
   */
  public function returnsGeneratorYieldingTranslationMemories(): void {
    $translationMemory = new TranslationMemory(1, 'Translation Memory');
    $this->client->getTranslationMemories()->willReturn([$translationMemory]);
    $service = new AvailableTranslationMemories($this->client->reveal());
    $translationMemories = $service->get();

    $this->assertInstanceOf(Generator::class, $translationMemories);

    $translationMemories->rewind();
    $retrievedTranslationMemory = $translationMemories->current();
    $this->assertSame($translationMemory->id(), $retrievedTranslationMemory->id());
    $this->assertSame($translationMemory->name(), $retrievedTranslationMemory->name());
  }

}
