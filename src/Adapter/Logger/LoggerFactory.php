<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\Logger;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Psr\Log\LoggerInterface;
use function version_compare;

/**
 * Logger factory.
 */
final class LoggerFactory {

  /**
   * System.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\SystemInterface
   */
  private SystemInterface $system;

  /**
   * Constructs a new LoggerFactory object.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\SystemInterface $system
   *   System.
   */
  public function __construct(SystemInterface $system) {
    $this->system = $system;
  }

  /**
   * Create logger.
   */
  public function createLogger(): LoggerInterface {
    return version_compare($this->system->version(), '9.99.99', '>') ? new TenLogger() : new NineLogger();
  }

}
