<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

use Psr\Http\Message\ResponseInterface;

/**
 * Content Factory.
 *
 * A helper class that can be used to create a Content object.
 */
final class ContentFactory {

  /**
   * From Response .
   */
  public static function fromResponse(ResponseInterface $response, ?SerializerInterface $serializer = NULL): ContentInterface {
    $contentType = $response->getHeader('Content-Type')[0];

    if ($serializer != NULL && $contentType === MediaType::UDF) {
      $udf = $serializer->deserialize((string) $response->getBody());
      return new UdfContent($udf);
    }

    return new StreamContent($response->getBody(), $contentType);
  }

}
