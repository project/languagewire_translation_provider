<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Content\HtmlPreview\Result;

use Drupal\languagewire_translation_provider\Domain\Document;

/**
 * Html preview document result.
 */
final class HtmlPreviewDocumentResult {

  /**
   * Package.
   *
   * @var HtmlPreviewZipPackage
   */
  private HtmlPreviewZipPackage $package;

  /**
   * Document.
   *
   * @var \Drupal\languagewire_translation_provider\Domain\Document
   */
  private Document $document;

  /**
   * Constructs a new HtmlPreviewDocumentResult object.
   *
   * @param \Drupal\languagewire_translation_provider\Content\HtmlPreview\Result\HtmlPreviewZipPackage $package
   *   Package.
   * @param \Drupal\languagewire_translation_provider\Domain\Document $document
   *   Document.
   */
  public function __construct(HtmlPreviewZipPackage $package, Document $document) {
    $this->package = $package;
    $this->document = $document;
  }

  /**
   * Get package path.
   */
  public function getPackagePath(): string {
    return $this->package->getPath();
  }

  /**
   * Get document platform ID.
   */
  public function getDocumentPlatformId(): ?string {
    return $this->document->getPlatformId();
  }

}
