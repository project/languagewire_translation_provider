<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

/**
 * Holds information about document validation status.
 */
final class ValidationStatus {
  /**
   * Validation status: Not Requested.
   */
  public const NOT_REQUESTED = 0;
  /**
   * Validation status: Pending.
   */
  public const PENDING = 1;
  /**
   * Validation status: Succeeded.
   */
  public const SUCCEEDED = 2;
  /**
   * Validation status: Failed.
   */
  public const FAILED = 3;
  /**
   * Validation status: Cancelled.
   */
  public const CANCELLED = 4;

  /**
   * Validation status code.
   *
   * @var int
   */
  private int $status;
  /**
   * Document errors.
   *
   * @var array
   */
  private array $errors;

  /**
   * Constructs a new ValidationStatus object.
   *
   * @param int $status
   *   Validation status code.
   * @param array $errors
   *   Document errors.
   */
  public function __construct(int $status, array $errors = []) {
    $this->status = $status;
    $this->errors = $errors;
  }

  /**
   * Returns validation status code.
   *
   * @return int
   *   Validation status code.
   */
  public function getStatus(): int {
    return $this->status;
  }

  /**
   * Returns all document errors.
   *
   * @return array
   *   Document errors.
   */
  public function getErrors(): array {
    return $this->errors;
  }

}
