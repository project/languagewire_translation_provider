<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Domain\Document;

/**
 * Document Repository interface.
 * */
interface DocumentRepositoryInterface {

  /**
   * Get by id.
   *
   * @param int $documentId
   *   Document id.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Document|null
   *   Document.
   */
  public function getById(int $documentId): ?Document;

  /**
   * Get all by project id.
   *
   * @param int $projectId
   *   Project id.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Document[]
   *   Documents.
   */
  public function getAllByProjectId(int $projectId): array;

  /**
   * Save.
   */
  public function save(Document $document): Document;

  /**
   * Save many.
   *
   * @param \Drupal\languagewire_translation_provider\Domain\Document[] $documents
   *   Documents.
   *
   * @return \Drupal\languagewire_translation_provider\Domain\Document[]
   *   Documents.
   */

  /**
   * Save many.
   */
  public function saveMany(array $documents): array;

}
