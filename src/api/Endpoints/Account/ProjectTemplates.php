<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Account;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Project templates endpoint.
 */
class ProjectTemplates extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'project-templates';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'id',
          'name',
        ],
  ];

}
