<?php

namespace Drupal\languagewire_translation_provider\Adapter\Logger;

use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\languagewire_translation_provider\LanguageWireConnector;
use Psr\Log\LoggerInterface;

/**
 * Drupal 10 Logger.
 */
final class TenLogger implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * {@inheritDoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    \Drupal::logger(LanguageWireConnector::MODULE_NAME)
      ->log($level, $message, $context);
  }

}
