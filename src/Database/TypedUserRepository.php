<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Client\User;

/**
 * User Repository.
 */
final class TypedUserRepository extends TypedConfigurationItemRepositoryBase implements UserRepositoryInterface {
  /**
   * The type of the configuration item.
   */
  private const TYPE = 'user';

  /**
   * {@inheritDoc}
   */
  protected function type(): string {
    return self::TYPE;
  }

  /**
   * {@inheritDoc}
   */
  protected function map(\stdClass $user): ConfigurationItemInterface {
    return new User($user->item_id, $user->name);
  }

}
