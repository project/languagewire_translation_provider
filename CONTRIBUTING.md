# Contributing to LanguageWire Translation Provider

We greatly appreciate your interest and willingness
 to contribute to this project.

This document serves as a guide for contributors, offering essential
information on how to report issues effectively,
as well as providing useful tips and guidelines
for those familiar with open source contributions.

## Reporting Issues

Your contributions can significantly impact the project,
especially when submitting detailed and well-documented
reports of any issues you encounter. We value comprehensive
and thoughtfully presented bug reports and express our gratitude
for such contributions.

Before reporting an issue, please ensure it has not already been identified by checking the [issue database](https://www.drupal.org/project/issues/languagewire_translation_provider). If you discover your issue has already been reported, you may follow the progress by using the "subscribe" button to receive updates.

To maintain clarity in our discussion threads, please refrain from posting
generic comments such as "+1" or "I have this too." Instead, we encourage you
 to contribute any additional details or steps to reproduce the issue,
 as this information can be crucial in resolving it.


## Contributing Code

Your pull requests, regardless of their size or nature, are always welcome. Whether it’s correcting a minor typo, identifying a bug, or knowing how to fix an issue, we encourage you to submit your contributions. For substantial improvements or changes, we recommend documenting them to [issue database](https://www.drupal.org/project/issues/languagewire_translation_provider) prior to commencing work.

We are committed to promptly reviewing and responding to pull requests,
offering feedback, and integrating your valuable contributions into the project.