<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\api\Models\Assignments\AssignmentDocumentModel;
use Drupal\languagewire_translation_provider\Client\Assignment;
use Drupal\languagewire_translation_provider\Client\AssignmentStatus;
use Drupal\languagewire_translation_provider\Test\Builder\ClientLibrary\AssignmentBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Assignment Test .
 */
class AssignmentTest extends TestCase {

  /**
   * It Wraps Client Platform Assignment .
   *
   * @test
   */
  public function itWrapsClientPlatformAssignment(): void {
    $platformAssignment = (new AssignmentBuilder())
      ->withAssignmentDocument(new AssignmentDocumentModel($sourceDocumentId = '10', $targetDocumentId = '20'))
      ->build();
    $assignment = new Assignment($platformAssignment);

    $this->assertSame(AssignmentStatus::PENDING, $assignment->status()->value());
    $this->assertEquals([$sourceDocumentId => $targetDocumentId], $assignment->getPlatformDocumentIdPairs());
  }

}
