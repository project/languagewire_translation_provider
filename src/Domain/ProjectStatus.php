<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Domain;

/**
 * Document status.
* */
final class ProjectStatus {
  public const ERROR = 'error';
  public const UNRECOVERABLE_ERROR = 'unrecoverable_error';
  public const CANCELLED = 'cancelled';
  public const UNDER_CREATION = 'under_creation';
  public const UPLOADING = 'uploading';
  public const IN_PROGRESS = 'in_progress';
  public const FINISHED = 'finished';
  public const IMPORTING = 'importing';
  public const CANCELLABLE_STATUSES = [
    self::UNDER_CREATION,
    self::UPLOADING,
  ];

  /**
   * Status.
   *
   * @var string
   */
  private string $status;

  /**
   * Constructs a new DocumentStatus object.
   *
   * @param string $status
   *   Status.
   */
  public function __construct(string $status) {
    $this->status = $status;
  }

  /**
   * Value.
   */
  public function value(): string {
    return (string) $this;
  }

  /**
   * To string.
   */
  public function __toString(): string {
    return $this->status;
  }

  /**
   * Under creation.
   */
  public static function underCreation(): self {
    return new self(self::UNDER_CREATION);
  }

  /**
   * Uploading.
   */
  public static function uploading(): self {
    return new self(self::UPLOADING);
  }

  /**
   * In progress.
   */
  public static function inProgress(): self {
    return new self(self::IN_PROGRESS);
  }

  /**
   * Error.
   */
  public static function error(): self {
    return new self(self::ERROR);
  }

  /**
   * Unrecoverable error.
   */
  public static function unrecoverableError(): self {
    return new self(self::UNRECOVERABLE_ERROR);
  }

  /**
   * Importing.
   */
  public static function importing(): self {
    return new self(self::IMPORTING);
  }

  /**
   * Finished.
   */
  public static function finished(): self {
    return new self(self::FINISHED);
  }

  /**
   * Cancelled.
   */
  public static function cancelled(): self {
    return new self(self::CANCELLED);
  }

  /**
   * Equals.
   */
  public function equals(self $other): bool {
    return $this->status == $other->status;
  }

  /**
   * Is error.
   */
  public function isError(): bool {
    return $this->status == self::ERROR ||
            $this->status == self::UNRECOVERABLE_ERROR;
  }

  /**
   * Is cancellable.
   */
  public function isCancellable(): bool {
    return in_array($this->status, self::CANCELLABLE_STATUSES);
  }

}
