<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Emoji Sanitizer.
 */
final class EmojiSanitizer implements SanitizerInterface {

  /**
   * Sanitize.
   */
  public function sanitize(string $content): string {
    return $this->removeEmoji($content);
  }

  /**
   * Desanitize.
   */
  public function desanitize(string $content): string {
    return $content;
  }

  /**
   * Remove emoji.
   */
  public function removeEmoji($string): string {
    $replacement = '';

    // Match Enclosed Alphanumeric Supplement.
    $regexAlphanumeric = '/[\x{1F100}-\x{1F1FF}]/u';
    $clearString = preg_replace($regexAlphanumeric, $replacement, $string);

    // Match Miscellaneous Symbols and Pictographs.
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clearString = preg_replace($regexSymbols, $replacement, $clearString);

    // Match Emoticons.
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clearString = preg_replace($regexEmoticons, $replacement, $clearString);

    // Match Transport And Map Symbols.
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clearString = preg_replace($regexTransport, $replacement, $clearString);

    // Match Supplemental Symbols and Pictographs.
    $regexSupplemental = '/[\x{1F900}-\x{1F9FF}]/u';
    $clearString = preg_replace($regexSupplemental, $replacement, $clearString);

    // Match Miscellaneous Symbols.
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clearString = preg_replace($regexMisc, $replacement, $clearString);

    // Match Dingbats.
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clearString = preg_replace($regexDingbats, $replacement, $clearString);

    // Remove zero-width joiners.
    $regexZeroWidth = '/[\x{200B}-\x{200D}]/u';
    $clearString = preg_replace($regexZeroWidth, $replacement, $clearString);

    // Remove variation selectors.
    $regexVariationSelectors = '/[\x{FE00}-\x{FE0F}]/u';
    return preg_replace($regexVariationSelectors, $replacement, $clearString);
  }

}
