<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Files;

/**
 * Invalid Filename Exception.
 *
 * Invalid Filename Exception is thrown when
 * trying to instantiate a ReferenceFile object with an invalid filename
 * language code.
 */
final class InvalidFilenameException extends \InvalidArgumentException {
}
