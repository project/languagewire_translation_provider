<?php

namespace Drupal\languagewire_translation_provider\Test\Content;

use Drupal\languagewire_translation_provider\Content\UnknownVariablePlaceholderPrefixingType;
use Drupal\languagewire_translation_provider\Content\VariablePlaceholderPrefixingConventionBase;
use Drupal\languagewire_translation_provider\Content\VariablePlaceholderPrefixingConventionInterface;
use PHPUnit\Framework\TestCase;

/**
 * Tests Variable Placeholder Prefixing Convention Base .
 */
class VariablePlaceholderPrefixingConventionBaseTest extends TestCase {

  /**
   * Prefixes Variable For String Format Function .
   *
   * @test
   */
  public function prefixesVariableForStringFormatFunction(): void {
    $prefixingConvention = $this->getPrefixingConvention();

    $this->assertSame('@varName', $prefixingConvention->prefix('varName', VariablePlaceholderPrefixingConventionInterface::TYPE_SECURE));
  }

  /**
   * Throws For Unknown Prefixing Type .
   *
   * @test
   */
  public function throwsForUnknownPrefixingType(): void {
    $unknownType = 'unknown';

    $prefixingConvention = $this->getPrefixingConvention();
    $this->expectException(UnknownVariablePlaceholderPrefixingType::class);
    $this->expectExceptionMessage("Unknown prefixing type $unknownType. Supported types are secure, secure_placeholder, insecure");

    $prefixingConvention->prefix('varName', $unknownType);
  }

  /**
   * Get Prefixing Convention .
   */
  private function getPrefixingConvention(): VariablePlaceholderPrefixingConventionBase {
    return new class extends VariablePlaceholderPrefixingConventionBase
        {

      /**
       * Map To Prefix .
       */
      protected function mapToPrefix(string $type): ?string {
        return $type === VariablePlaceholderPrefixingConventionInterface::TYPE_SECURE ? '@' : NULL;
      }

    };
  }

}
