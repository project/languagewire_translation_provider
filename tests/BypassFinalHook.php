<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test;

use DG\BypassFinals;
use PHPUnit\Runner\BeforeTestHook;

/**
 * Bypass final hook.
 */
class BypassFinalHook implements BeforeTestHook {

  /**
   * Execute Before Test .
   */
  public function executeBeforeTest(string $test): void {
    BypassFinals::enable();
  }

}
