<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Files;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * File metadata endpoint.
 */
class FileMetadata extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/files/:fileID/metadata';
  protected const METHOD = Method::GET;
  protected const REQUEST_STRUCTURE = [
    'id',
    'fileID',
  ];
  protected const RESPONSE_STRUCTURE = [
    'fileId',
    'fileName',
    'contentType',
    'fileType',
    'fileSize',
    'projectId',
    'uploadDate',
    'lastAccessDate',
  ];

  /**
   * Set project ID and file ID.
   */
  public function setFileData(int $projectID, string $fileID): void {
    $this->setParams(
          [
            'id' => $projectID,
            'fileID' => $fileID,
          ]
      );
  }

}
