<?php

namespace Drupal\languagewire_translation_provider\Content;

/**
 * Composite Sanitizer.
 * */
class CompositeSanitizer implements SanitizerInterface {

  /**
   * Sanitizers.
   *
   * @var SanitizerInterface[]
   */
  private array $sanitizers;

  /**
   * Constructs a new CompositeSanitizer object.
   *
   * @param SanitizerInterface[] $sanitizers
   *   Sanitizers.
   */
  public function __construct(...$sanitizers) {
    $this->sanitizers = $sanitizers;
  }

  /**
   * Sanitize.
   */
  public function sanitize(string $content): string {
    foreach ($this->sanitizers as $sanitizer) {
      $content = $sanitizer->sanitize($content);
    }

    return $content;
  }

  /**
   * Desanitize.
   */
  public function desanitize(string $content): string {
    foreach (array_reverse($this->sanitizers) as $sanitizer) {
      $content = $sanitizer->desanitize($content);
    }

    return $content;
  }

}
