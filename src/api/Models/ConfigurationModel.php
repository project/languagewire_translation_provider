<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models;

/**
 * Configuration model.
 */
class ConfigurationModel extends AbstractModel {

  /**
   * Work Area ID.
   *
   * @var int
   */
  protected int $workAreaID;

  /**
   * Product ID.
   *
   * @var int|null
   */
  protected ?int $productID;

  /**
   * Invoicing Account ID.
   *
   * @var int
   */
  protected int $invoicingAccountID;

  /**
   * Translation Memory ID.
   *
   * @var int|null
   */
  protected ?int $translationMemoryID;

  /**
   * Term Base ID.
   *
   * @var int|null
   */
  protected ?int $termBaseID;

  /**
   * User ID.
   *
   * @var int
   */
  protected int $userID;

  /**
   * Get Work Area ID.
   */
  public function getWorkAreaId(): ?int {
    return $this->workAreaID;
  }

  /**
   * Set Work Area ID.
   */
  public function setWorkAreaId(int $workAreaID): void {
    $this->workAreaID = $workAreaID;
  }

  /**
   * Get Product ID.
   */
  public function getProductId(): ?int {
    return $this->productID;
  }

  /**
   * Set Product ID.
   */
  public function setProductId(?int $productID): void {
    $this->productID = $productID;
  }

  /**
   * Get Invoicing Account ID.
   */
  public function getInvoicingAccountId(): int {
    return $this->invoicingAccountID;
  }

  /**
   * Set Invoicing Account ID.
   */
  public function setInvoicingAccountId(int $invoicingAccountID): void {
    $this->invoicingAccountID = $invoicingAccountID;
  }

  /**
   * Get Translation Memory ID.
   */
  public function getTranslationMemoryId(): ?int {
    return $this->translationMemoryID;
  }

  /**
   * Set Translation Memory ID.
   */
  public function setTranslationMemoryId(?int $translationMemoryID): void {
    $this->translationMemoryID = $translationMemoryID;
  }

  /**
   * Get Term Base ID.
   */
  public function getTermBaseId(): ?int {
    return $this->termBaseID;
  }

  /**
   * Set Term Base ID.
   */
  public function setTermBaseId(?int $termBaseID): void {
    $this->termBaseID = $termBaseID;
  }

  /**
   * Get User ID.
   */
  public function getUserId(): int {
    return $this->userID;
  }

  /**
   * Set User ID.
   */
  public function setUserId(int $userID): void {
    $this->userID = $userID;
  }

}
