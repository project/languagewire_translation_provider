<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\General;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Work areas endpoint.
 */
class WorkAreas extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'workareas';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'id',
          'name',
        ],
  ];

}
