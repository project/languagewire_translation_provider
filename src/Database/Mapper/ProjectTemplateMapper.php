<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Database\Mapper;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Domain\ProjectTemplate;
use Drupal\languagewire_translation_provider\LanguageWireConnector;
use function explode;
use function implode;
use function intval;

/**
 * Project Template Mapper.
 * */
class ProjectTemplateMapper {

  /**
   * To project template.
   */
  public function toProjectTemplate(array $projectTemplateDto): ?ProjectTemplate {
    try {
      $projectTemplateId = isset($projectTemplateDto['template_id']) ? intval($projectTemplateDto['template_id']) : NULL;
      $deadLine = isset($projectTemplateDto['desired_deadline'])
                ? \DateTimeImmutable::createFromFormat(
                LanguageWireConnector::DEADLINE_DATE_FORMAT,
                $projectTemplateDto['desired_deadline']
            ) : NULL;

      return new ProjectTemplate(
            $projectTemplateDto['name'],
            intval($projectTemplateDto['server_id']),
            explode(",", $projectTemplateDto['target_languages']),
            intval($projectTemplateDto['user_id']),
            intval($projectTemplateDto['work_area_id']),
            intval($projectTemplateDto['invoicing_account_id']),
            intval($projectTemplateDto['translation_memory_id']),
            intval($projectTemplateDto['term_base_id']),
            $projectTemplateDto['order_type'],
            $projectTemplateId,
            $projectTemplateDto['description'] ?? NULL,
            $projectTemplateDto['project_name'] ?? NULL,
            $projectTemplateDto['source_language'] ?? NULL,
            $deadLine,
            isset($projectTemplateDto['job_types'])
                ? explode(",", $projectTemplateDto['job_types'])
                : NULL,
            $projectTemplateDto['briefing_to_languagewire'] ?? NULL
        );
    }
    catch (\Throwable) {
      // @todo log exceptions
      return NULL;
    }
  }

  /**
   * To database entity.
   */
  public function toDatabaseEntity(ProjectTemplate $projectTemplate, TmgmtTranslatorInterface $translator): array {
    $desiredDeadline = $projectTemplate->getDesiredDeadline()?->format(LanguageWireConnector::DEADLINE_DATE_FORMAT);

    return [
      'name' => $projectTemplate->getName(),
      'server_id' => $projectTemplate->getPlatformId(),
      'target_languages' => implode(",", $projectTemplate->getTargetLanguages()),
      'user_id' => $projectTemplate->getUserId(),
      'work_area_id' => $projectTemplate->getWorkAreaId(),
      'invoicing_account_id' => $projectTemplate->getInvoicingAccountId(),
      'translation_memory_id' => $projectTemplate->getTranslationMemoryId() ?? 0,
      'term_base_id' => $projectTemplate->getTermBaseId() ?? 0,
      'translator_id' => $translator->id(),
      'order_type' => $projectTemplate->getOrderType(),
      'description' => $projectTemplate->getDescription(),
      'project_name' => $projectTemplate->getProjectName(),
      'source_language' => $projectTemplate->getSourceLanguage(),
      'desired_deadline' => $desiredDeadline,
      'job_types' => $projectTemplate->getAvailableServices()
        ? implode(",", $projectTemplate->getAvailableServices())
        : NULL,
      'briefing_to_languagewire' => $projectTemplate->getBriefingToLanguageWire(),
    ];
  }

}
