<?php

namespace Drupal\languagewire_translation_provider\Test\Adapter\Content\HtmlPreview;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Drupal\languagewire_translation_provider\Content\HtmlPreview\HtmlPreviewSettingsChecker;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

/**
 * Html Preview Settings Checker Test .
 */
class HtmlPreviewSettingsCheckerTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * System .
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $system;

  /**
   * Set Up .
   */
  protected function setUp(): void {
    $this->system = $this->prophesize(SystemInterface::class);
  }

  /**
   * When Preview Site Is Not Installed Then Cannot Generate Html Previews .
   *
   * @test
   */
  public function whenPreviewSiteIsNotInstalledThenCannotGenerateHtmlPreviews(): void {
    $this->previewSiteIsNotInstalled();
    $this->privateFilesystemIsConfigured();

    $sut = $this->settingsChecker();

    $errors = $sut->getFailedRequirements();

    $this->assertContains('Preview Site module not installed', $errors);
    $this->assertFalse($sut->canGenerateHtmlPreview());
  }

  /**
   * When Preview Site Is Old Version Then Cannot Generate .
   *
   * @test
   */
  public function whenPreviewSiteIsInstalledButOldVersionThenCannotGenerateHtmlPreviews(): void {
    $this->previewSiteIsInstalled('0.0.1');
    $this->privateFilesystemIsConfigured();

    $sut = $this->settingsChecker();

    $errors = $sut->getFailedRequirements();

    $this->assertContains('Preview Site module (version 0.0.1) does not meet the minimum required version 1.1.2', $errors);
    $this->assertFalse($sut->canGenerateHtmlPreview());
  }

  /**
   * When Preview Site Is Installed Then Can Generate Html Previews .
   *
   * @test
   */
  public function whenPreviewSiteIsInstalledAndPrivateFilesystemExistsThenCanGenerateHtmlPreviews(): void {
    $this->previewSiteIsInstalled('1.1.2');
    $this->privateFilesystemIsConfigured();

    $sut = $this->settingsChecker();

    $errors = $sut->getFailedRequirements();

    $this->assertEmpty($errors);
    $this->assertTrue($sut->canGenerateHtmlPreview());
  }

  /**
   * When Private Filesystem Does Not Exist Then Cannot Generate Html Previews .
   *
   * @test
   */
  public function whenPreviewSiteIsInstalledButPrivateFilesystemDoesNotExistThenCannotGenerateHtmlPreviews(): void {
    $this->previewSiteIsInstalled('0.0.1');
    $this->privateFilesystemIsNotConfigured();

    $sut = $this->settingsChecker();

    $errors = $sut->getFailedRequirements();

    $this->assertContains('Private filesystem folder is not set. Please define $settings[\'file_private_path\'] in your site\'s settings.local.php file', $errors);
    $this->assertFalse($sut->canGenerateHtmlPreview());
  }

  /**
   * Settings Checker .
   */
  private function settingsChecker(): HtmlPreviewSettingsChecker {
    return new HtmlPreviewSettingsChecker($this->system->reveal());
  }

  /**
   * Preview Site Is Installed .
   */
  private function previewSiteIsInstalled(string $version): void {
    $this->system->moduleVersion('preview_site')->willReturn($version);
  }

  /**
   * Preview Site Is Not Installed .
   */
  private function previewSiteIsNotInstalled(): void {
    $this->system->moduleVersion('preview_site')->willReturn(NULL);
  }

  /**
   * Private Filesystem Is Configured .
   */
  private function privateFilesystemIsConfigured(): void {
    $this->system->realPath(Argument::type('string'))->willReturn('/');
  }

  /**
   * Private Filesystem Is Not Configured .
   */
  private function privateFilesystemIsNotConfigured(): void {
    $this->system->realPath(Argument::type('string'))->willReturn('');
  }

}
