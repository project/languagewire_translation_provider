<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Domain;

use Drupal\languagewire_translation_provider\Domain\DocumentFactory;
use Drupal\languagewire_translation_provider\Domain\DocumentStatus;
use Drupal\languagewire_translation_provider\Test\Builder\Domain\ProjectBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\JobItemBuilder;
use Drupal\languagewire_translation_provider\Test\Builder\TmgmtJobBuilder;
use PHPUnit\Framework\TestCase;

/**
* Tests DocumentFactory.
 */
final class DocumentFactoryTest extends TestCase {

  /**
   * When Supplied With Project Domain Entity .
   *
   * @test
   */
  public function whenSuppliedWithProjectDomainEntityThenCreatesCollectionOfDocumentsToUpload(): void {
    $tmgmtJobItem1 = (new JobItemBuilder())->withItemId(100)->build();
    $tmgmtJobItem2 = (new JobItemBuilder())->withItemId(101)->build();
    $tmgmtJob = (new TmgmtJobBuilder())->withJobId(10)->withJobItems([$tmgmtJobItem1, $tmgmtJobItem2])->build();
    $projectId = 1;
    $project = (new ProjectBuilder())->withProjectId($projectId)->withTmgmtJob($tmgmtJob)->build();
    $result = DocumentFactory::createDocumentsForTmgmtJobItems($tmgmtJob->items(), $project->getId());
    $this->assertCount(2, $result);
    /** @var \Drupal\languagewire_translation_provider\Domain\Document $document1 */
    $document1 = $result[0];
    $this->assertSame($projectId, $document1->getProjectId());
    $this->assertSame($tmgmtJobItem1, $document1->getTmgmtItem());
    $this->assertEquals(DocumentStatus::uploadingContent(), $document1->getStatus());
    /** @var \Drupal\languagewire_translation_provider\Domain\Document $document2 */
    $document2 = $result[1];
    $this->assertSame($projectId, $document2->getProjectId());
    $this->assertSame($tmgmtJobItem2, $document2->getTmgmtItem());
    $this->assertEquals(DocumentStatus::uploadingContent(), $document2->getStatus());
  }

}
