<?php

namespace Drupal\languagewire_translation_provider\Test\Builder;

use Drupal\languagewire_translation_provider\Content\TranslatedContent;

/**
 * Translated Content Builder .
 */
class TranslatedContentBuilder {

  /**
   * Text .
   *
   * @var string
   */
  private $text = 'Lorem ipsum';

  /**
   * Content Id .
   *
   * @var string
   */
  private $contentId = 'item-1-field-1';

  /**
   * With Text .
   */
  public function withText(string $text): self {
    $this->text = $text;
    return $this;
  }

  /**
   * With Content Id .
   */
  public function withContentId(string $contentId): self {
    $this->contentId = $contentId;
    return $this;
  }

  /**
   * Build .
   */
  public function build(): TranslatedContent {
    return new TranslatedContent($this->text, $this->contentId);
  }

}
