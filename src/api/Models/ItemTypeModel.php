<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models;

/**
 * Item type model.
 */
class ItemTypeModel {
  public const USER = 'user';
  public const PRODUCT = 'product';
  public const WORK_AREA = 'workArea';
  public const INVOICING_ACCOUNT = 'invoicingAccount';
  public const TRANSLATION_MEMORY = 'translationMemory';
  public const TERM_BASE = 'termBase';
  public const LIMIT = 'limit';

}
