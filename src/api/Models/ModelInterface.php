<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Models;

/**
 * Model interface.
 */
interface ModelInterface {

  /**
   * Get all fields.
   */
  public function getAllFields(): array;

  /**
   * Set fields.
   */
  public function setFields(array $data): void;

  /**
   * Get required fields.
   */
  public function getRequiredFields(): array;

  /**
   * Check if model has required fields.
   */
  public function hasRequiredFields(): bool;

}
