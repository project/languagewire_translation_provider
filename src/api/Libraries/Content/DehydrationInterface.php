<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content;

/**
 * Interface providing dehydration functionality.
 *
 * Dehydration is a process of extracting complex data into
 * a simplified format that is typically used for
 * serialization. Usually the most convenient type for
 * serialization is an array as it can be easily serialized
 * to JSON, XML or any other format.
 */
interface DehydrationInterface {

  /**
   * Dehydrates object data into a serializable format.
   *
   * @return array
   *   Dehydrated data.
   */
  public function dehydrate(): array;

}
