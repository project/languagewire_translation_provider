<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

use Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface;

/**
 * Document interface.
 */
interface DocumentInterface {

  /**
   * Returns document title.
   *
   * @return string
   *   Document title.
   */
  public function getTitle(): string;

  /**
   * Returns document source language.
   *
   * @return string|null
   *   Document source language.
   */
  public function getLanguage(): ?string;

  /**
   * Returns content associated with the document.
   *
   * @return \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface|null
   *   Content associated with the document.
   */
  public function getContent(): ?ContentInterface;

}
