<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite\CleanupAction;

/**
 * Invalid index file amount exception.
 */
class InvalidIndexFileAmountException extends \Exception {

  /**
   * Constructs a new InvalidIndexFileAmountException object.
   *
   * @param int $fileCount
   *   The amount of index.html files found.
   */
  public function __construct(int $fileCount) {
    $message = sprintf("Found %d index.html files (expected 1) to move to the root folder", $fileCount);
    parent::__construct($message);
  }

}
