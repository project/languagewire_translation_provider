<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\api\Models\Project\ProjectStatusModel as PlatformProjectStatus;
use Drupal\languagewire_translation_provider\Client\ProjectStatus;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Project Status Test .
 */
class ProjectStatusTest extends TestCase {

  /**
   * It Maps Platform Project Status .
   *
   * @test
   * @dataProvider projectStatusMapDataProvider
   */
  public function itMapsPlatformProjectStatus(string $platformStatus, int $mappedStatus): void {
    $status = ProjectStatus::fromPlatformStatusValue($platformStatus);
    $this->assertSame($mappedStatus, $status->value());
  }

  /**
   * It Throws For Unrecognized Status .
   *
   * @test
   */
  public function itThrowsForUnrecognizedStatus(): void {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Project status 99 is not recognized.");
    ProjectStatus::fromPlatformStatusValue(99);
  }

  /**
   * It Is Pending .
   *
   * @test
   */
  public function itIsPending(): void {
    $status = new ProjectStatus(ProjectStatus::PENDING);
    $this->assertTrue($status->isPending());
  }

  /**
   * It Is Finished .
   *
   * @test
   */
  public function itIsFinished(): void {
    $status = new ProjectStatus(ProjectStatus::FINISHED);
    $this->assertTrue($status->isFinished());
  }

  /**
   * It Is Cancelled .
   *
   * @test
   */
  public function itIsCancelled(): void {
    $status = new ProjectStatus(ProjectStatus::CANCELLED);
    $this->assertTrue($status->isCancelled());
  }

  /**
   * Project Status Map Data Provider .
   */
  public function projectStatusMapDataProvider(): array {
    return [
          [PlatformProjectStatus::ACTIVE, ProjectStatus::PENDING],
          [PlatformProjectStatus::FINISHED, ProjectStatus::FINISHED],
          [PlatformProjectStatus::CANCELLED, ProjectStatus::CANCELLED],
    ];
  }

}
