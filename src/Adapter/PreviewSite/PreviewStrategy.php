<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite;

use Drupal\preview_site\Entity\PreviewStrategy as DrupalPreviewStrategy;

/**
 * Preview strategy.
 */
final class PreviewStrategy {

  /**
   * Strategy.
   *
   * @var \Drupal\preview_site\Entity\PreviewStrategy
   */
  private DrupalPreviewStrategy $strategy;

  /**
   * Constructs a new PreviewStrategy object.
   *
   * @param \Drupal\preview_site\Entity\PreviewStrategy $strategy
   *   Strategy.
   */
  public function __construct(DrupalPreviewStrategy $strategy) {
    $this->strategy = $strategy;
  }

  /**
   * Get strategy.
   */
  public function getId(): string {
    return $this->strategy->id();
  }

}
