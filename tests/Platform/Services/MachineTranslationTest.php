<?php

namespace Drupal\languagewire_translation_provider\Test\Platform\Services;

use Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface;
use Drupal\languagewire_translation_provider\Platform\Services\MachineTranslation;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use Drupal\languagewire_translation_provider\Settings\OrderType;
use PHPUnit\Framework\TestCase;

/**
 * Machine Translation Test .
 */
class MachineTranslationTest extends TestCase {
  use \Prophecy\PhpUnit\ProphecyTrait;

  /**
   * Deadline Is One Day In The Future From Now .
   *
   * @test
   */
  public function deadlineIsOneDayInTheFutureFromNowDespiteOfJobSettingsDeadlineValue(): void {
    $platformLanguages = $this->prophesize(PlatformLanguagesInterface::class)->reveal();
    $service = new MachineTranslation(21, 'Raw Machine Translation', $platformLanguages);
    $jobSettings = new JobSettings(
        [
          'user' => 1,
          'translation_memory' => 1,
          'work_area' => 1,
          'invoicing_account' => 1,
          'order_type' => OrderType::ORDER,
          'deadline' => '2019-01-01',
        ]
    );
    $service->configure($jobSettings);

    $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    $tomorrow = new \DateTimeImmutable('+1 day', new \DateTimeZone('UTC'));
    $deadline = $service->deadline()->toUtc();

    $nowDiff = $deadline->diff($now);
    $this->assertSame(1, $nowDiff->days);
    // Years.
    $this->assertSame(0, $nowDiff->y);
    // Months.
    $this->assertSame(0, $nowDiff->m);
    // Hours.
    $this->assertSame(0, $nowDiff->h);
    // Minutes.
    $this->assertSame(0, $nowDiff->i);
    // Seconds.
    $this->assertSame(0, $nowDiff->s);

    $tomorrowDiff = $deadline->diff($tomorrow);
    $this->assertSame(0, $tomorrowDiff->days);
    $this->assertSame(0, $tomorrowDiff->y);
    $this->assertSame(0, $tomorrowDiff->m);
    $this->assertSame(0, $tomorrowDiff->h);
    $this->assertSame(0, $tomorrowDiff->i);
    $this->assertSame(0, $tomorrowDiff->s);
  }

}
