<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Client\WorkArea;

/**
 * Work Area Repository.
 */
final class TypedWorkAreaRepository extends TypedConfigurationItemRepositoryBase implements WorkAreaRepositoryInterface {
  private const TYPE = 'work_area';

  /**
   * {@inheritDoc}
   */
  protected function type(): string {
    return self::TYPE;
  }

  /**
   * {@inheritDoc}
   */
  protected function map(\stdClass $workArea): ConfigurationItemInterface {
    return new WorkArea($workArea->item_id, $workArea->name);
  }

}
