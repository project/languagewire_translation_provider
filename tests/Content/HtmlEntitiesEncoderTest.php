<?php

namespace Drupal\languagewire_translation_provider\test\Content;

use Drupal\languagewire_translation_provider\Content\HtmlEntitiesEncoder;
use PHPUnit\Framework\TestCase;

/**
 * Html Entities Encoder Test .
 */
class HtmlEntitiesEncoderTest extends TestCase {

  /**
   * It Escapes Characters To Html Entities .
   *
   * @test
   * @dataProvider unsanitizedContentDataProvider
   */
  public function itEscapesCharactersToHtmlEntities($input, $expected) {
    $sanitizer = new HtmlEntitiesEncoder();

    $this->assertSame($expected, $sanitizer->sanitize($input));
  }

  /**
   * It Does Not Escape Characters For Html Content .
   *
   * @test
   */
  public function itDoesNotEscapeCharactersForHtmlContent() {
    $content = "<div>` ~ ! @ # $ % ^ & * ( ) - = _ + [ ] { } ; : ' \" \\ | , . < > / ?</div>";
    $sanitizer = new HtmlEntitiesEncoder();

    $this->assertSame($content, $sanitizer->sanitize($content));
  }

  /**
   * It Unescapes Html Entities From Content Only For Plain Text .
   *
   * @test
   * @dataProvider sanitizedContentDataProvider
   */
  public function itUnescapesHtmlEntitiesFromContentOnlyForPlainText($input, $expected) {
    $sanitizer = new HtmlEntitiesEncoder();

    $this->assertSame($expected, $sanitizer->desanitize($input));
  }

  /**
   * It Skips Unescaping For Html Content .
   *
   * @test
   */
  public function itSkipsUnescapingForHtmlContent() {
    $content = "<div>` ~ ! @ # $ % ^ &amp; * ( ) - = _ + [ ] { } ; : &#039; &quot; \ | , . &lt; &gt; / ?</div>";
    $sanitizer = new HtmlEntitiesEncoder();

    $this->assertSame($content, $sanitizer->desanitize($content));
  }

  /**
   * Unsanitized Content Data Provider .
   */
  public function unsanitizedContentDataProvider() {
    return [
          [
            "` ~ ! @ # $ % ^ & * ( ) - = _ + [ ] { } ; : ' \" \\ | , . < > / ?",
            "` ~ ! @ # $ % ^ &amp; * ( ) - = _ + [ ] { } ; : &#039; &quot; \ | , . &lt; &gt; / ?",
          ],
          [
          // 0x00A0 is a non breaking space
            "word with nbsp " . mb_chr(0x00A0, 'UTF-8') . " here",
            "word with nbsp &nbsp; here",
          ],
    ];
  }

  /**
   * Sanitized Content Data Provider .
   */
  public function sanitizedContentDataProvider() {
    return [
          [
            "` ~ ! @ # $ % ^ &amp; * ( ) - = _ + [ ] { } ; : &#039; &quot; \ | , . &lt; &gt; / ?",
            "` ~ ! @ # $ % ^ & * ( ) - = _ + [ ] { } ; : ' \" \\ | , . < > / ?",
          ],
          [
            "word with nbsp &nbsp; here",
          // 0x00A0 is a non breaking space
            "word with nbsp " . mb_chr(0x00A0, 'UTF-8') . " here",
          ],
    ];
  }

}
