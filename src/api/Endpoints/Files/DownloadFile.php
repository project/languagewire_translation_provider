<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Files;

use Drupal\languagewire_translation_provider\api\Core\ContentType\ContentJson;
use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Download file endpoint.
 */
class DownloadFile extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/files/:fileID';
  protected const METHOD = Method::GET;
  protected const REQUEST_STRUCTURE = [
    'id',
    'fileID',
  ];

  /**
   * Set file data.
   */
  public function setFileData(int $projectID, string $fileID): void {
    $this->setParams(
          [
            'id' => $projectID,
            'fileID' => $fileID,
          ]
      );
  }

  /**
   * Execute override.
   */
  public function execute(): ?array {
    $this->setResponseContentType(new ContentJson());
    return parent::execute();
  }

}
