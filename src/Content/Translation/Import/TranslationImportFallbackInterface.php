<?php

namespace Drupal\languagewire_translation_provider\Content\Translation\Import;

use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\Project;

/**
 * Translation Import Fallback interface.
 * */
interface TranslationImportFallbackInterface {

  /**
   * Apply translation fallback.
   */
  public function applyTranslationFallback(array $translatedData, Document $document, Project $project): array;

}
