<?php

namespace Drupal\languagewire_translation_provider\Client;

/**
 * Product configuration item.
 */
final class Product implements ConfigurationItemInterface {

  /**
   * ID.
   *
   * @var int
   */
  private int $id;

  /**
   * Name.
   *
   * @var string
   */
  private string $name;

  /**
   * Constructs a new Product object.
   *
   * @param int $id
   *   ID.
   * @param string $name
   *   Name.
   */
  public function __construct(int $id, string $name) {
    $this->id = $id;
    $this->name = $name;
  }

  /**
   * ID.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Name.
   */
  public function name(): string {
    return $this->name;
  }

  /**
   * From platform product.
   */
  public static function fromPlatformProduct(array $product): self {
    return new self($product['id'], $product['name']);
  }

}
