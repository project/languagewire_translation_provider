<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core\DataType;

/**
 * Data type.
 */
interface DataTypeInterface {

  /**
   * Set Structure.
   */
  public function setStructure(array $structure): void;

  /**
   * Validate.
   */
  public function validate(array $data, ?string $position = NULL): bool;

}
