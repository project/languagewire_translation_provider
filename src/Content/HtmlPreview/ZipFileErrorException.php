<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Content\HtmlPreview;

/**
 * Html Preview Settings Checker.
 */
class ZipFileErrorException extends \Exception {
}
