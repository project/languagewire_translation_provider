<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Report;

use Drupal\languagewire_translation_provider\api\Helpers\Ensure;
use Drupal\languagewire_translation_provider\api\Libraries\Content\DehydrationInterface;

/**
 * Translation Statistics Data Transfer Object.
 */
final class TranslationStatistics implements DehydrationInterface {
  /**
   * Internal data storage.
   *
   * @var array
   */
  private array $data;

  /**
   * Constructs a new TranslationStatistics object.
   *
   * @param string $language
   *   Language code.
   * @param int $translatedItems
   *   Translated items count.
   * @param int $traffic
   *   Traffic.
   */
  public function __construct(string $language, int $translatedItems, int $traffic) {
    Ensure::positiveOrZeroInteger($translatedItems, 'Translated items count');
    Ensure::positiveOrZeroInteger($traffic, 'Traffic');

    $this->data = [
      'language' => $language,
      'translatedItems' => $translatedItems,
      'traffic' => $traffic,
    ];
  }

  /**
   * Returns language code.
   *
   * @return string
   *   Language code.
   */
  public function getLanguage(): string {
    return $this->data['language'];
  }

  /**
   * Returns translated items information.
   *
   * @return int
   *   Translated items count.
   */
  public function getTranslatedItems(): int {
    return $this->data['translatedItems'];
  }

  /**
   * Returns traffic information.
   *
   * @return int
   *   Traffic.
   */
  public function getTraffic(): int {
    return $this->data['traffic'];
  }

  /**
   * Dehydrates object data into a serializable format.
   *
   * @return array
   *   Dehydrated data.
   */
  public function dehydrate(): array {
    return $this->data;
  }

}
