<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Project;

use Drupal\languagewire_translation_provider\api\Models\ConfigurationModel;

/**
 * Project Specification Interface.
 *
 * Project Specification Interface for creating
 * specifications by which projects are created.
 */
interface ProjectSpecificationInterface extends ProjectSpecificationBaseInterface {

  /**
   * Returns source documents.
   *
   * @return array
   *   Array of source documents.
   */
  public function getSourceDocuments(): array;

  /**
   * Returns target languages.
   *
   * @return array
   *   Array of target languages.
   */
  public function getTargetLanguages(): array;

  /**
   * Returns purchase order number.
   *
   * @return string
   *   Purchase order number.
   */
  public function getPurchaseOrderNumber(): string;

  /**
   * Returns configuration model.
   *
   * @return \Drupal\languagewire_translation_provider\api\Models\ConfigurationModel
   *   Configuration model.
   */
  public function getConfiguration(): ConfigurationModel;

  /**
   * Whether is demo project.
   *
   * @return bool
   *   Whether is demo project.
   */
  public function isDemo(): bool;

}
