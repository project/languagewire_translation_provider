<?php

namespace Drupal\languagewire_translation_provider\Platform;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\api\Models\LanguageModel;
use Drupal\languagewire_translation_provider\api\Services\LanguageService;

/**
 * All LanguageWire Languages.
* */
final class AllLanguages implements PlatformLanguagesInterface {

  /**
   * Is supported.
   */
  public function isSupported(LanguageMapping $languageMapping): bool {
    return LanguageService::isSupported($languageMapping->remoteLanguage());
  }

  /**
   * Get all languages.
   */
  public function getAllLanguages(): array {
    return LanguageModel::$englishLabels;
  }

}
