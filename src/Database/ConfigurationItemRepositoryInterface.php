<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;
use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;

/**
 * Configuration Item Repository.
 */
interface ConfigurationItemRepositoryInterface {

  /**
   * Remove all configuration items.
   */
  public function removeAll(TmgmtTranslatorInterface $translator): void;

  /**
   * Save all configuration items.
   *
   * @param \Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface[] $items
   *   Configuration items.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function saveAll(array $items, TmgmtTranslatorInterface $translator): void;

  /**
   * Get all configuration items.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   *
   * @return \Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface[]
   *   Configuration items.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

  /**
   * Get configuration item by id.
   */
  public function getById(int $itemId, TmgmtTranslatorInterface $translator): ?ConfigurationItemInterface;

}
