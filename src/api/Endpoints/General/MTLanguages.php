<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\General;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Machine Translation languages endpoint.
 */
class MTLanguages extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'languages/mt-pairs';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
        [
          'sourceLanguageCode',
          'targetLanguageCode',
        ],
  ];

}
