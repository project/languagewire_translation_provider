<?php

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

use Drupal\tmgmt\Entity\Job;

/**
 * TMGMT Job Repository.
* */
final class TmgmtJobRepository implements JobRepositoryInterface {

  /**
   * Get job by ID.
   */
  public function getById(int $jobId): ?TmgmtJobInterface {
    $tmgmtJob = Job::load($jobId);
    return $tmgmtJob ? new TmgmtJob($tmgmtJob) : NULL;
  }

}
