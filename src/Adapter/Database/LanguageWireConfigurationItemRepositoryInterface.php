<?php

namespace Drupal\languagewire_translation_provider\Adapter\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * LanguageWire Configuration Item Repository.
 */
interface LanguageWireConfigurationItemRepositoryInterface {

  /**
   * Remove all configuration items.
   */
  public function removeAll(TmgmtTranslatorInterface $translator, string $itemType): void;

  /**
   * Save all configuration items.
   *
   * @param \Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface[] $items
   *   The items.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   The translator.
   * @param string $itemType
   *   The item type.
   */
  public function saveAll(array $items, TmgmtTranslatorInterface $translator, string $itemType): void;

  /**
   * Get all items.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   The translator.
   * @param string $itemType
   *   The item type.
   *
   * @return \stdClass[]
   *   The items.
   */
  public function getAll(TmgmtTranslatorInterface $translator, string $itemType): array;

  /**
   * Get by id and type.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator adapter.
   * @param int $itemId
   *   Item id.
   * @param string $itemType
   *   Item type.
   *
   * @return \stdClass|null
   *   Configuration item or null.
   */
  public function getByIdAndType(TmgmtTranslatorInterface $translator, int $itemId, string $itemType): ?\stdClass;

}
