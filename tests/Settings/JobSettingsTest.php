<?php

namespace Drupal\languagewire_translation_provider\Test\Settings;

use Drupal\languagewire_translation_provider\Settings\JobSettings;
use PHPUnit\Framework\TestCase;

/**
 * Job Settings Test .
 */
class JobSettingsTest extends TestCase {

  /**
   * Returns Int Value .
   *
   * @test
   */
  public function returnsIntValue(): void {
    $settings = new JobSettings(['key' => 1]);
    $this->assertSame(1, $settings->getInt('key'));
  }

  /**
   * Returns Null If Int Value Is Null .
   *
   * @test
   */
  public function returnsNullIfIntValueIsNull(): void {
    $settings = new JobSettings(['key' => NULL]);
    $this->assertNull($settings->getInt('key'));
  }

  /**
   * Returns String Value .
   *
   * @test
   */
  public function returnsStringValue(): void {
    $settings = new JobSettings(['key' => 'string']);
    $this->assertSame('string', $settings->getString('key'));
  }

  /**
   * Returns Null If String Value Is Null .
   *
   * @test
   */
  public function returnsNullIfStringValueIsNull(): void {
    $settings = new JobSettings(['key' => NULL]);
    $this->assertNull($settings->getString('key'));
  }

  /**
   * Item Content Field Is Enabled For Translation By Default .
   *
   * @test
   * @testWith [{}]
   *           [{"content_settings": {}}]
   *           [{"content_settings": {"item-1": {}}}]
   */
  public function itemContentFieldIsEnabledForTranslationByDefault(array $itemSettings): void {
    $itemId = 1;
    $contentId = 'content-1';
    $settings = new JobSettings($itemSettings);

    $this->assertTrue($settings->isItemContentEnabledForTranslation($itemId, $contentId));
  }

  /**
   * When Deadline Is Valid Then Get Deadline As Date Returns Date Object .
   *
   * @test
   */
  public function whenDeadlineIsValidThenGetDeadlineAsDateReturnsDateObject(): void {
    $nowDate = (new \DateTimeImmutable())
      ->setTimezone(new \DateTimeZone('UTC'))
      ->setTime(0, 0, 0);

    $settings = new JobSettings(['deadline' => $nowDate->format('Y-m-d')]);

    $this->assertEquals(
      $nowDate,
      $settings
        ->getDeadlineAsDate()
        ->toUtc()
        ->setTime(0, 0, 0)
    );
  }

  /**
   * Item Content Field Is Disabled For Translation .
   *
   * @test
   */
  public function itemContentFieldIsDisabledForTranslation(): void {
    $itemId = 1;
    $contentId = 'content-1';
    $itemSettings = ['content_settings' => ["item-$itemId" => ["content-$contentId" => 0]]];
    $settings = new JobSettings($itemSettings);

    $this->assertFalse($settings->isItemContentEnabledForTranslation($itemId, $contentId));
  }

  /**
   * If Prefixed Field Exists Returns Valid Id .
   *
   * @test
   */
  public function whenPrefixedFieldExistsThenGetprefixedfieldwithfallbackReturnsValidId(): void {
    $settingsWithService = new JobSettings(['template_or_service' => 'service_1']);
    $settingsWithTemplate = new JobSettings(['template_or_service' => 'template_1']);

    $this->assertEquals(1, $settingsWithService->getService());
    $this->assertEquals(1, $settingsWithTemplate->getProjectTemplate());
  }

  /**
   * If Prefixed Field Doesn't Exist But Has Fallback Returns Valid Id .
   *
   * @test
   */
  public function whenPrefixedFieldDoesNotExistButHasFallbackThenGetprefixedfieldwithfallbackReturnsValidId(): void {
    $settingsWithService = new JobSettings(['service' => 1]);
    $settingsWithTemplate = new JobSettings(['project_template' => 1]);

    $this->assertEquals(1, $settingsWithService->getService());
    $this->assertEquals(1, $settingsWithTemplate->getProjectTemplate());
  }

  /**
   * If Prefixed Field Exists But Has Wrong Prefix Returns Null .
   *
   * @test
   */
  public function whenPrefixedFieldExistsButHasWrongPrefixThenGetprefixedfieldwithfallbackReturnsNull(): void {
    $settings = new JobSettings(['template_or_service' => 'asd']);

    $this->assertNull($settings->getService());
    $this->assertNull($settings->getProjectTemplate());
  }

  /**
   * If Prefixed Field And Fallback Field Don't Exist Returns Null .
   *
   * @test
   */
  public function whenPrefixedFieldAndFallbackFieldDoNotExistThenGetprefixedfieldwithfallbackReturnsNull(): void {
    $settings = new JobSettings([]);

    $this->assertNull($settings->getService());
    $this->assertNull($settings->getProjectTemplate());
  }

  /**
   * When Briefing Is String Then Getbriefing Returns It .
   *
   * @test
   */
  public function whenBriefingIsStringThenGetbriefingReturnsIt(): void {
    $settings = new JobSettings(['briefing' => 'briefing']);

    $this->assertEquals('briefing', $settings->getBriefing());
  }

  /**
   * When Briefing Is Text Format Array Then Get Briefing Returns String .
   *
   * @test
   */
  public function whenBriefingIsTextFormatArrayThenGetBriefingReturnsString(): void {
    $settings = new JobSettings([
      'briefing' => [
        'value' => 'briefing',
        'format' => 'basic_html',
      ],
    ]);

    $this->assertEquals('briefing', $settings->getBriefing());
  }

}
