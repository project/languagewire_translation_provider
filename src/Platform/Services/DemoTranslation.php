<?php

namespace Drupal\languagewire_translation_provider\Platform\Services;

use Drupal\languagewire_translation_provider\Date;
use Drupal\languagewire_translation_provider\Platform\AvailableServices;
use Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface;

/**
 * Demo Translation service.
 */
final class DemoTranslation extends TranslationService {

  /**
   * Constructs a new DemoTranslation object.
   *
   * @param \Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface $languages
   *   Languages.
   */
  public function __construct(PlatformLanguagesInterface $languages) {
    parent::__construct(AvailableServices::DEMO_SERVICE_ID, 'Demo translation', $languages);
  }

  /**
   * Get deadline.
   */
  public function deadline(): Date {
    $now = new \DateTimeImmutable();
    $in2Minutes = $now->add(new \DateInterval('PT2M'));
    return new Date($in2Minutes);
  }

}
