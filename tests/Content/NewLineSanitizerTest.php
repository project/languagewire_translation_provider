<?php

namespace Drupal\languagewire_translation_provider\Test\Content;

use Drupal\languagewire_translation_provider\Content\NewLineSanitizer;
use PHPUnit\Framework\TestCase;

/**
 * Sanitizes Line Breaks In Text .
 */
class NewLineSanitizerTest extends TestCase {

  /**
   * Sanitizes Line Breaks In Text .
   *
   * @test
   */
  public function sanitizesLineBreaksInText() {
    $unsanitized = "Has\rLine\nBreaks\r\n";
    $sanitizer = new NewLineSanitizer();

    $sanitized = $sanitizer->sanitize($unsanitized);
    $expected = 'Has<hr data-escaped-char="carriage-return">Line<hr data-escaped-char="line-feed">Breaks<hr data-escaped-char="carriage-return"><hr data-escaped-char="line-feed">';

    $this->assertSame($expected, $sanitized);
  }

  /**
   * Desanitizes Line Breaks In Test .
   *
   * @test
   */
  public function desanitizesLineBreaksInTest() {
    $sanitized = 'Has<hr data-escaped-char="carriage-return">Line<hr data-escaped-char="line-feed">Breaks<hr data-escaped-char="carriage-return"><hr data-escaped-char="line-feed">';
    $sanitizer = new NewLineSanitizer();

    $desanitized = $sanitizer->desanitize($sanitized);
    $expected = "Has\rLine\nBreaks\r\n";

    $this->assertSame($expected, $desanitized);
  }

}
