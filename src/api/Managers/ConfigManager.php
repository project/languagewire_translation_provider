<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Managers;

use Drupal\languagewire_translation_provider\api\Core\Exception\Error;

/**
 * Configs manager handler.
 */
class ConfigManager {

  /**
   * ConfigManager constructor.
   *
   * @throws \Drupal\languagewire_translation_provider\api\Core\Exception\Error
   */
  public function __construct() {
    $this->defineAllConfigs();
  }

  /**
   * Define all configs.
   *
   * @throws \Drupal\languagewire_translation_provider\api\Core\Exception\Error
   *   Error handler exception.
   */
  private function defineAllConfigs(): void {
    $configs = $this->getAllConfigs();
    foreach ($configs as $key => $value) {
      $envValue = getenv($key);
      if ($envValue !== FALSE) {
        $value = $envValue;
      }
      if (!defined($key) && !define($key, $value)) {
        throw new Error('Could not define constant ' . $key);
      }
    }
  }

  /**
   * Get all configs.
   */
  private function getAllConfigs(): array {
    return require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Configs.php';
  }

}
