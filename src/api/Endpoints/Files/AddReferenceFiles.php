<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Files;

use Drupal\languagewire_translation_provider\api\Core\ContentType\ContentBinaryStream;
use Drupal\languagewire_translation_provider\api\Core\DataPosition;
use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;
use Drupal\languagewire_translation_provider\api\Helpers\Strings;

/**
 * Add reference files endpoint.
 */
class AddReferenceFiles extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/reference-files';
  protected const METHOD = Method::POST;
  protected const RESPONSE_SUCCESS_CODES = [201];
  protected const REQUEST_STRUCTURE = [
    DataPosition::HEAD => [
      'id',
      'filename',
    ],
  ];
  protected const RESPONSE_STRUCTURE = [
    'fileId',
    'fileName',
    'projectId',
  ];

  /**
   * Add reference file.
   */
  public function addReferenceFile(int $projectID, string $filename, string $data): void {
    $this->setParams(
          [
            DataPosition::HEAD => [
              'id' => $projectID,
              'filename' => Strings::cleanUp($filename),
            ],
            DataPosition::BODY => $data,
          ]
      );
  }

  /**
   * Execution override.
   */
  public function execute(): ?array {
    $this->setRequestContentType(new ContentBinaryStream());
    return parent::execute();
  }

}
