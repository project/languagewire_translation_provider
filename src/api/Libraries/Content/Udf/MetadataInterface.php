<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Content\Udf;

use Drupal\languagewire_translation_provider\api\Libraries\Content\DehydrationInterface;

/**
 * Metadata Interface.
 *
 * Provides an interface for creating metadata
 * objects that can be assigned to Documents or Frames.
 *
 * Metadata is a set of arbitrary data. This data is uploaded
 * to the LanguageWire server along with the document's
 * text. Metadata is not translated though. It is round-tripped
 * along with the translated text. This allows the client
 * to uniquely identify every text in a document
 * (as a document may consist of multiple texts).
 */
interface MetadataInterface extends DehydrationInterface {

  /**
   * Adds new metadata pair.
   *
   * @param mixed $key
   *   The key of the metadata pair.
   * @param mixed $value
   *   The value of the metadata pair.
   *
   * @return void
   *   Returns nothing.
   */
  public function add(mixed $key, mixed $value): void;

  /**
   * Removes metadata information by key.
   *
   * @param mixed $key
   *   The key of the metadata pair.
   *
   * @return void
   *   Returns nothing.
   */
  public function remove(mixed $key): void;

}
