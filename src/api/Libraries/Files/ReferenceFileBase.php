<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Files;

use Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface;

/**
 * Base class for reference files.
 */
class ReferenceFileBase implements ReferenceFileInterface {
  /**
   * Content associated with the reference file.
   *
   * @var \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface
   */
  protected ContentInterface $content;
  /**
   * Reference file filename.
   *
   * @var string
   */
  protected string $filename;

  /**
   * Constructs a new ReferenceFileBase object.
   *
   * @param \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface $content
   *   Content associated with the reference file.
   * @param string $filename
   *   Reference file filename.
   */
  public function __construct(
    ContentInterface $content,
    string $filename
  ) {
    $this->validateFilename($filename);

    $this->content = $content;
    $this->filename = $filename;
  }

  /**
   * Returns reference file filename.
   *
   * @return string|null
   *   Reference file filename.
   */
  public function getFilename(): ?string {
    return $this->filename;
  }

  /**
   * Returns content associated with the reference file.
   *
   * @return \Drupal\languagewire_translation_provider\api\Libraries\Content\ContentInterface
   *   Content associated with the reference file.
   */
  public function getContent(): ContentInterface {
    return $this->content;
  }

  /**
   * Validates filename.
   */
  private function validateFilename(string $filename): void {
    if (empty($filename)) {
      throw new InvalidFilenameException('Filename cannot be empty');
    }
  }

}
