<?php

namespace Drupal\languagewire_translation_provider\Platform\Services;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\LanguageMapping;
use Drupal\languagewire_translation_provider\Date;
use Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface;
use Drupal\languagewire_translation_provider\Settings\JobSettings;
use Drupal\languagewire_translation_provider\Settings\OrderType;

/**
 * Translation Service.
 * */
abstract class TranslationService implements ServiceInterface {

  /**
   * Translation service id.
   *
   * @var int
   */
  protected int $id;

  /**
   * Translation service name.
   *
   * @var string
   */
  protected string $name;

  /**
   * User id.
   *
   * @var string
   */
  protected string $userId;

  /**
   * Template id.
   *
   * @var int|null
   */
  protected ?int $templateId;

  /**
   * Translation service translation memory id.
   *
   * @var int|null
   */
  protected ?int  $translationMemoryId;

  /**
   * Translation service term base id.
   *
   * @var int|null
   */
  protected ?int $termBaseId;

  /**
   * Translation service invoicing account id.
   *
   * @var int
   */
  protected int $invoicingAccountId;

  /**
   * Translation service work area id.
   *
   * @var int
   */
  protected int $workAreaId;

  /**
   * Translation service order type.
   *
   * @var string
   */
  protected string $orderType;

  /**
   * Translation service deadline.
   *
   * @var \Drupal\languagewire_translation_provider\Date
   */
  protected Date $deadline;

  /**
   * Platform languages.
   *
   * @var \Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface
   */
  protected PlatformLanguagesInterface $languages;

  /**
   * Constructs a new TranslationService object.
   *
   * @param int $id
   *   Translation service id.
   * @param string $name
   *   Translation service name.
   * @param \Drupal\languagewire_translation_provider\Platform\PlatformLanguagesInterface $languages
   *   Platform languages.
   */
  public function __construct(int $id, string $name, PlatformLanguagesInterface $languages) {
    $this->id = $id;
    $this->name = $name;
    $this->languages = $languages;
  }

  /**
   * Get id.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Get name.
   */
  public function name(): string {
    return $this->name;
  }

  /**
   * Get user id.
   */
  public function userId(): int {
    return $this->userId;
  }

  /**
   * Get template id.
   */
  public function templateId(): ?int {
    return $this->templateId;
  }

  /**
   * Get translation memory id.
   */
  public function translationMemoryId(): ?int {
    return $this->translationMemoryId;
  }

  /**
   * Get term base id.
   */
  public function termBaseId(): ?int {
    return $this->termBaseId;
  }

  /**
   * Get invoicing account id.
   */
  public function invoicingAccountId(): int {
    return $this->invoicingAccountId;
  }

  /**
   * Get work area id.
   */
  public function workAreaId(): int {
    return $this->workAreaId;
  }

  /**
   * Get order type.
   */
  public function orderType(): string {
    return $this->orderType;
  }

  /**
   * Get deadline.
   */
  public function deadline(): Date {
    return $this->deadline;
  }

  /**
   * Configure job settings.
   */
  public function configure(JobSettings $jobSettings): void {
    $this->userId = $jobSettings->getUser() ?? 0;
    $this->templateId = $jobSettings->getProjectTemplate();
    $this->translationMemoryId = $jobSettings->getTranslationMemory();
    $this->termBaseId = $jobSettings->getTermBase();
    $this->invoicingAccountId = $jobSettings->getInvoicingAccount() ?? 0;
    $this->workAreaId = $jobSettings->getWorkArea() ?? 0;
    $this->orderType = $jobSettings->getOrderType() ?? OrderType::ORDER;
    $this->deadline = $jobSettings->getDeadlineAsDate();
  }

  /**
   * Is language supported.
   */
  public function isLanguageSupported(LanguageMapping $languageMapping): bool {
    // Trust system pre-definition.
    return TRUE;
  }

}
