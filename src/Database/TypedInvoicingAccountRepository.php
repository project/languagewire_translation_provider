<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Client\ConfigurationItemInterface;
use Drupal\languagewire_translation_provider\Client\InvoicingAccount;

/**
 * Invoicing Account Repository.
 */
final class TypedInvoicingAccountRepository extends TypedConfigurationItemRepositoryBase implements InvoicingAccountRepositoryInterface {
  private const TYPE = 'invoicing_account';

  /**
   * {@inheritDoc}
   */
  protected function type(): string {
    return self::TYPE;
  }

  /**
   * {@inheritDoc}
   */
  protected function map(\stdClass $invoicingAccount): ConfigurationItemInterface {
    return new InvoicingAccount($invoicingAccount->item_id, $invoicingAccount->name);
  }

}
