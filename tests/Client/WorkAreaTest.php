<?php

namespace Drupal\languagewire_translation_provider\Test\Client;

use Drupal\languagewire_translation_provider\Client\WorkArea;
use PHPUnit\Framework\TestCase;

/**
 * Work Area Test .
 */
class WorkAreaTest extends TestCase {

  /**
   * It Creates New Object Instance From Platform Work Area .
   *
   * @test
   */
  public function itCreatesNewObjectInstanceFromPlatformWorkArea(): void {
    $platformWorkArea = ['id' => 1, 'name' => 'WA'];
    $workArea = WorkArea::fromPlatformWorkArea($platformWorkArea);

    $this->assertSame(1, $workArea->id());
    $this->assertSame('WA', $workArea->name());
  }

}
