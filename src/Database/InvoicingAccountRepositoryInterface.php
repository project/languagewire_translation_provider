<?php

namespace Drupal\languagewire_translation_provider\Database;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * Invoicing Account Repository interface.
 */
interface InvoicingAccountRepositoryInterface extends ConfigurationItemRepositoryInterface {

  /**
   * Save all.
   *
   * @param \Drupal\languagewire_translation_provider\Client\InvoicingAccount[] $items
   *   Invoicing account items.
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function saveAll(array $items, TmgmtTranslatorInterface $translator): void;

  /**
   * Get all.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   *
   * @return \Drupal\languagewire_translation_provider\Client\InvoicingAccount[]
   *   Invoicing account items.
   */
  public function getAll(TmgmtTranslatorInterface $translator): array;

}
