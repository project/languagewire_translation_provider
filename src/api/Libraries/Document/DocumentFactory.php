<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Document;

use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\UniversalDocumentFactory;
use Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\UniversalDocumentInterface;
use Drupal\languagewire_translation_provider\api\Libraries\Content\UdfContent;

/**
 * Document Factory class for creating instances of Document object.
 *
 * It provides methods for creating new Document object
 * based on different type of input data.
 */
final class DocumentFactory {

  /**
   * Creates Document object from an array.
   *
   * Creates Document object from an array containing
   * texts to translate, language code and optional title.
   *
   * Full `$texts` array format:
   * ```
   * [
   *     'comment' => 'a full UDF frame', //optional
   *     'metadata' => [  //optional arbitrary data
   *         'foo' => 'bar',
   *         'baz' => 'bak'
   *     ],
   *    'frames' => [
   *        [
   *            'text' => 'Hello World', //text to translate
   *            'comment' => 'Frame comment', //optional
   *            'metadata' => [   //optional arbitrary data
   *                'a' => 'b',
   *                'c' => 'd'
   *            ]
   *        ]
   *    ]
   * ];
   * ```
   * Simple `$texts` array:
   * ```
   * ['Text 1', 'Text 2'];
   * ```
   * Simple `$texts` array with comments and metadata
   * (comment and metadata are optional):
   * ```
   * [
   *     [
   *        'text' => 'Text 1',
   *        'comment' => 'Comment 1',
   *        'metadata' => ['key 1' => 'val 1']
   *     ],
   *     [
   *        'text' => 'Text 2',
   *        'comment' => 'Comment 2',
   *        'metadata' => ['key 2' => 'val 2']
   *    ],
   * ];
   * ```.
   *
   * @param array $texts
   *   Array of texts to translate.
   * @param string $language
   *   Language code.
   * @param string $title
   *   Optional title.
   *
   * @return DocumentInterface
   *   Document object.
   */
  public static function makeFromText(array $texts, string $language, string $title = ''): DocumentInterface {
    $universalDocument = UniversalDocumentFactory::create($texts);

    return self::makeFromUdf($universalDocument, $language, $title);
  }

  /**
   * Creates Document object based on UDF.
   *
   * Creates Document object based on
   * Universal Document Format object,
   * language code and optional title.
   *
   * @param \Drupal\languagewire_translation_provider\api\Libraries\Content\Udf\UniversalDocumentInterface $universalDocument
   *   Universal Document Format object.
   * @param string $language
   *   Language code.
   * @param string $title
   *   Optional title.
   *
   * @return DocumentInterface
   *   Document object.
   */
  public static function makeFromUdf(UniversalDocumentInterface $universalDocument, string $language, string $title = ''): DocumentInterface {
    return new DraftDocument(new UdfContent($universalDocument), $language, $title);
  }

}
