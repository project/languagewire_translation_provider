<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Core;

/**
 * Data position.
 */
class DataPosition {
  public const HEAD = 'head';
  public const BODY = 'body';

}
