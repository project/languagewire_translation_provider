<?php

namespace Drupal\languagewire_translation_provider;

/**
 * Translator UI Trait.
 */
trait TranslatorUi {

  /**
   * Filter out not selected items.
   */
  private function filterOutNotSelectedItems(array $items, array $userSelectedItemIds): array {
    return array_filter($items, function (int $itemId) use ($userSelectedItemIds) {
        return in_array($itemId, $userSelectedItemIds);
    }, ARRAY_FILTER_USE_KEY);
  }

}
