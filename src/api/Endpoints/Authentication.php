<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints;

use Drupal\languagewire_translation_provider\api\Core\ContentType\ContentFormUrlEncoded;
use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;
use Drupal\languagewire_translation_provider\api\Core\Settings;

/**
 * Authenticator endpoint.
 */
class Authentication extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = PROJECT_API_AUTH_ENDPOINT;
  protected const METHOD = Method::POST;
  protected const REQUEST_STRUCTURE = [
    'client_id',
    'client_secret',
    'grant_type',
  ];
  protected const RESPONSE_STRUCTURE = [
    'access_token',
    'expires_in',
    'refresh_expires_in',
    'token_type',
    'not-before-policy',
    'scope',
  ];

  public function __construct(Settings $settings) {
    parent::__construct($settings);

    $this->setRequestContentType(new ContentFormUrlEncoded());
  }

  /**
   * Set credentials.
   */
  public function setCredentials(string $clientID, string $clientSecret): void {
    $this->setParams(
          [
            'client_id' => $clientID,
            'client_secret' => $clientSecret,
            'grant_type' => 'client_credentials',
          ]
      );
  }

}
