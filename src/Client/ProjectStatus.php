<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\languagewire_translation_provider\api\Models\Project\ProjectStatusModel;

/**
 * Project Status.
 */
final class ProjectStatus {
  public const UNDER_CREATION = 0;
  public const PENDING = 1;
  public const FINISHED = 2;
  public const CANCELLED = 3;

  /**
   * Project status.
   *
   * @var int
   */
  private int $status;

  /**
   * Constructs a new ProjectStatus object.
   *
   * @param int $status
   *   Project status.
   */
  public function __construct(int $status) {
    $this->status = $status;
  }

  /**
   * Value.
   */
  public function value(): int {
    return $this->status;
  }

  /**
   * Is cancelled.
   */
  public function isCancelled(): bool {
    return $this->status == ProjectStatus::CANCELLED;
  }

  /**
   * Is finished.
   */
  public function isFinished(): bool {
    return $this->status == ProjectStatus::FINISHED;
  }

  /**
   * Is pending.
   */
  public function isPending(): bool {
    return $this->status == ProjectStatus::PENDING;
  }

  /**
   * Under creation.
   */
  public static function underCreation(): self {
    return new self(self::UNDER_CREATION);
  }

  /**
   * Finished.
   */
  public static function finished(): self {
    return new self(self::FINISHED);
  }

  /**
   * Cancelled.
   */
  public static function cancelled(): self {
    return new self(self::CANCELLED);
  }

  /**
   * From platform status value.
   */
  public static function fromPlatformStatusValue(string $status): self {
    return match ($status) {
      ProjectStatusModel::DRAFT => new self(self::UNDER_CREATION),
      ProjectStatusModel::ACTIVE => new self(self::PENDING),
      ProjectStatusModel::FINISHED => new self(self::FINISHED),
      ProjectStatusModel::CANCELLED => new self(self::CANCELLED),
      default => throw new \InvalidArgumentException("Project status $status is not recognized."),
    };
  }

  /**
   * Equals.
   */
  public function equals(self $other): bool {
    return $this->status == $other->status;
  }

}
