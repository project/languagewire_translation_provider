<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Service\ProjectContentUploader;

use Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface;

/**
 * Fixed limit document selector.
 */
final class FixedLimitDocumentSelector implements DocumentSelectorInterface {

  /**
   * Limit.
   *
   * @var int
   */
  private int $limit;

  /**
   * Constructs a new FixedLimitDocumentSelector object.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\TMGMT\TmgmtTranslatorInterface $translator
   *   Translator.
   */
  public function __construct(TmgmtTranslatorInterface $translator) {
    $this->limit = $translator->getItemUploadBatchSize();

    if ($this->limit <= 0) {
      throw new \InvalidArgumentException("Received an invalid non-positive value $this->limit for ItemUploadBatchSize");
    }
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\languagewire_translation_provider\Platform\Document\UploadableDocument[] $documents
   *   Uploadable documents.
   *
   * @return \Drupal\languagewire_translation_provider\Platform\Document\UploadableDocument[]
   *   Uploadable documents.
   */
  public function selectDocuments(array $documents): array {
    return array_slice($documents, 0, $this->limit);
  }

}
