<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\General;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Available versions endpoint.
 */
class Version extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'version';
  protected const METHOD = Method::GET;
  protected const RESPONSE_STRUCTURE = [
    'version',
    'branchName',
  ];

}
