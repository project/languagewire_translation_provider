<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Test\Domain;

use Drupal\languagewire_translation_provider\Domain\ProjectFactory;
use Drupal\languagewire_translation_provider\Domain\ProjectStatus;
use Drupal\languagewire_translation_provider\Test\Builder\TmgmtJobBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Project Factory Test.
 */
class ProjectFactoryTest extends TestCase {

  /**
   * When Supplied With Tmgmt Job Then Creates Project Domain Entity .
   *
   * @test
   */
  public function whenSuppliedWithTmgmtJobThenCreatesProjectDomainEntity(): void {
    $jobId = 1;
    $job = (new TmgmtJobBuilder())->withJobId($jobId)->build();
    $result = ProjectFactory::createProjectFromJob($job);
    $this->assertSame($job, $result->getTmgmtJob());
    $this->assertNotEmpty($result->getCorrelationId());
    $this->assertEquals(ProjectStatus::underCreation(), $result->getStatus());
  }

}
