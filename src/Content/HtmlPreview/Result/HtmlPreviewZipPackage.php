<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Content\HtmlPreview\Result;

/**
 * Html preview zip package.
 */
final class HtmlPreviewZipPackage {
  /**
   * Path.
   *
   * @var string
   */
  private string $path;

  /**
   * Constructs a new HtmlPreviewZipPackage object.
   *
   * @param string $path
   *   Path.
   */
  public function __construct(string $path) {
    $this->path = $path;
  }

  /**
   * Get path.
   */
  public function getPath(): string {
    return $this->path;
  }

}
