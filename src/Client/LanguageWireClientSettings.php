<?php

namespace Drupal\languagewire_translation_provider\Client;

use Drupal\Core\Extension;
use Drupal\languagewire_translation_provider\Adapter\SystemInterface;
use Drupal\languagewire_translation_provider\api\Core\Settings;
use Drupal\languagewire_translation_provider\Settings\Endpoint;

/**
 * LanguageWire Client Settings.
 */
final class LanguageWireClientSettings implements ClientSettingsInterface {
  /**
   * Application name.
   */
  private const APPLICATION_NAME = 'drupal-connector';

  /**
   * System.
   *
   * @var \Drupal\languagewire_translation_provider\Adapter\SystemInterface
   */
  private SystemInterface $system;

  /**
   * Authorization token.
   *
   * @var string
   */
  private string $authorizationToken;

  /**
   * Constructs a new LanguageWireClientSettings object.
   *
   * @param string $authorizationToken
   *   Authorization token.
   * @param \Drupal\languagewire_translation_provider\Adapter\SystemInterface $system
   *   System.
   */
  public function __construct(string $authorizationToken, SystemInterface $system) {
    $this->authorizationToken = $authorizationToken;
    $this->system = $system;
  }

  /**
   * {@inheritdoc}
   */
  public function get(): Settings {
    $endpoint = new Endpoint($this->system->settings());

    return new Settings($endpoint->get(), $this->authorizationToken, self::APPLICATION_NAME, $this->getDrupalVersion());
  }

  /**
   * Get the Drupal version.
   */
  private function getDrupalVersion(): string {
    $not_found_version = 'undefined';
    if (method_exists('Drupal\Core\Extension', 'getExtensionInfo')) {
      $system_info = Extension::getExtensionInfo('core');

      return $system_info['version'] ?? $not_found_version;
    }
    elseif (defined('VERSION')) {
      return VERSION;
    }

    return $not_found_version;
  }

}
