<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Endpoints\Files;

use Drupal\languagewire_translation_provider\api\Core\Method;
use Drupal\languagewire_translation_provider\api\Core\ProjectAPILibraryAbstract;

/**
 * Delete reference file endpoint.
 */
class DeleteReferenceFile extends ProjectAPILibraryAbstract {
  protected const ENDPOINT = 'projects/:id/reference-files/:fileID';
  protected const METHOD = Method::DELETE;
  protected const RESPONSE_SUCCESS_CODES = [204];
  protected const REQUEST_STRUCTURE = [
    'id',
    'fileID',
  ];

  /**
   * Set file data.
   */
  public function setFileData(int $projectID, string $fileID): void {
    $this->setParams(
          [
            'id' => $projectID,
            'fileID' => $fileID,
          ]
      );
  }

}
