<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\api\Libraries\Files;

/**
 * Draft reference file class.
 */
class DraftReferenceFile extends ReferenceFileBase {

}
