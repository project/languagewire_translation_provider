<?php

namespace Drupal\languagewire_translation_provider\Adapter\TMGMT;

use Drupal\languagewire_translation_provider\Adapter\SystemInterface;

/**
 * Translator Interface.
 * */
interface TmgmtTranslatorInterface {

  /**
   * Get translator ID.
   */
  public function id();

  /**
   * Get client ID.
   */
  public function getClientId(): string;

  /**
   * Get client secret.
   */
  public function getClientSecret(): string;

  /**
   * Has credentials.
   */
  public function hasCredentials(): bool;

  /**
   * Get mapped remote language.
   */
  public function mapToRemoteLanguage(string $language): string;

  /**
   * Get remote languages mappings.
   */
  public function getRemoteLanguagesMappings(): array;

  /**
   * Get selected services.
   */
  public function getSelectedServices(): array;

  /**
   * Has services.
   */
  public function hasServices(): bool;

  /**
   * Get selected user.
   */
  public function getSelectedUser(): array;

  /**
   * Has user.
   */
  public function hasUser(): bool;

  /**
   * Get selected translation memories.
   */
  public function getSelectedTranslationMemories(): array;

  /**
   * Has translation memories.
   */
  public function hasTranslationMemories(): bool;

  /**
   * Get selected term bases.
   */
  public function getSelectedTermBases(): array;

  /**
   * Has term bases.
   */
  public function hasTermBases(): bool;

  /**
   * Get selected work areas.
   */
  public function getSelectedWorkAreas(): array;

  /**
   * Has work areas.
   */
  public function hasWorkAreas(): bool;

  /**
   * Is bundle languages enabled.
   */
  public function isBundlingEnabled(): bool;

  /**
   * Is default delivery date enabled.
   */
  public function isDefaultDeliveryDateEnabled(): bool;

  /**
   * Is HTML preview enabled.
   */
  public function isHtmlPreviewEnabled(): bool;

  /**
   * Get selected invoicing accounts.
   */
  public function getSelectedInvoicingAccounts(): array;

  /**
   * Has invoicing accounts.
   */
  public function hasInvoicingAccounts(): bool;

  /**
   * Get selected order type.
   */
  public function getSelectedOrderTypes(): array;

  /**
   * Has order type.
   */
  public function hasOrderTypes(): bool;

  /**
   * Get selected project template IDs.
   */
  public function getSelectedProjectTemplateIds(): array;

  /**
   * Has project templates.
   */
  public function hasProjectTemplates(): bool;

  /**
   * Get item upload batch size.
   */
  public function getItemUploadBatchSize(): int;

  /**
   * Clear selected configuration items.
   */
  public function clearSelectedConfigurationItems(): void;

  /**
   * Is configured.
   */
  public function isConfigured(): bool;

  /**
   * Get plugin configuration.
   */
  public function getPluginConfiguration(SystemInterface $system): TranslatorPluginConfiguration;

  /**
   * Set max documents per project.
   */
  public function setMaxDocumentsPerProject(int $value): void;

  /**
   * Get max documents per project.
   */
  public function getMaxDocumentsPerProject(): int;

}
