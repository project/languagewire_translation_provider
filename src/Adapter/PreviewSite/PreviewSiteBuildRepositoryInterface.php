<?php

declare(strict_types=1);

namespace Drupal\languagewire_translation_provider\Adapter\PreviewSite;

use Drupal\languagewire_translation_provider\Adapter\Entity\EntityInterface;

/**
 * Preview site build repository.
 * */
interface PreviewSiteBuildRepositoryInterface {

  /**
   * Get preview site build by ID.
   */
  public function getById(int $buildId): ?PreviewSiteBuild;

  /**
   * Create static preview for entity.
   *
   * @param \Drupal\languagewire_translation_provider\Adapter\Entity\EntityInterface $entity
   *   Entity to create static preview for.
   * @param string $label
   *   Admin-viewable name of the preview site build.
   * @param string $strategyId
   *   ID of strategy to use.
   *
   * @return PreviewSiteBuild|null
   *   Created preview site build or null if failed.
   */
  public function create(EntityInterface $entity, string $label, string $strategyId): ?PreviewSiteBuild;

  /**
   * Delete preview site build by ID.
   */
  public function deleteById(int $buildId): void;

}
