<?php

namespace Drupal\languagewire_translation_provider\Content\Translation\Import;

use Drupal\languagewire_translation_provider\Domain\Document;
use Drupal\languagewire_translation_provider\Domain\Project;

/**
 * Translation Import Fallback.
* */
final class CompositeFallback implements TranslationImportFallbackInterface {

  /**
   * Fallback strategies.
   *
   * @var TranslationImportFallbackInterface[]
   */
  private array $fallbackStrategies;

  /**
   * Constructs a new CompositeFallback object.
   *
   * @param TranslationImportFallbackInterface[] $fallbackStrategies
   *   Fallback strategies.
   */
  public function __construct(...$fallbackStrategies) {
    $this->fallbackStrategies = $fallbackStrategies;
  }

  /**
   * Apply translation fallback.
   */
  public function applyTranslationFallback(array $translatedData, Document $document, Project $project): array {
    foreach ($this->fallbackStrategies as $strategy) {
      $translatedData = $strategy->applyTranslationFallback($translatedData, $document, $project);
    }

    return $translatedData;
  }

}
